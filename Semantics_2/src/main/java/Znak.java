

public class Znak {

	private String imeZnaka;
	private boolean jeliNezavrsni;
	private boolean jeliBroj;
	private boolean jeliKonstanta;
	public Tipovi tip = null;
	
	public String getTip() {
		return tip.toString();
	}

	public void setTip(Tipovi tip) {
		this.tip = tip;
	}

	public boolean getJeliKonstanta() {
		return jeliKonstanta;
	}

	public void setJeliKonstanta(boolean jeliKonstanta) {
		this.jeliKonstanta = jeliKonstanta;
	}

	private int redakZavrsnog;
	private String zavrsniUProgramu;

	public String getZavrsniUProgramu() {
		return zavrsniUProgramu;
	}

	// konstruktor za zavrsni znak
	public Znak(String imeZnaka, int redakZavrsnog, String zavrsniUProgramu) {

		super();
		this.imeZnaka = imeZnaka;
		this.jeliNezavrsni = false;
		this.redakZavrsnog = redakZavrsnog;
		this.zavrsniUProgramu = zavrsniUProgramu;

	}

	public Znak(String imeZnaka, boolean jeliNezavrsni) {
		super();
		this.imeZnaka = imeZnaka;
		this.jeliNezavrsni = jeliNezavrsni;
		this.jeliBroj = false;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((imeZnaka == null) ? 0 : imeZnaka.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Znak other = (Znak) obj;
		if (imeZnaka == null) {
			if (other.imeZnaka != null)
				return false;
		} else if (!imeZnaka.equals(other.imeZnaka))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return imeZnaka;
	}
	public String getImeZnaka() {
		return imeZnaka;
	}

	public boolean isJeliNezavrsni() {
		return jeliNezavrsni;
	}

	public boolean isJeliBroj() {
		return jeliBroj;
	}

	public void setJeliBroj(boolean jeliBroj) {
		this.jeliBroj = jeliBroj;
	}

	public int getRedakZavrsnog() {
		return redakZavrsnog;
	}

	public void setRedakZavrsnog(int redakZavrsnog) {
		this.redakZavrsnog = redakZavrsnog;
	}
	
}
