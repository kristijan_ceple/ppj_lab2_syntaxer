import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;

public class SemantickiAnalizator {
	
	public static Cvor<Znak> glava;
	public static Znak radniZnak;
	public static Set<DeklaracijaFunkcije> sveDefiniraneFunkcije = new HashSet<>();
	public static boolean imaIspravanMain = false;
	
	public static void main(String[] args) {
		
		ArrayList<Cvor<Znak>> sviCvorovi = new ArrayList<Cvor<Znak>>();
		
		Scanner sc = new Scanner(System.in);
		ArrayList<String> ulaz = new ArrayList<String>();
		while(sc.hasNextLine())
			ulaz.add(sc.nextLine());
		int indexClana = 0;
		int brojPraznina=1;
		Cvor<Znak> cvoric;
		Cvor<Znak> trenutniCvor;
		int brojacObradjenihCvorova=0;
		
		//Prolazak kroz stablo i biljezenje svih razina
		while(brojacObradjenihCvorova<ulaz.size()) {
			
			if(indexClana==0) {
				//rezanje < i >
				String imeGlave = ulaz.get(indexClana).substring(1, ulaz.get(indexClana).length()-1);
				Znak znak = new Znak(imeGlave,true);
				glava = new Cvor<Znak>(znak ,indexClana);
				sviCvorovi.add(glava);
			
				for(int i=indexClana+1;i<ulaz.size();++i) {
					if(brojPraznina(ulaz.get(i))==brojPraznina) {
						String imeZnaka = ulaz.get(i).substring(brojPraznina(ulaz.get(i)));
						if((imeZnaka.toCharArray())[0]=='<') {
							imeZnaka = imeZnaka.substring(1,imeZnaka.length()-1);
							Znak znakic = new Znak(imeZnaka,true);
							cvoric = new Cvor<Znak>(znakic, i);
							glava.dodajDijete(cvoric);
							sviCvorovi.add(cvoric);
							cvoric.setRodCvor(glava);
						} else {
							String[] dijeloviNezavrsnogZnaka = ulaz.get(i).substring(brojPraznina(ulaz.get(i))).split(" ");
							Znak znakic = new Znak(dijeloviNezavrsnogZnaka[0], Integer.parseInt(dijeloviNezavrsnogZnaka[1]), dijeloviNezavrsnogZnaka[2]);
							cvoric = new Cvor<Znak>(znakic, i);
							glava.dodajDijete(cvoric);
							sviCvorovi.add(cvoric);
							cvoric.setRodCvor(glava);
						}
					}
				}
				
				indexClana++;
				brojPraznina++;
				brojacObradjenihCvorova++;
			} else {
				
				trenutniCvor = pronadjiCvor(sviCvorovi, indexClana ,glava ,brojPraznina);
				
				for ( int i = indexClana+1;i<ulaz.size() && brojPraznina(ulaz.get(i))>=brojPraznina;++i) {
					if(brojPraznina(ulaz.get(i))==brojPraznina) {
						String imeZnaka = ulaz.get(i).substring(brojPraznina(ulaz.get(i)));
						if((imeZnaka.toCharArray())[0]=='<') {
							imeZnaka = imeZnaka.substring(1,imeZnaka.length()-1);
							Znak znakic = new Znak(imeZnaka,true);
//							System.out.println(imeZnaka);
							cvoric = new Cvor<Znak>(znakic, i);
							trenutniCvor.dodajDijete(cvoric);
							sviCvorovi.add(cvoric);
							cvoric.setRodCvor(trenutniCvor);
						} else {
							String[] dijeloviNezavrsnogZnaka = ulaz.get(i).substring(brojPraznina(ulaz.get(i))).split(" ", 3);
//							Znak znakic;
//							if(imeZnaka.contains("NIZ_ZNAKOVA")) {
//								znakic = new Znak(dijeloviNezavrsnogZnaka[0], Integer.parseInt(dijeloviNezavrsnogZnaka[1]), dijeloviNezavrsnogZnaka[2]);
//							} else {
//								znakic = new Znak(dijeloviNezavrsnogZnaka[0], Integer.parseInt(dijeloviNezavrsnogZnaka[1]), dijeloviNezavrsnogZnaka[2]);
//							}
							Znak znakic = new Znak(dijeloviNezavrsnogZnaka[0], Integer.parseInt(dijeloviNezavrsnogZnaka[1]), dijeloviNezavrsnogZnaka[2]);
							cvoric = new Cvor<Znak>(znakic, i);
							trenutniCvor.dodajDijete(cvoric);
							sviCvorovi.add(cvoric);
							cvoric.setRodCvor(trenutniCvor);
						}
					}
				}
				
				//pronaci indeks iduceg cvora
				boolean nijeNadjenIduci=true;
				for(int i=indexClana+1;i<ulaz.size() && nijeNadjenIduci;++i) {
					if(brojPraznina(ulaz.get(i))==brojPraznina-1) {
						indexClana=i;
						nijeNadjenIduci=false;
					}
				}
				if(nijeNadjenIduci) {
					brojPraznina++;
					for(int i=0;i<ulaz.size();++i)
						if(brojPraznina(ulaz.get(i))==brojPraznina-1) {
							indexClana = i;
							break;
						}
				}
				brojacObradjenihCvorova++;
				
			}	
		}
		sc.close();

		kreirajStabloDjelokruga(glava ,sviCvorovi);
		prijevodnaJedinica(glava);
		
		if(!imaIspravanMain) {
			System.out.println("main");
			System.exit(-1);
		}
		provjeraDefinicijaFunkcija();
	}
	
	public static void prijevodnaJedinica(Cvor<Znak> radniCvor) {	
		Cvor<Znak> dijeteCvor = radniCvor.getDjeca().get(0);
		radniZnak = dijeteCvor.getVrijednost();
		
		if(radniZnak.getImeZnaka().equals("vanjska_deklaracija"))
			vanjskaDeklaracija(dijeteCvor);
		else if (radniZnak.getImeZnaka().equals("prijevodna_jedinica")) {
			prijevodnaJedinica(dijeteCvor);	//TODO:  moguce potrebno dodati parametre
			dijeteCvor = radniCvor.getDjeca().get(1);
			vanjskaDeklaracija(dijeteCvor);
		}
	}
	
	public static void vanjskaDeklaracija(Cvor<Znak> radniCvor) {
		Cvor<Znak> dijeteCvor = radniCvor.getDjeca().get(0);
		radniZnak = dijeteCvor.getVrijednost();
		
		if(radniZnak.getImeZnaka().equals("definicija_funkcije"))
			definicijaFunkcije(dijeteCvor);
		
		else if(radniZnak.getImeZnaka().equals("deklaracija"))
			deklaracija(dijeteCvor);
	}
	
	public static void definicijaFunkcije(Cvor<Znak> radniCvor) {
		Cvor<Znak> dijeteCvor = radniCvor.getDjeca().get(0);               //1.
		radniZnak = dijeteCvor.getVrijednost(); // radniZnak = ime_tipa
		
		imeTipa(dijeteCvor);
		
		String s = dijeteCvor.atributi.get("tip").toString();
		if(!(s == "INT" || s == "CHAR" || s == "VOID")) {  			//2
			pogreska(radniCvor);
		}
		
		dijeteCvor = radniCvor.getDjeca().get(1);       		//3
		radniZnak = dijeteCvor.getVrijednost(); // radniZnak = IDN
		
		for(DeklaracijaFunkcije pom : sveDefiniraneFunkcije) {
			//function can't be defined twice
			if(pom.getIme().equals(radniZnak.getZavrsniUProgramu()) && pom.definiranaFunkcija) {
				pogreska(radniCvor);
			}
		}
		
		// Let's check if there are any other identifiers(variable declarations) present
		Djelokrug globalScope = glava.getReferenca().getVrijednost();
		for(Varijabla currVar : globalScope.getListaVarijabla()) {
			if(currVar.getIme().equals(radniZnak.getZavrsniUProgramu())) {
				pogreska(radniCvor);
			}
		}
		
		if(radniCvor.getDjeca().get(3).getVrijednost().getImeZnaka().equals("KR_VOID")) { //4.1
			if(!imaIspravanMain) {  // PROVJERA MAINA 4.4.7
				Cvor <Znak> pomCvorPov = radniCvor.getDjeca().get(0);
				Cvor <Znak> pomCvorParam = radniCvor.getDjeca().get(3);
				if(dijeteCvor.getVrijednost().getZavrsniUProgramu().equals("main") && 
			       pomCvorPov.atributi.get("tip") == Tipovi.INT &&
			       pomCvorParam.getVrijednost().getZavrsniUProgramu().equals("void")) {
					imaIspravanMain = true;
				}
			}
			
			for(DeklaracijaFunkcije pom : globalScope.getListaDeklaracija()) {
				if(pom.getIme().equals(radniZnak.getZavrsniUProgramu())) { //3.1 i 4.1
					if(!(pom.getPovratniTip().equals(radniCvor.getDjeca().get(0).atributi.get("tip")) && //4.1
					   pom.getTipoviParametara().size() == 1 && pom.getTipoviParametara().toArray()[0]==Tipovi.VOID)) {
							pogreska(radniCvor);
					} else {
						globalScope.getListaDeklaracija().remove(pom);
						break;
					}
				}
			}
			ArrayList<Tipovi> pomList = new ArrayList<>();    //5.1
			pomList.add(Tipovi.VOID);
			DeklaracijaFunkcije pomDek = new DeklaracijaFunkcije(dijeteCvor.getVrijednost().getZavrsniUProgramu(),
					   											 (Tipovi) radniCvor.getDjeca().get(0).atributi.get("tip"),
					   											 pomList);
			pomDek.definiranaFunkcija = true;
			sveDefiniraneFunkcije.add(pomDek);
			
			dijeteCvor = radniCvor.getDjeca().get(5);   //6.1
//			Djelokrug tmp = dijeteCvor.getReferenca().getVrijednost();
//			tmp.getListaDeklaracija().add(pomDek);
			
			slozenaNaredba(dijeteCvor);
		} else if(radniCvor.getDjeca().get(3).getVrijednost().getImeZnaka().equals("lista_parametara")) {
			dijeteCvor = radniCvor.getDjeca().get(3);  //4.2
			listaParametara(dijeteCvor);
						
			radniZnak = radniCvor.getDjeca().get(1).getVrijednost();
			for(DeklaracijaFunkcije pom : globalScope.getListaDeklaracija()) { //5.2
				if(pom.getIme().equals(radniZnak.getZavrsniUProgramu())) {
					if(!(pom.getPovratniTip().equals(radniCvor.getDjeca().get(0).atributi.get("tip")) &&
					  (radniCvor.getDjeca().get(3).atributi.get("tipovi").equals(pom.tipoviParametara)))) {
							pogreska(radniCvor);
					}
				}
			}
			// pomListImena.size() == pomListTipovi.size()
			List<Tipovi> pomListTipova = (List<Tipovi>) radniCvor.getDjeca().get(3).atributi.get("tipovi"); //6.2
			List<String> pomListImena = (List<String>) radniCvor.getDjeca().get(3).atributi.get("imena");
			DeklaracijaFunkcije pomDek = new DeklaracijaFunkcije(radniCvor.getDjeca().get(1).getVrijednost().getZavrsniUProgramu(),
					   											 (Tipovi) radniCvor.getDjeca().get(0).atributi.get("tip"),
					   											 pomListTipova);
			pomDek.definiranaFunkcija = true;
			sveDefiniraneFunkcije.add(pomDek);
			
			dijeteCvor = radniCvor.getDjeca().get(5); //7.2
			Djelokrug tmp = dijeteCvor.getReferenca().getVrijednost();
//			tmp.getListaDeklaracija().add(pomDek);
			
			List<String> allParamNames = tmp.getListaVarijabla()
					.parallelStream()
					.map(Varijabla::getIme)
					.collect(Collectors.toList());
			
			String functionName = radniCvor.getDjeca().get(1).getVrijednost().getZavrsniUProgramu();
			for( int i = 0; i < pomListTipova.size(); i++) {
				if(allParamNames.contains(pomListImena.get(i))) {
					// More parameters that have the same name lol
					pogreska(radniCvor);
				} else {
					tmp.getListaVarijabla().add(new Varijabla(pomListImena.get(i), pomListTipova.get(i)));
				}
			}
			slozenaNaredba(dijeteCvor);
		}
	}

	private static void slozenaNaredba(Cvor<Znak> radniCvor) {
		Cvor<Znak> dijeteCvor = radniCvor.getDjeca().get(1);  
		radniZnak = dijeteCvor.getVrijednost();
		
		if(radniZnak.getImeZnaka().equals("lista_naredbi")) { //1.1
			listaNaredbi(dijeteCvor);
		}
		
		else if(radniZnak.getImeZnaka().equals("lista_deklaracija")) {
			listaDeklaracija(dijeteCvor); //1.2
			
			dijeteCvor = radniCvor.getDjeca().get(2);
			listaNaredbi(dijeteCvor);
			
		}
	}

	private static void listaDeklaracija(Cvor<Znak> radniCvor) {
		Cvor<Znak> dijeteCvor = radniCvor.getDjeca().get(0);  
		radniZnak = dijeteCvor.getVrijednost();
		
		if(radniZnak.getImeZnaka().equals("deklaracija"))
			deklaracija(dijeteCvor);
		
		else if(radniZnak.getImeZnaka().equals("lista_deklaracija")) {
			listaDeklaracija(dijeteCvor);   
			dijeteCvor = radniCvor.getDjeca().get(1);
			deklaracija(dijeteCvor); 
		}
		
	}

	private static void listaNaredbi(Cvor<Znak> radniCvor) {
		Cvor<Znak> dijeteCvor = radniCvor.getDjeca().get(0);  
		radniZnak = dijeteCvor.getVrijednost();
		
		if(radniZnak.getImeZnaka().equals("naredba")) //1.1
			naredba(dijeteCvor);
		
		else if(radniZnak.getImeZnaka().equals("lista_naredbi")) {
			listaNaredbi(dijeteCvor);     //1.2
			dijeteCvor = radniCvor.getDjeca().get(1);
			naredba(dijeteCvor); 		//2.2
		}
		
	}

	private static void naredba(Cvor<Znak> radniCvor) {
		Cvor<Znak> dijeteCvor = radniCvor.getDjeca().get(0);  
		radniZnak = dijeteCvor.getVrijednost();
		
		if(radniZnak.getImeZnaka().equals("slozena_naredba"))
			slozenaNaredba(dijeteCvor);
		else if(radniZnak.getImeZnaka().equals("izraz_naredba"))
			izrazNaredba(dijeteCvor);
		else if(radniZnak.getImeZnaka().equals("naredba_grananja"))
			naredbaGrananja(dijeteCvor);
		else if(radniZnak.getImeZnaka().equals("naredba_petlje"))
			naredbaPetlje(dijeteCvor);
		else if(radniZnak.getImeZnaka().equals("naredba_skoka"))
			naredbaSkoka(dijeteCvor);
		
	}

	private static void naredbaSkoka(Cvor<Znak> radniCvor) {
		Cvor<Znak> dijeteCvor = radniCvor.getDjeca().get(0);  
		radniZnak = dijeteCvor.getVrijednost();
		
		if(radniZnak.getImeZnaka().equals("KR_CONTINUE") || //1.1
		   radniZnak.getImeZnaka().equals("KR_BREAK")) {
			   Cvor<Znak> tmpCvor = dijeteCvor;
			   boolean imaPetlje = false;
			   while(tmpCvor.getRodCvor() != null) {
				   tmpCvor = tmpCvor.getRodCvor();
				   if(tmpCvor.getVrijednost().getImeZnaka().equals("naredba_petlje")) {
					   imaPetlje = true;
					   break;
				   }
			   }
			   if(!imaPetlje)
				   pogreska(radniCvor);
			   
			   return;
		  }
		
		dijeteCvor = radniCvor.getDjeca().get(1);
		radniZnak = dijeteCvor.getVrijednost();
		
		if(radniZnak.getImeZnaka().equals("TOCKAZAREZ")) { //1.2
		   Cvor<Znak> tmpCvor = dijeteCvor;
		   boolean imaFunkcijuSVoid = false;
		   while(tmpCvor.getRodCvor() != null) {
			   tmpCvor = tmpCvor.getRodCvor();
			   if(tmpCvor.getVrijednost().getImeZnaka().equals("definicija_funkcije")) {
				   if(tmpCvor.getDjeca().get(0).atributi.get("tip").equals(Tipovi.VOID)) {
					   imaFunkcijuSVoid = true;
					   break;
				   }
			   }
		   }
		   if(!imaFunkcijuSVoid)
			   pogreska(radniCvor);
		} else if(radniZnak.getImeZnaka().equals("izraz")) { //1.3
			 
			izraz(dijeteCvor);
			 
			// Now check if func
			Object func = dijeteCvor.atributi.get("func");
			if(func != null) {
				// It was put, so check if it's a function cal without args -- case if TRUE
				boolean funcWithoutArgs = (boolean)func;
				if(funcWithoutArgs) {
					pogreska(radniCvor);
				}
			}
			
			Cvor<Znak> tmpCvor = dijeteCvor;
			 
			// Not sure if DeklaracijaFunkcije or Varijabla or Tipovi
			Object obj = radniCvor.getDjeca().get(1).atributi.get("tip");
//			if(obj == null) {
//				// Object type non-existent? Means an error
//				pogreska(radniCvor);
//			}
			Class<? extends Object> objClass = obj.getClass();
			Tipovi tipOdIzraz;
			
			if(objClass == DeklaracijaFunkcije.class) {
				tipOdIzraz = ((DeklaracijaFunkcije)obj).getPovratniTip();
			} else if(objClass == Varijabla.class) {
				tipOdIzraz = ((Varijabla)obj).getTip();
			} else if(objClass == Tipovi.class) {
				tipOdIzraz = (Tipovi) obj;
			} else {
				throw new IllegalArgumentException("Type not properly handled");
			}
			
			boolean imaFunkcijuSPov = false;
			 
			while(tmpCvor.getRodCvor() != null) {
				tmpCvor = tmpCvor.getRodCvor();
				if(tmpCvor.getVrijednost().getImeZnaka().equals("definicija_funkcije")) {
					if(Pravila.pretvori(tipOdIzraz.toString(),tmpCvor.getDjeca().get(0).atributi.get("tip").toString())) {
						imaFunkcijuSPov = true;
						break;
					}
				}
			}
			
			if(!imaFunkcijuSPov)
				pogreska(radniCvor);
		}
	}

	private static void izraz(Cvor<Znak> radniCvor) {
		Cvor<Znak> dijeteCvor = radniCvor.getDjeca().get(0); 
		radniZnak = dijeteCvor.getVrijednost();
		
		if(radniZnak.getImeZnaka().equals("izraz_pridruzivanja")) {  //1.1
			izrazPridruzivanja(dijeteCvor);
			Object toAdd = dijeteCvor.atributi.get("tip");
			radniCvor.atributi.put("tip", toAdd);
			
			toAdd = dijeteCvor.atributi.get("l-izraz");
			radniCvor.atributi.put("l-izraz", toAdd);
		}
		else if(radniZnak.getImeZnaka().equals("izraz")) {
			izraz(dijeteCvor);			//1.2
			
			dijeteCvor = radniCvor.getDjeca().get(0);  //2.2
			izrazPridruzivanja(dijeteCvor);
			
			Object toAdd = dijeteCvor.atributi.get("tip");
			radniCvor.atributi.put("tip", toAdd);
			
			radniCvor.atributi.put("l-izraz", false);
		}
		
		Object toPut = dijeteCvor.atributi.get("func");
		radniCvor.atributi.put("func", toPut);
	}

	private static void izrazPridruzivanja(Cvor<Znak> radniCvor) {
		Cvor<Znak> dijeteCvor = radniCvor.getDjeca().get(0);  
		radniZnak = dijeteCvor.getVrijednost();
		
		if(radniZnak.getImeZnaka().equals("log_ili_izraz")) {  //1.1
			logIliIzraz(dijeteCvor);
			
			Object toAdd = dijeteCvor.atributi.get("tip");
			radniCvor.atributi.put("tip", toAdd);
			
			toAdd = dijeteCvor.atributi.get("l-izraz");
			radniCvor.atributi.put("l-izraz", toAdd);
			
//			toAdd = dijeteCvor.atributi.get("br-elem");
//			radniCvor.atributi.put("br-elem", toAdd);
		} else if(radniZnak.getImeZnaka().equals("postfiks_izraz")) { //1.2
			radniCvor.atributi.put("l-izraz", false);
			
			postfiksIzraz(dijeteCvor);
			
			// 2.2 --> <postfiks_izraz>.l-izraz = 1 check
			Object toCheck = dijeteCvor.atributi.get("l-izraz");
			if(!(boolean)toCheck) {
				pogreska(radniCvor);
			}
			
			Object toAdd = dijeteCvor.atributi.get("tip");
			radniCvor.atributi.put("tip", toAdd);
			
			dijeteCvor = radniCvor.getDjeca().get(2);
			izrazPridruzivanja(dijeteCvor);
			Tipovi pomTip = (Tipovi) radniCvor.getDjeca().get(0).atributi.get("tip");
			if(!Pravila.assignmentImplicitCast(dijeteCvor.atributi.get("tip").toString(), pomTip.toString())) {
				// Put an error flag and return up top, where the error shall be detected
//				String key = "assignment_error";
//				radniCvor.atributi.put(key, true);
//				return;
				pogreska(radniCvor);
			}
		}
		
		Object toPut = dijeteCvor.atributi.get("func");
		radniCvor.atributi.put("func", toPut);
	}

	private static void logIliIzraz(Cvor<Znak> radniCvor) {
		Cvor<Znak> dijeteCvor = radniCvor.getDjeca().get(0);  
		radniZnak = dijeteCvor.getVrijednost();
		if(radniZnak.getImeZnaka().equals("log_i_izraz")) { // 1.1
			logIIzraz(dijeteCvor);
			
			Object toAdd = dijeteCvor.atributi.get("tip");
			radniCvor.atributi.put("tip", toAdd);
			
			toAdd = dijeteCvor.atributi.get("l-izraz");
			radniCvor.atributi.put("l-izraz", toAdd);
			
//			toAdd = dijeteCvor.atributi.get("br-elem");
//			radniCvor.atributi.put("br-elem", toAdd);
		}
		else if(radniZnak.getImeZnaka().equals("log_ili_izraz")) { //1.2
			radniCvor.atributi.put("tip", Tipovi.INT);
			radniCvor.atributi.put("l-izraz", false);
			logIliIzraz(dijeteCvor);
			
			if(!Pravila.pretvori(dijeteCvor.atributi.get("tip").toString(), Tipovi.INT.toString())) { //2.2
				pogreska(radniCvor);
			}
			
			dijeteCvor = radniCvor.getDjeca().get(2); //3.2
			logIIzraz(dijeteCvor);
			
			if(!Pravila.pretvori(dijeteCvor.atributi.get("tip").toString(), Tipovi.INT.toString())) { //4.2
				pogreska(radniCvor);
			}
		}
		
		Object toPut = dijeteCvor.atributi.get("func");
		radniCvor.atributi.put("func", toPut);
	}

	private static void logIIzraz(Cvor<Znak> radniCvor) {
		Cvor<Znak> dijeteCvor = radniCvor.getDjeca().get(0);  
		radniZnak = dijeteCvor.getVrijednost();
		
		if(radniZnak.getImeZnaka().equals("bin_ili_izraz")) {
			binIliIzraz(dijeteCvor); //1.1
			Object toAdd = dijeteCvor.atributi.get("tip");
			radniCvor.atributi.put("tip", toAdd);
			
			toAdd = dijeteCvor.atributi.get("l-izraz");
			radniCvor.atributi.put("l-izraz", toAdd);
			
//			toAdd = dijeteCvor.atributi.get("br-elem");
//			radniCvor.atributi.put("br-elem", toAdd);
		}
		else if(radniZnak.getImeZnaka().equals("log_i_izraz")) {
			radniCvor.atributi.put("tip", Tipovi.INT);
			radniCvor.atributi.put("l-izraz", false);
			logIIzraz(dijeteCvor);
			
			if(!Pravila.pretvori(dijeteCvor.atributi.get("tip").toString(), Tipovi.INT.toString())) { //2.2
				pogreska(radniCvor);
			}
			
			dijeteCvor = radniCvor.getDjeca().get(2); //3.2
			binIliIzraz(dijeteCvor);
			
			if(!Pravila.pretvori(dijeteCvor.atributi.get("tip").toString(), Tipovi.INT.toString())) { //4.2
				pogreska(radniCvor);
			}
		}
		
		Object toPut = dijeteCvor.atributi.get("func");
		radniCvor.atributi.put("func", toPut);
	}

	private static void binIliIzraz(Cvor<Znak> radniCvor) {
		Cvor<Znak> dijeteCvor = radniCvor.getDjeca().get(0);  
		radniZnak = dijeteCvor.getVrijednost();
		
		if(radniZnak.getImeZnaka().equals("bin_xili_izraz")) {
			binXiliIzraz(dijeteCvor); //1.1
			Object toAdd = dijeteCvor.atributi.get("tip");
			radniCvor.atributi.put("tip", toAdd);
			
			toAdd = dijeteCvor.atributi.get("l-izraz");
			radniCvor.atributi.put("l-izraz", toAdd);
			
//			toAdd = dijeteCvor.atributi.get("br-elem");
//			radniCvor.atributi.put("br-elem", toAdd);
		}
		else if(radniZnak.getImeZnaka().equals("bin_ili_izraz")) {
			radniCvor.atributi.put("tip", Tipovi.INT);
			radniCvor.atributi.put("l-izraz", false);
			binIliIzraz(dijeteCvor);
			
			if(!Pravila.pretvori(dijeteCvor.atributi.get("tip").toString(), Tipovi.INT.toString())) { //2.2
				pogreska(radniCvor);
			}
			
			dijeteCvor = radniCvor.getDjeca().get(2); //3.2
			binXiliIzraz(dijeteCvor);
			
			if(!Pravila.pretvori(dijeteCvor.atributi.get("tip").toString(), Tipovi.INT.toString())) { //4.2
				pogreska(radniCvor);
			}
		}
		
		Object toPut = dijeteCvor.atributi.get("func");
		radniCvor.atributi.put("func", toPut);
	}
	
	private static void binXiliIzraz(Cvor<Znak> radniCvor) {
		Cvor<Znak> dijeteCvor = radniCvor.getDjeca().get(0);  
		radniZnak = dijeteCvor.getVrijednost();
		
		if(radniZnak.getImeZnaka().equals("bin_i_izraz")) {
			binIIzraz(dijeteCvor); //1.1
			Object toAdd = dijeteCvor.atributi.get("tip");
			radniCvor.atributi.put("tip", toAdd);
			
			toAdd = dijeteCvor.atributi.get("l-izraz");
			radniCvor.atributi.put("l-izraz", toAdd);
			
//			toAdd = dijeteCvor.atributi.get("br-elem");
//			radniCvor.atributi.put("br-elem", toAdd);
		}
		else if(radniZnak.getImeZnaka().equals("bin_ili_izraz")) {
			radniCvor.atributi.put("tip", Tipovi.INT);
			radniCvor.atributi.put("l-izraz", false);
			binXiliIzraz(dijeteCvor);
			
			if(!Pravila.pretvori(dijeteCvor.atributi.get("tip").toString(), Tipovi.INT.toString())) { //2.2
				pogreska(radniCvor);
			}
			
			dijeteCvor = radniCvor.getDjeca().get(2); //3.2
			binIIzraz(dijeteCvor);
			
			if(!Pravila.pretvori(dijeteCvor.atributi.get("tip").toString(), Tipovi.INT.toString())) { //4.2
				pogreska(radniCvor);
			}
		}	
		
		Object toPut = dijeteCvor.atributi.get("func");
		radniCvor.atributi.put("func", toPut);
	}
	
	private static void binIIzraz(Cvor<Znak> radniCvor) {
		Cvor<Znak> dijeteCvor = radniCvor.getDjeca().get(0);  
		radniZnak = dijeteCvor.getVrijednost();
		
		if(radniZnak.getImeZnaka().equals("jednakosni_izraz")) {
			jednakosniIzraz(dijeteCvor); //1.1
			Object toAdd = dijeteCvor.atributi.get("tip");
			radniCvor.atributi.put("tip", toAdd);
			
			toAdd = dijeteCvor.atributi.get("l-izraz");
			radniCvor.atributi.put("l-izraz", toAdd);
			
//			toAdd = dijeteCvor.atributi.get("br-elem");
//			radniCvor.atributi.put("br-elem", toAdd);
		}
		else if(radniZnak.getImeZnaka().equals("bin_i_izraz")) {
			radniCvor.atributi.put("tip", Tipovi.INT);
			radniCvor.atributi.put("l-izraz", false);
			binIIzraz(dijeteCvor);
			
			if(!Pravila.pretvori(dijeteCvor.atributi.get("tip").toString(), Tipovi.INT.toString())) { //2.2
				pogreska(radniCvor);
			}
			
			dijeteCvor = radniCvor.getDjeca().get(2); //3.2
			jednakosniIzraz(dijeteCvor);
			
			if(!Pravila.pretvori(dijeteCvor.atributi.get("tip").toString(), Tipovi.INT.toString())) { //4.2
				pogreska(radniCvor);
			}
		}
		
		Object toPut = dijeteCvor.atributi.get("func");
		radniCvor.atributi.put("func", toPut);
	}

	private static void naredbaPetlje(Cvor<Znak> radniCvor) {
		
		ArrayList<Cvor<Znak>> djeca = radniCvor.getDjeca();
		
		if(djeca.size()==5) {
			
			izraz(djeca.get(2));
			if(!Pravila.pretvori(djeca.get(2).atributi.get("tip").toString(),Tipovi.INT.toString()))
				pogreska(radniCvor);
			naredba(djeca.get(4));
			
		} else if(djeca.size()==6) {
			
			izrazNaredba(djeca.get(2));
			izrazNaredba(djeca.get(3));
			if(!Pravila.pretvori(djeca.get(3).atributi.get("tip").toString(),Tipovi.INT.toString()))
				pogreska(radniCvor);
			naredba(djeca.get(5));
			
		} else if(djeca.size()==7) {
			
			izrazNaredba(djeca.get(2));
			izrazNaredba(djeca.get(3));
			
			Object toCheckImplicitCast = djeca.get(3).atributi.get("tip");
			if(toCheckImplicitCast == null) {
				pogreska(radniCvor);
			} else if(!Pravila.pretvori(toCheckImplicitCast.toString(),Tipovi.INT.toString()))
				pogreska(radniCvor);
			
			izraz(djeca.get(4));
			naredba(djeca.get(6));
			
		}
		
	}

	private static void naredbaGrananja(Cvor<Znak> radniCvor) {
		
		ArrayList<Cvor<Znak>> djeca = radniCvor.getDjeca();
		
		if(djeca.size()==5) {
			
			izraz(djeca.get(2));
			if(!Pravila.pretvori(djeca.get(2).atributi.get("tip").toString(),Tipovi.INT.toString()))
				pogreska(radniCvor);
			naredba(djeca.get(4));
			
		} else if(djeca.size()==7) {
			
			izraz(djeca.get(2));
			if(!Pravila.pretvori(djeca.get(2).atributi.get("tip").toString(),Tipovi.INT.toString()))
				pogreska(radniCvor);
			naredba(djeca.get(4));
			naredba(djeca.get(6));
		}
		
	}

	private static void izrazNaredba(Cvor<Znak> radniCvor) {
		
		ArrayList<Cvor<Znak>> djeca = radniCvor.getDjeca();
		
		if(djeca.size()==1) {
			
			radniCvor.atributi.put("tip", Tipovi.INT);
			
		} else if(djeca.size()==2) {
			
			izraz(djeca.get(0));
			
			radniCvor.atributi.put("tip", djeca.get(0).atributi.get("tip"));	
			
		}
		
	}

	public static void deklaracija(Cvor<Znak> radniCvor) {
		Cvor<Znak> dijeteCvor = radniCvor.getDjeca().get(0);  
		radniZnak = dijeteCvor.getVrijednost();
		imeTipa(dijeteCvor);
		
		dijeteCvor = radniCvor.getDjeca().get(1);
		
		dijeteCvor.atributi.put("ntip", radniCvor.getDjeca().get(0).atributi.get("tip"));
		listaInitDeklaratora(dijeteCvor);
		
	}
	
	public static void imeTipa(Cvor<Znak> radniCvor) {
		
		ArrayList<Cvor<Znak>> djeca = radniCvor.getDjeca();
		
		if(djeca.size()==1) {
			
			specifikatorTipa(djeca.get(0));
			
			radniCvor.atributi.put("tip", djeca.get(0).atributi.get("tip"));
			
		} else {
			
			specifikatorTipa(djeca.get(1));
			if((Tipovi) djeca.get(1).atributi.get("tip")==Tipovi.VOID)
				pogreska(radniCvor);
			
			radniCvor.atributi.put("tip", Pravila.pretvoriUKonstantu((Tipovi) djeca.get(1).atributi.get("tip")));
			
			}
		
	}	

	public static void postfiksIzraz(Cvor<Znak> radniCvor) {
		
		ArrayList<Cvor<Znak>> djeca = radniCvor.getDjeca();
		
		if(djeca.size()==1) {
			//produkcija u primarni_izraz
			Cvor<Znak> zerothChild = radniCvor.getDjeca().get(0);
			
			primarniIzraz(djeca.get(0));
		
			radniCvor.atributi.put("func", zerothChild.atributi.get("func"));
			radniCvor.atributi.put("tip", zerothChild.atributi.get("tip"));
			radniCvor.atributi.put("l-izraz", zerothChild.atributi.get("l-izraz"));
//			radniCvor.atributi.put("br-elem", zerothChild.atributi.get("br-elem"));
			radniCvor.atributi.put("name", zerothChild);
			
		} else if(djeca.size()==2) {
			
			postfiksIzraz(djeca.get(0));
			// provjeri
			boolean tmp = (boolean)djeca.get(0).atributi.get("l-izraz");
			boolean tmp2 = Pravila.pretvori(djeca.get(0).atributi.get("tip").toString(),Tipovi.INT.toString());
			if(!tmp)
				pogreska(radniCvor);
			if(!tmp2)
				pogreska(radniCvor);
			
			radniCvor.atributi.put("tip", Tipovi.INT);
			radniCvor.atributi.put("l-izraz", false);
			
		} else if(djeca.size()==3) {
			postfiksIzraz(djeca.get(0));
			Set<Tipovi> listaParametara = new HashSet<>();
			listaParametara.add(Tipovi.VOID);
			
			DeklaracijaFunkcije deklFun = (DeklaracijaFunkcije)djeca.get(0).atributi.get("tip");
			if(deklFun == null || deklFun.getTipoviParametara().toArray()[0]!=Tipovi.VOID) {
				pogreska(radniCvor);
			}
			
			radniCvor.atributi.put("tip", ((DeklaracijaFunkcije)djeca.get(0).atributi.get("tip")).getPovratniTip());
			radniCvor.atributi.put("l-izraz", false);
			
			
		} else if(djeca.size()==4) {
			
			if(djeca.get(2).getVrijednost().getImeZnaka().equals("lista_argumenata")) {
			
				postfiksIzraz(djeca.get(0));
				listaArgumenata(djeca.get(2));
				
				/**
				 * Here lies the problem. We just need to grab the nearest same-name var or function
				 */
				String functionName = djeca.get(0).getDjeca().get(0).getDjeca().get(0).getVrijednost().getZavrsniUProgramu();
				// Now let's retrieve the signature of that function
				// First try to find the nearest declaration
//				DeklaracijaFunkcije func = (DeklaracijaFunkcije)retrieveFromNearestScope(functionName, radniCvor, "fun");
				Object retrieved = retrieveFromNearestScope(radniCvor, functionName);
				
				// First check if we've retrieved some trashy var
				// Otherwise, if no declaration was found, attempt to find a definition in the global scope
				if(retrieved == null) {
					for(DeklaracijaFunkcije currFuncf : sveDefiniraneFunkcije) {
						if(currFuncf.ime.equals(functionName)) {
							retrieved = currFuncf;
						}
					}
				} else if(retrieved.getClass()!=DeklaracijaFunkcije.class) {
					pogreska(radniCvor);
				}
				
				// If still no function was found then it doesn't exist lmao and that's an error
				// Here we check the signature
				if(retrieved == null || !Pravila.mozeCastGrupno((List<Tipovi>)djeca.get(2).atributi.get("tipovi"),
						((DeklaracijaFunkcije)retrieved).getTipoviParametara())) {
					pogreska(radniCvor);
				}
				
				Object toRetrieve = djeca.get(0).atributi.get("tip");
				Object toPutType = null;
				if(toRetrieve.getClass()==DeklaracijaFunkcije.class) {
					toPutType = ((DeklaracijaFunkcije)toRetrieve).getPovratniTip();
				} else if(toRetrieve.getClass()==Varijabla.class) {
					toPutType = ((Varijabla)toRetrieve).getTip();
				} else {
					toPutType = toRetrieve;
				}
				radniCvor.atributi.put("tip", toPutType);
				radniCvor.atributi.put("l-izraz", false);
				
			} else {
				
				postfiksIzraz(djeca.get(0));
				if(!(djeca.get(0).atributi.get("tip").toString().contains("NIZ")))
					pogreska(radniCvor);
				izraz(djeca.get(2));
				if(!(Pravila.pretvori(djeca.get(2).atributi.get("tip").toString(),Tipovi.INT.toString())))
					pogreska(radniCvor);	

				Tipovi tmp = (Tipovi)djeca.get(0).atributi.get("tip");
				String resString = tmp.toString().substring(4, tmp.toString().length());
				Tipovi resType = Tipovi.valueOf(resString);
				
				radniCvor.atributi.put("tip", resType);
				radniCvor.atributi.put("l-izraz", !Pravila.jeliTipKonstanta((Tipovi)radniCvor.atributi.get("tip")));
				
			}
			
		}
	}
	
	private static void listaIzrazaPridruzivanja(Cvor<Znak> cvor) {
		
		ArrayList<Cvor<Znak>> djeca = cvor.getDjeca();
		
		if(djeca.size()==1) {
			
			ArrayList<Tipovi> tipovi = new ArrayList<Tipovi>();
			
			izrazPridruzivanja(djeca.get(0));
			
			tipovi.add((Tipovi) djeca.get(0).atributi.get("tip"));
			cvor.atributi.put("tipovi", tipovi);
			cvor.atributi.put("br-elem",1);
			
		} else if (djeca.size()==3) {
			
			listaIzrazaPridruzivanja(djeca.get(0));
			izrazPridruzivanja(djeca.get(2));
			
			ArrayList<Tipovi> tipovi = (ArrayList<Tipovi>)djeca.get(0).atributi.get("tipovi");
			tipovi.add((Tipovi) djeca.get(2).atributi.get("tip"));
			cvor.atributi.put("tipovi", tipovi);
			
			cvor.atributi.put("br-elem", ((int)cvor.getDjeca().get(0).atributi.get("br-elem")+1));
		}
	}
	
	private static void listaParametara(Cvor<Znak> cvor) {
		
		ArrayList<Cvor<Znak>> djeca = cvor.getDjeca();
		
		if(djeca.size()==1) {
			
			ArrayList<Tipovi> tipovi = new ArrayList<Tipovi>();
			ArrayList<String> imena = new ArrayList<String>();
			
			deklaracijaParametra(djeca.get(0));
			
			tipovi.add((Tipovi) djeca.get(0).atributi.get("tip"));
			cvor.atributi.put("tipovi", tipovi);
			
			imena.add((String) djeca.get(0).atributi.get("ime"));
			cvor.atributi.put("imena", imena);
			
		} else if(djeca.size()==3) {
			
			listaParametara(djeca.get(0));
			deklaracijaParametra(djeca.get(2));
			
			ArrayList<String> imena = (ArrayList<String>)djeca.get(0).atributi.get("imena");
			
			if(imena.contains(djeca.get(2).atributi.get("ime")))
				pogreska(cvor);
			
			ArrayList<Tipovi> tipovi = (ArrayList<Tipovi>)djeca.get(0).atributi.get("tipovi");
			tipovi.add((Tipovi) djeca.get(2).atributi.get("tip"));
			cvor.atributi.put("tipovi", tipovi);
			
			imena.add((String) djeca.get(2).atributi.get("ime"));
			cvor.atributi.put("imena", imena);
			
		}
		
	}

	
	private static void listaInitDeklaratora(Cvor<Znak> cvor) {
			
		ArrayList<Cvor<Znak>> djeca = cvor.getDjeca();
		
		if(djeca.size()==1) {
			
			djeca.get(0).atributi.put("ntip", cvor.atributi.get("ntip"));
			initDeklarator(djeca.get(0));
			
		} else if(djeca.size()==3) {
			
			djeca.get(0).atributi.put("ntip", cvor.atributi.get("ntip"));
			listaInitDeklaratora(djeca.get(0));
			
			djeca.get(2).atributi.put("ntip", cvor.atributi.get("ntip"));
			initDeklarator(djeca.get(2));
		}
		
	}
	
	
	private static void initDeklarator(Cvor<Znak> radniCvor) {
		
		ArrayList<Cvor<Znak>> djeca = radniCvor.getDjeca();
		
		if(djeca.size()==1) {
			
			// initdeklarator -> IDN
			
			djeca.get(0).atributi.put("ntip", radniCvor.atributi.get("ntip"));
			izravniDeklarator(djeca.get(0));
			
			// Not sure if DeklaracijaFunkcije or Varijabla or Tipovi
			Object obj = djeca.get(0).atributi.get("tip");
			Class<? extends Object> objClass = obj.getClass();
			Tipovi type;
			
			if(objClass == DeklaracijaFunkcije.class) {
				type = ((DeklaracijaFunkcije)obj).getPovratniTip();
			} else if(objClass == Varijabla.class) {
				type = ((Varijabla)obj).getTip();
			} else if(objClass == Tipovi.class) {
				type = (Tipovi) obj;
			} else {
				throw new IllegalArgumentException("Type not properly handled");
			}
			
			if(Pravila.jeliTipKonstanta(type)) {
				pogreska(radniCvor);
			}
			
		} else if(djeca.size()== 3) {
			
			djeca.get(0).atributi.put("ntip", radniCvor.atributi.get("ntip"));
			izravniDeklarator(djeca.get(0));
			inicijalizator(djeca.get(2));
			
			// Check if x is a func declaration. If yes, pogreska
			Object toGet = djeca.get(2).atributi.get("func");
			if(toGet != null) {
				// Now check if function declaration, if yes then pogreska(radniCvor)
				if((boolean)toGet) {
					pogreska(radniCvor);
				}
			}
			
			// Not sure if DeklaracijaFunkcije or Varijabla or Tipovi
			Object obj = djeca.get(0).atributi.get("tip");
			Class<? extends Object> objClass = obj.getClass();
			Tipovi type;
			
			if(objClass == DeklaracijaFunkcije.class) {
				type = ((DeklaracijaFunkcije)obj).getPovratniTip();
			} else if(objClass == Varijabla.class) {
				type = ((Varijabla)obj).getTip();
			} else if(objClass == Tipovi.class) {
				type = (Tipovi) obj;
			} else {
				throw new IllegalArgumentException("Type not properly handled");
			}
			
			if(!Pravila.jeliTipNiz(type)) {
				Tipovi tip1 = (Tipovi)djeca.get(2).atributi.get("tip");
				if(tip1 == null || !Pravila.pretvori(tip1.toString(),Pravila.pretvoriUObicni(type).toString()))
					pogreska(radniCvor);
				
			} else {
				// First check if br-elem even exists at the right side, and check that
				// it's not another NIZ, unless it's a CONST_NIZ
				Cvor<Znak> secondChild = djeca.get(2);
				Cvor<Znak> zerothChild = djeca.get(0);
				
				Object secondChildElemQuantNum = secondChild.atributi.get("br-elem");
				// Now need to check if the var at the right side even is an array
				if(secondChildElemQuantNum == null) {
					pogreska(radniCvor);
				}
				
				int tmp1 = (int)secondChildElemQuantNum;
				int tmp2 = (int)zerothChild.atributi.get("br-elem");
				if(!(tmp1 <= tmp2)) {
					pogreska(radniCvor);
				}
				List<Tipovi> tipovi = (List<Tipovi>)djeca.get(2).atributi.get("tipovi");
				for(Tipovi tip : tipovi)
					if(!Pravila.pretvori(tip.toString(),Pravila.pretvoriUObicni(((Tipovi)djeca.get(0).atributi.get("tip"))).toString()))
						pogreska(radniCvor);
			}
		}
		
		Djelokrug djelokrug = vratiDjelokrug(radniCvor);
		if(djeca.size()==3) {
			// Need to set defined to true, but can't tell if var or func (???)
			String varOrFun = (String)djeca.get(0).atributi.get("varOrFun");
			if(varOrFun.equals("var")) {
				List<Varijabla> variableList = djelokrug.getListaVarijabla();
				variableList.get(variableList.size()-1).setDefined(true);
			} else if(varOrFun.equals("fun")) {
				List<DeklaracijaFunkcije> functionsList = djelokrug.getListaDeklaracija();
				functionsList.get(functionsList.size()-1).definiranaFunkcija = true;
			} else {
				throw new IllegalArgumentException("Illegal varOrFun argumet!");
			}
		}
	}

	private static void listaArgumenata(Cvor<Znak> cvor) {
		
		ArrayList<Cvor<Znak>> djeca = cvor.getDjeca();
		
		if(djeca.size()==1) {
			
			ArrayList<Tipovi> tipovi = new ArrayList<Tipovi>();
			
			izrazPridruzivanja(cvor.getDjeca().get(0));
			
			tipovi.add((Tipovi) djeca.get(0).atributi.get("tip"));
			cvor.atributi.put("tipovi", tipovi);
			
		} else {
			
			listaArgumenata(cvor.getDjeca().get(0));
			izrazPridruzivanja(cvor.getDjeca().get(2));
			
			ArrayList<Tipovi> tipovi = (ArrayList<Tipovi>)djeca.get(0).atributi.get("tipovi");
			tipovi.add((Tipovi) djeca.get(2).atributi.get("tip"));
			cvor.atributi.put("tipovi", tipovi);
		}
	}

	private static void primarniIzraz(Cvor<Znak> radniCvor) {
		Cvor<Znak> dijeteCvor = radniCvor.getDjeca().get(0);  
		radniZnak = dijeteCvor.getVrijednost();
		
		boolean func = false;
		if(radniZnak.getImeZnaka().equals("IDN")) {
//			tmp = retrieveFromNearestScope(radniZnak.getZavrsniUProgramu(), radniCvor, "var");
//			if(tmp == null) {
//				// Now try to find a function call?
//				tmp = retrieveFromNearestScope(radniZnak.getZavrsniUProgramu(), radniCvor, "fun");
//				if(tmp == null) {
//					pogreska(radniCvor);
//				} else {
//					toAdd = tmp;
//					func = true;
//				}
//			} else {
//				toAdd = ((Varijabla)tmp).getTip();
//			}
			Object found = retrieveFromNearestScope(radniCvor, radniZnak.getZavrsniUProgramu());
			boolean lexpr = false;
			if(found != null) {
				if(found.getClass() == DeklaracijaFunkcije.class) {
					func = true;
				} else if(found.getClass() == Varijabla.class) {
					Varijabla var = (Varijabla)found; 
					Tipovi type = var.getTip();
					
					// If niz or const, not l-value
					lexpr = !( type.toString().contains("CONST") || type.toString().contains("NIZ") );
					found = var.getTip();  // A lil bit stupid, but it has to be this way
					
					// If DeklaracijaFunkcije, then we leave found = DeklaracijaFunkcije
					// However, if it's a Varijabla, we set found to the corresponding variable type 
				}
			} else {
				// 1.1. -- IDN.ime must be declared!
				pogreska(radniCvor);
			}
			
			radniCvor.atributi.put("func", func);
			radniCvor.atributi.put("tip", found);
			radniCvor.atributi.put("l-izraz", lexpr);
			
			
		} else if(radniZnak.getImeZnaka().equals("BROJ")){
			try {
				String toParse = radniZnak.getZavrsniUProgramu();
				
				if(toParse.startsWith("0x")) {
					Integer.parseInt(toParse.substring(2), 16);
				} else if(toParse.startsWith("0") && toParse.length() >= 2) {
					Integer.parseInt(toParse.substring(1), 8);
				} else {
					Integer.parseInt(toParse);
				}
			} catch(Exception e) {
				pogreska(radniCvor);
			}
			
			radniCvor.atributi.put("tip", Tipovi.INT);
			radniCvor.atributi.put("l-izraz", false);

		} else if(radniZnak.getImeZnaka().equals("ZNAK")) {
			if(!Pravila.jeliDobarChar(dijeteCvor.getVrijednost().getZavrsniUProgramu(), true)) {		// If character doesn't satisfy the requirements
				pogreska(radniCvor);
			}
			
			// After checking let's put attributed
			radniCvor.atributi.put("tip", Tipovi.CHAR);
			radniCvor.atributi.put("l-izraz", false);
			
		} else if(radniZnak.getImeZnaka().equals("NIZ_ZNAKOVA")) {
			if(!Pravila.provjeriNizZnakova(radniZnak.getZavrsniUProgramu()))
					pogreska(radniCvor);
			
			radniCvor.atributi.put("tip", Tipovi.NIZ_CONST_CHAR);
			radniCvor.atributi.put("l-izraz", false);
			// Have to add 1 due to the \0 that is put at the end of C strings
//			radniCvor.atributi.put("br-elem", Pravila.effectiveStringSize(radniZnak.getZavrsniUProgramu()) + 1);
		} else if(radniZnak.getImeZnaka().equals("L_ZAGRADA")) {
			dijeteCvor = radniCvor.getDjeca().get(1);
			izraz(dijeteCvor);
			
			Object toAdd = dijeteCvor.atributi.get("tip");
			radniCvor.atributi.put("tip", toAdd);
			
			toAdd = dijeteCvor.atributi.get("l-izraz");
			radniCvor.atributi.put("l-izraz", toAdd);
		}
	}

	public static void pogreska(Cvor<Znak> radniCvor){

		String ispis ="";

		ispis+="<"+radniCvor.getVrijednost().getImeZnaka()+">";

		ispis+=" ::=";
		for(Cvor<Znak> dijete : radniCvor.getDjeca()) {
			
			if(dijete.getVrijednost().isJeliNezavrsni()) {
				
				ispis+=(" <"+dijete.getVrijednost().getImeZnaka()+">");
			
			} else {
				
				Znak znak =dijete.getVrijednost();
				ispis+=(" "+znak.getImeZnaka()+"("+znak.getRedakZavrsnog()+","+znak.getZavrsniUProgramu()+")");
				
			}
				
		}
		
		System.out.println(ispis);
		System.exit(1);
		
	}
	
	public static Cvor<Znak> pronadjiCvor(ArrayList<Cvor<Znak>> sviCvorovi , int indeksUListi, Cvor<Znak> glava ,int brojacPraznina) {
		for(Cvor<Znak> cvoric : sviCvorovi) {
			if(cvoric.getIndeksUlisti()==indeksUListi)
				return cvoric;
		}
		throw new IllegalStateException("Nije moguce pronaci cvor "+ indeksUListi + " "+ sviCvorovi.toString());
	}
	
	public static int brojPraznina(String niz) {
		
		return niz.length()-niz.trim().length();
	}
	
	public static Cvor<Djelokrug> kreirajStabloDjelokruga(Cvor<Znak> glavaOriginalnogStabla, ArrayList<Cvor<Znak>> sviCvorovi) {
		
		Cvor<Djelokrug> glavaStabla = null;
		boolean nijePostavljenGlobalniDjelokrug = true;
		
		for(Cvor<Znak> trenutniCvor : sviCvorovi)
			
			if(trenutniCvor.getVrijednost().getImeZnaka().equals("prijevodna_jedinica") && nijePostavljenGlobalniDjelokrug) {
				
				nijePostavljenGlobalniDjelokrug = false;
				glavaStabla = new Cvor<Djelokrug>(new ArrayList<Cvor<Djelokrug>>(),new Djelokrug());
				trenutniCvor.setImaSvojDjelokrug(true);
				trenutniCvor.setReferenca(glavaStabla);
				glavaStabla.setSlozenaNaredba(trenutniCvor);
				
			} else if(trenutniCvor.getVrijednost().getImeZnaka().equals("slozena_naredba")) {
				
				Cvor<Znak> roditeljskiCvor = trenutniCvor.getRodCvor();
				
				while(!roditeljskiCvor.isImaSvojDjelokrug()) {
					roditeljskiCvor = roditeljskiCvor.getRodCvor();
				}
				
				trenutniCvor.setImaSvojDjelokrug(true);
				Cvor<Djelokrug> roditeljskiDjelCvor = roditeljskiCvor.getReferenca();
				Cvor<Djelokrug> cvorDjelokruga = new Cvor<Djelokrug>(new ArrayList<Cvor<Djelokrug>>(),new Djelokrug());
				cvorDjelokruga.setRodCvor(roditeljskiDjelCvor);
				roditeljskiDjelCvor.dodajDijete(cvorDjelokruga);
				trenutniCvor.setReferenca(cvorDjelokruga);
				cvorDjelokruga.setSlozenaNaredba(trenutniCvor);
			
		}
		
		return glavaStabla;
	}

	public static Djelokrug vratiDjelokrug(Cvor<Znak> radniCvor) {
		Djelokrug tmp;
		
		while(!radniCvor.isImaSvojDjelokrug()) {
			radniCvor = radniCvor.getRodCvor();
		}
		
		tmp = radniCvor.getReferenca().getVrijednost();
		return tmp;
	}
	
	public static Object retrieveFromNearestScope(Cvor<Znak> radniCvor, String toFind) {
		
		// First find local scope
		Cvor<Djelokrug> scopeNode = localScopeNode(radniCvor);
		
		do {
			// Attempt to find the name in this scope
			List<String> variablesNames = scopeNode.getVrijednost().getListaVarijabla()
					.parallelStream().map(Varijabla::getIme).collect(Collectors.toList());
			
			List<String> functionsNames = scopeNode.getVrijednost().getListaDeklaracija()
					.parallelStream().map(DeklaracijaFunkcije::getIme).collect(Collectors.toList());
			
			if(variablesNames.contains(toFind) && functionsNames.contains(toFind)) {
				return null;
			}
			
			if(variablesNames.contains(toFind)) {
				// Find and return the wanted variable
				for(Varijabla variable : scopeNode.getVrijednost().getListaVarijabla()) {
					if(variable.getIme().equals(toFind)) {
						return variable;
					}
				}
	 		} else if(functionsNames.contains(toFind)) {
	 			for(DeklaracijaFunkcije funcDecl : scopeNode.getVrijednost().getListaDeklaracija()) {
	 				if(funcDecl.getIme().equals(toFind)) {
	 					return funcDecl;
	 				}
	 			}
	 		}

			scopeNode = scopeNode.getRodCvor();
		} while(scopeNode != null);
		
		// Last ditch attempt to find functions in the global defined functions list
		for(DeklaracijaFunkcije tmp_fun : sveDefiniraneFunkcije) {
			if(tmp_fun.getIme().equals(toFind)) {
				return tmp_fun;
			}
		}
		
		return null;
	}
	
	public static Cvor<Djelokrug> localScopeNode(Cvor<Znak> radniCvor) {
		// First find local scope
		while(!radniCvor.isImaSvojDjelokrug()) {
			radniCvor = radniCvor.getRodCvor();
		}
		
		return radniCvor.getReferenca();
	}
	
	public static Object retrieveFromNearestScope(String ime, Cvor<Znak> radniCvor, String varORfun) {
		if(!(varORfun.equals("var") || varORfun.equals("fun"))) {
			throw new IllegalArgumentException("Pisi kako treba klinjo");
		}
		
		Djelokrug tmp;
		//TODO optimize this
		Cvor<Djelokrug> djelokrugCvor = localScopeNode(radniCvor);
		
		do {
			tmp = djelokrugCvor.getVrijednost();
			if(varORfun.equals("var")) {
				for(Varijabla var: tmp.getListaVarijabla()) {
					if(var.getIme().equals(ime)){
						return var;
					}
				}
			} else if(varORfun.equals("fun")) {
				for(DeklaracijaFunkcije fun: tmp.getListaDeklaracija()) {
					if(fun.getIme().equals(ime)){
						return fun;
					}
				}
			}
			
			djelokrugCvor = djelokrugCvor.getRodCvor();
		} while(djelokrugCvor != null);

		// Last ditch attempt to find functions in the global defined functions list
		if(varORfun.equals("fun")) {
			for(DeklaracijaFunkcije tmp_fun : sveDefiniraneFunkcije) {
				if(tmp_fun.getIme().equals(ime)) {
					return tmp_fun;
				}
			}
		}
		
		return null;
	}
	
	public static void multiplikativniIzraz(Cvor<Znak> radniCvor) {
		Cvor<Znak> dijeteCvor = radniCvor.getDjeca().get(0);  
		radniZnak = dijeteCvor.getVrijednost();
		
		if(radniZnak.getImeZnaka().equals("cast_izraz")) {
			castIzraz(dijeteCvor);
			
			Object toAdd = dijeteCvor.atributi.get("tip");
			radniCvor.atributi.put("tip", toAdd);
			
			toAdd = dijeteCvor.atributi.get("l-izraz");
			radniCvor.atributi.put("l-izraz", toAdd);
			
//			toAdd = dijeteCvor.atributi.get("br-elem");
//			radniCvor.atributi.put("br-elem", toAdd);
		} else {
			radniCvor.atributi.put("tip", Tipovi.INT);
			radniCvor.atributi.put("l-izraz", false);
			
			multiplikativniIzraz(dijeteCvor);
			
			if(!Pravila.pretvori(dijeteCvor.atributi.get("tip").toString(), "INT"))
				pogreska(radniCvor);
			
			dijeteCvor = radniCvor.getDjeca().get(2);
			
			castIzraz(dijeteCvor);
			
			if(!Pravila.pretvori(dijeteCvor.atributi.get("tip").toString(), "INT"))
				pogreska(radniCvor);
		}
		
		Object toPut = dijeteCvor.atributi.get("func");
		radniCvor.atributi.put("func", toPut);
	}

	public static void castIzraz(Cvor<Znak> radniCvor) {
		Cvor<Znak> dijeteCvor = radniCvor.getDjeca().get(0);  
		radniZnak = dijeteCvor.getVrijednost();
		
		if(radniZnak.getImeZnaka().equals("unarni_izraz")) {
			unarniIzraz(dijeteCvor);
			
			Object toAdd = dijeteCvor.atributi.get("tip");
			radniCvor.atributi.put("tip", toAdd);
			
			toAdd = dijeteCvor.atributi.get("l-izraz");
			radniCvor.atributi.put("l-izraz", toAdd);
			
//			toAdd = dijeteCvor.atributi.get("br-elem");
//			radniCvor.atributi.put("br-elem", toAdd);
		} else if(radniZnak.getImeZnaka().equals("L_ZAGRADA")) {
			dijeteCvor = radniCvor.getDjeca().get(1);
			imeTipa(dijeteCvor);
			
			Object toAdd = dijeteCvor.atributi.get("tip");
			radniCvor.atributi.put("tip", toAdd);
			
			radniCvor.atributi.put("l-izraz", false);
			
			dijeteCvor = radniCvor.getDjeca().get(3);
			castIzraz(dijeteCvor);
			
			Tipovi pomTip = (Tipovi) radniCvor.getDjeca().get(1).atributi.get("tip");
			if(!Pravila.mozeCast(dijeteCvor.atributi.get("tip").toString(), pomTip.toString()))
				pogreska(radniCvor);
		}
		
		Object toPut = dijeteCvor.atributi.get("func");
		radniCvor.atributi.put("func", toPut);
	}
	
	private static void unarniIzraz(Cvor<Znak> radniCvor) {
		Cvor<Znak> dijeteCvor = radniCvor.getDjeca().get(0);  
		radniZnak = dijeteCvor.getVrijednost();
		
		if(radniZnak.getImeZnaka().equals("postfiks_izraz")) {
			postfiksIzraz(dijeteCvor);
			
			Object toAdd = dijeteCvor.atributi.get("tip");
			radniCvor.atributi.put("tip", toAdd);
			
			toAdd = dijeteCvor.atributi.get("l-izraz");
			radniCvor.atributi.put("l-izraz", toAdd);
			
//			toAdd = dijeteCvor.atributi.get("br-elem");
//			radniCvor.atributi.put("br-elem", toAdd);
		}
		else if(radniZnak.getImeZnaka().equals("unarni_operator")) {
			radniCvor.atributi.put("tip", Tipovi.INT);
			radniCvor.atributi.put("l-izraz", false);
			
			dijeteCvor = radniCvor.getDjeca().get(1);
			castIzraz(dijeteCvor);
			
			if(!Pravila.pretvori(dijeteCvor.atributi.get("tip").toString(),Tipovi.INT.toString()))
				pogreska(radniCvor);
		} else {
			radniCvor.atributi.put("tip", Tipovi.INT);
			radniCvor.atributi.put("l-izraz", false);
			
			dijeteCvor = radniCvor.getDjeca().get(1);
			unarniIzraz(dijeteCvor);
			if(!((boolean) dijeteCvor.atributi.get("l-izraz") && 
				Pravila.pretvori(dijeteCvor.atributi.get("tip").toString(),Tipovi.INT.toString()))) {
				pogreska(radniCvor);
			}
		}
		
		Object toPut = dijeteCvor.atributi.get("func");
		radniCvor.atributi.put("func", toPut);
	}

	public static void specifikatorTipa(Cvor<Znak> radniCvor) {
		Cvor<Znak> dijeteCvor = radniCvor.getDjeca().get(0);
		radniZnak = dijeteCvor.getVrijednost();
		
		if(radniZnak.getImeZnaka().equals("KR_VOID")) {
			radniCvor.atributi.put("tip", Tipovi.VOID);
		} else if(radniZnak.getImeZnaka().equals("KR_CHAR")) {
			radniCvor.atributi.put("tip", Tipovi.CHAR);
		} else if(radniZnak.getImeZnaka().equals("KR_INT")) {
			radniCvor.atributi.put("tip", Tipovi.INT);
		}
		
		
	}

	public static void izravniDeklarator(Cvor<Znak> radniCvor) {
		Cvor<Znak> dijeteCvor = radniCvor.getDjeca().get(0);  
		
		List<Cvor<Znak>> children = radniCvor.getDjeca();
		int childrenNum = children.size();
		
		if(childrenNum == 4) {
			// They differ from the second index
			if(children.get(1).getVrijednost().getImeZnaka().equals("L_UGL_ZAGRADA")) {
				// Prod 1
				// Check that ntip isn't void
				Tipovi ntip = (Tipovi)radniCvor.atributi.get("ntip");
				if(ntip == Tipovi.VOID) {
					pogreska(radniCvor);
				}
				
				// Check if IDN.ime isn't declared in the local scope
				String toCheckName = dijeteCvor.getVrijednost().getZavrsniUProgramu(); 
				Djelokrug tmp = localScope(radniCvor);
				// Now need to check whether the localScope contains this var
				List<String> localScopeNames = tmp.getListaDeklaracija().parallelStream()
					.map((elem) -> elem.getIme())
					.collect(Collectors.toList());
				
				localScopeNames.addAll(
						tmp.getListaVarijabla().parallelStream()
						.map((elem) -> elem.getIme())
						.collect(Collectors.toList())
						);
				
				if(localScopeNames.contains(toCheckName)) {
					pogreska(radniCvor);
				}
				
				// Check BROJ.vrijedost
				int toCheckNum = 0;
				try {
					toCheckNum = Integer.parseInt(radniCvor.getDjeca().get(2).getVrijednost().getZavrsniUProgramu());
				} catch(Exception e) {
					pogreska(radniCvor);
				}
				
				if(toCheckNum > 1024 || toCheckNum <= 0) {
					pogreska(radniCvor);
				}

				// Need to decode ntip and get its array version
				String expectedTip = "NIZ_"+ntip.toString();
				Tipovi arrayType = Tipovi.valueOf(expectedTip);
				// Now make the declaration and add it to the local scope
				Varijabla newVar = new Varijabla(toCheckName, arrayType);
				tmp.getListaVarijabla().add(newVar);
				
				// Let's note down that a Varijabla was added
				radniCvor.atributi.put("varOrFun", "var");
				
				// Put the br-elem attrib
				radniCvor.atributi.put("br-elem", Integer.parseInt(radniCvor.getDjeca().get(2).getVrijednost().getZavrsniUProgramu()));
				radniCvor.atributi.put("tip", arrayType);
			} else if(children.get(1).getVrijednost().getImeZnaka().equals("L_ZAGRADA") && children.get(2).getVrijednost().getImeZnaka().equals("KR_VOID")) {
				// Prod 2
				// Now we're making functions
				// Check for declarations inside local(?) scope
				String toCheckName = dijeteCvor.getVrijednost().getZavrsniUProgramu(); 
				Djelokrug tmp = localScope(radniCvor);
				Tipovi ntip = (Tipovi)radniCvor.atributi.get("ntip");
				
				boolean alreadyPresent = false;
				DeklaracijaFunkcije currDecl = new DeklaracijaFunkcije(toCheckName, ntip, Tipovi.VOID);
				for(DeklaracijaFunkcije tmpDecl : tmp.getListaDeklaracija()) {
					if(tmpDecl.getIme().equals(dijeteCvor.getVrijednost().getZavrsniUProgramu())) {
						// Same name functions - check if everything else matches
						if(!tmpDecl.compareReturnAndParamsType(tmpDecl)) {
							pogreska(radniCvor);		// Same name function declaration, but different return/parameter types
						} else {
							// A good declaration!
							alreadyPresent = true;
						}
					}
				}
				
				if(!alreadyPresent) {
					// Add this declaration to the local scope
					tmp.getListaDeklaracija().add(currDecl);
					// Let's note down that a Function was added
					radniCvor.atributi.put("varOrFun", "fun");
				}
				
				// Finally put the attributes
				radniCvor.atributi.put("tip", currDecl);
			} else {
				// Prod 3
	
				listaParametara(radniCvor.getDjeca().get(2));	// Check lista_parametara
				
//				// Check if already declared
				String toCheckName = dijeteCvor.getVrijednost().getZavrsniUProgramu();
				boolean alreadyPresent = false;
				Djelokrug tmp = localScope(radniCvor);
				Tipovi ntip = (Tipovi)radniCvor.atributi.get("ntip");
				DeklaracijaFunkcije currDecl = new DeklaracijaFunkcije(toCheckName, ntip, (List<Tipovi>)radniCvor.getDjeca().get(2).atributi.get("tipovi"));
				for(DeklaracijaFunkcije tmpDecl : tmp.getListaDeklaracija()) {
					if(currDecl.getIme().equals(tmpDecl.getIme())) {
						// Same name functions - check if everything else matches
						if(!currDecl.compareReturnAndParamsType(tmpDecl)) {
							pogreska(radniCvor);		// Same name function declaration, but different return/parameter types
						} else {
							// A good declaration!
							alreadyPresent = true;
						}
					}
				}
				
				// If not already declared add the declaration
				if(!alreadyPresent) {
					tmp.getListaDeklaracija().add(currDecl);
					// Let's note down that a Function was added
					radniCvor.atributi.put("varOrFun", "fun");
				}
				
				// Finally, attribs!
				radniCvor.atributi.put("tip", currDecl);
			}
		} else if(childrenNum == 1) {
			// Prod 0 - this should be fine
			// First check that ntip isn't VOID
			String toCheckName = dijeteCvor.getVrijednost().getZavrsniUProgramu(); 
			
			Cvor<Djelokrug> tmpNode = localScopeNode(radniCvor);
			Djelokrug tmp = tmpNode.getVrijednost();
			
			Tipovi ntip = (Tipovi)radniCvor.atributi.get("ntip");
			if(ntip == Tipovi.VOID) {
				pogreska(radniCvor);
			}
			
			// Now check that IDN.ime isn't declared in the local scope
			// Now need to check whether the localScope contains this var
			Set<String> localScopeNames = tmp.getListaDeklaracija().parallelStream()
				.map((elem) -> elem.getIme())
				.collect(Collectors.toSet()
			);
			
			localScopeNames.addAll(
					tmp.getListaVarijabla().parallelStream()
					.map((elem) -> elem.getIme())
					.collect(Collectors.toList())
			);
			
			// But what about function definitions? How could I know
			// that we're dealing with the global scope?
			boolean global = tmpNode.getRodCvor()==null;
			if(global) {
				//it's global, so add now all the definitions
				/*
				 * That is because we have got a special list that contains all the function definitions(
				 * at the global level ofcourse). The global scope node contains only the global declarations
				 * 
				 * To summarise, our scopes do not contain function definitions
				 */
				localScopeNames.addAll(
						sveDefiniraneFunkcije.parallelStream()
							.map(DeklaracijaFunkcije::getIme)
							.collect(Collectors.toSet())
						);
			}
			
			if(localScopeNames.contains(toCheckName)) {
				pogreska(radniCvor);
			}
			
			// Now make the declaration and add it to the local scope
			Varijabla newVar = new Varijabla(toCheckName, ntip);
			tmp.getListaVarijabla().add(newVar);
			// Let's note down that a Varijabla was added
			radniCvor.atributi.put("varOrFun", "var");
			
			// At last, add the attribs
			radniCvor.atributi.put("tip", newVar);
		}
	}

	/**
	 * @param radniCvor the current node
	 * @return The local scope
	 */
	private static Djelokrug localScope(Cvor<Znak> radniCvor) {
		Cvor<Djelokrug> tmp = localScopeNode(radniCvor);
		return tmp.getVrijednost();
	}
	
	public static void jednakosniIzraz(Cvor<Znak> radniCvor) {
		Cvor<Znak> dijeteCvor = radniCvor.getDjeca().get(0);  
		radniZnak = dijeteCvor.getVrijednost();
		
		if(radniZnak.getImeZnaka().equals("odnosni_izraz")) {
			odnosniIzraz(dijeteCvor);
			
			Object toAdd = dijeteCvor.atributi.get("tip");
			radniCvor.atributi.put("tip", toAdd);
			
			toAdd = dijeteCvor.atributi.get("l-izraz");
			radniCvor.atributi.put("l-izraz", toAdd);
			
//			toAdd = dijeteCvor.atributi.get("br-elem");
//			radniCvor.atributi.put("br-elem", toAdd);
		} else if(radniZnak.getImeZnaka().equals("jednakosni_izraz")) {
			radniCvor.atributi.put("tip", Tipovi.INT);
			radniCvor.atributi.put("l-izraz", false);
			
			jednakosniIzraz(dijeteCvor);
			
			if(!Pravila.pretvori(dijeteCvor.atributi.get("tip").toString(), "INT"))
				pogreska(radniCvor);
			
			dijeteCvor = radniCvor.getDjeca().get(2);
			odnosniIzraz(dijeteCvor);
			
			if(!Pravila.pretvori(dijeteCvor.atributi.get("tip").toString(), "INT"))
				pogreska(radniCvor);	
		}
		
		Object toPut = dijeteCvor.atributi.get("func");
		radniCvor.atributi.put("func", toPut);
	}

	public static void odnosniIzraz(Cvor<Znak> radniCvor) {

		Cvor<Znak> dijeteCvor = radniCvor.getDjeca().get(0);  
		radniZnak = dijeteCvor.getVrijednost();
		
		if(radniZnak.getImeZnaka().equals("aditivni_izraz")) {
			aditivniIzraz(dijeteCvor);
			
			Object toAdd = dijeteCvor.atributi.get("tip");
			radniCvor.atributi.put("tip", toAdd);
			
			toAdd = dijeteCvor.atributi.get("l-izraz");
			radniCvor.atributi.put("l-izraz", toAdd);
			
//			toAdd = dijeteCvor.atributi.get("br-elem");
//			radniCvor.atributi.put("br-elem", toAdd);
		} else if(radniZnak.getImeZnaka().equals("odnosni_izraz")) {
			radniCvor.atributi.put("tip", Tipovi.INT);
			radniCvor.atributi.put("l-izraz", false);
			
			odnosniIzraz(dijeteCvor);
			
			if(!Pravila.pretvori(dijeteCvor.atributi.get("tip").toString(), "INT"))
				pogreska(radniCvor);
			
			dijeteCvor = radniCvor.getDjeca().get(2);
			aditivniIzraz(dijeteCvor);
			
			if(!Pravila.pretvori(dijeteCvor.atributi.get("tip").toString(), "INT"))
				pogreska(radniCvor);
		}
		
		Object toPut = dijeteCvor.atributi.get("func");
		radniCvor.atributi.put("func", toPut);
	}

	private static void aditivniIzraz(Cvor<Znak> radniCvor) {
		Cvor<Znak> dijeteCvor = radniCvor.getDjeca().get(0);
		radniZnak = dijeteCvor.getVrijednost();
		
		if(radniZnak.getImeZnaka().equals("multiplikativni_izraz")) {
			multiplikativniIzraz(dijeteCvor);
			
			Object toAdd = dijeteCvor.atributi.get("tip");
			radniCvor.atributi.put("tip", toAdd);
			
			toAdd = dijeteCvor.atributi.get("l-izraz");
			radniCvor.atributi.put("l-izraz", toAdd);
			
//			toAdd = dijeteCvor.atributi.get("br-elem");
//			radniCvor.atributi.put("br-elem", toAdd);
		} else if(radniZnak.getImeZnaka().equals("aditivni_izraz")) {
			radniCvor.atributi.put("tip", Tipovi.INT);
			radniCvor.atributi.put("l-izraz", false);
			
			aditivniIzraz(dijeteCvor);
			
			if(!Pravila.pretvori(dijeteCvor.atributi.get("tip").toString(), "INT"))
				pogreska(radniCvor);
			
			dijeteCvor = radniCvor.getDjeca().get(2);
			multiplikativniIzraz(dijeteCvor);
			
			if(!Pravila.pretvori(dijeteCvor.atributi.get("tip").toString(), "INT"))
				pogreska(radniCvor);
		}
		
		Object toPut = dijeteCvor.atributi.get("func");
		radniCvor.atributi.put("func", toPut);
	}
	
	private static void provjeraDefinicijaFunkcija() {
		Cvor<Djelokrug> globalniDjelokrug = glava.getReferenca();
		Set<DeklaracijaFunkcije> sveDeklaracije = new HashSet<>();
		
		obilazakStablaZaDeklaracije(sveDeklaracije, globalniDjelokrug, true);
		Map<String, DeklaracijaFunkcije> functionsMap = new HashMap<>();
		for(DeklaracijaFunkcije jednaDefinicija: sveDefiniraneFunkcije) {
			if(jednaDefinicija.definiranaFunkcija)
				functionsMap.put(jednaDefinicija.getIme(), jednaDefinicija);
		}
		
		for(DeklaracijaFunkcije jednaDeklaracija: sveDeklaracije) {
			DeklaracijaFunkcije matchedFunDecl = functionsMap.get(jednaDeklaracija.getIme());
			boolean error1 = matchedFunDecl == null;
			boolean error2 = jednaDeklaracija.compareDeclarations(matchedFunDecl);
			
			if(error1 || !error2) {
				System.out.println("funkcija");
				System.exit(-1);
			}
		}
	}
	
	private static void obilazakStablaZaDeklaracije(Set<DeklaracijaFunkcije> sveDeklaracije, Cvor<Djelokrug> radniCvor, boolean global) {
		if(radniCvor == null) return;
		
		List<DeklaracijaFunkcije> lokalneDeklaracije;
		if(global) {
			for(DeklaracijaFunkcije jednaDeklaracija: sveDefiniraneFunkcije) {
					sveDeklaracije.add(jednaDeklaracija);
			}
			
			// Now go over and add the global scope from the scope tree
			sveDeklaracije.addAll(radniCvor.getVrijednost().getListaDeklaracija());
		} else {
			lokalneDeklaracije = radniCvor.getVrijednost().getListaDeklaracija();
			sveDeklaracije.addAll(lokalneDeklaracije);
		}
		
		List<Cvor<Djelokrug>> djeca = radniCvor.getDjeca();
		for(Cvor<Djelokrug> dijete: djeca) {
			obilazakStablaZaDeklaracije(sveDeklaracije, dijete, false);
		}
	}
		
	private static void deklaracijaParametra (Cvor<Znak> radniCvor) {
		ArrayList<Cvor<Znak>> djeca = radniCvor.getDjeca();
		
		if(djeca.size()==2) {
			
			imeTipa(djeca.get(0));
			if((Tipovi)djeca.get(0).atributi.get("tip") == Tipovi.VOID)
				pogreska(radniCvor);
			
			radniCvor.atributi.put("tip",djeca.get(0).atributi.get("tip"));
			radniCvor.atributi.put("ime",djeca.get(1).getVrijednost().getZavrsniUProgramu());
			
		} else if(djeca.size()==4) {
			
			imeTipa(djeca.get(0));
			if((Tipovi)djeca.get(0).atributi.get("tip") == Tipovi.VOID)
				pogreska(radniCvor);
			
			radniCvor.atributi.put("tip",Pravila.pretvoriUNiz((Tipovi)djeca.get(0).atributi.get("tip")));
			radniCvor.atributi.put("ime",djeca.get(1).getVrijednost().getZavrsniUProgramu());
			
		}
	}
	
	private static void inicijalizator(Cvor<Znak> cvor) {
		
		ArrayList<Cvor<Znak>> djeca = cvor.getDjeca();
		ArrayList<Tipovi> tipovi = new ArrayList<>();
		
		if(djeca.size()==1) {
			
			izrazPridruzivanja(djeca.get(0));
			
			//TODO: MAYBE NOT SURE ABOUT THE NEXT IF(THAT IS NOW COMMENTED)
			// Let's check if an assignnment cast error happened
			// Basically we're checking the non-const_array assignment
//			Object toCheck = djeca.get(0).atributi.get("assignment_error");
//			if(toCheck != null && (boolean)toCheck) {
//				pogreska(cvor);
//			}
			
			if(djeca.get(0).atributi.get("tip")==Tipovi.NIZ_CONST_CHAR) {
				Cvor<Znak> pomCvor = cvor;
				while(!pomCvor.getVrijednost().getImeZnaka().equals("NIZ_ZNAKOVA")) {
					pomCvor = pomCvor.getDjeca().get(0);
				}
				cvor.atributi.put("br-elem", Pravila.effectiveStringSize(pomCvor.getVrijednost().getZavrsniUProgramu()) + 1); //navodnici!!
				for(int i = 1; i<pomCvor.getVrijednost().getZavrsniUProgramu().length(); i++)
					tipovi.add(Tipovi.CHAR);
				cvor.atributi.put("tipovi",tipovi);
			} else {
				cvor.atributi.put("tip", djeca.get(0).atributi.get("tip"));
			}
			
		} else if(djeca.size()==3) {
			
			listaIzrazaPridruzivanja(djeca.get(1));
			cvor.atributi.put("br-elem", djeca.get(1).atributi.get("br-elem"));
			cvor.atributi.put("tipovi", djeca.get(1).atributi.get("tipovi"));
			
		}
		
		Object toAdd = djeca.get(0).atributi.get("func");
		cvor.atributi.put("func", toAdd);
	}
}

