

import java.util.ArrayList;
import java.util.List;

public class Pravila {
	
	public static boolean jeliBrojURasponu(int a) {
		return (a >= -2147483648 && a <= 2147483647);
	}
	
	public static boolean jeliCharURasponu(int c) {
		return (c >= 0 && c <= 255);
	}
	
	public static boolean jeliDobarChar(String s, boolean character) {
		char[] charArray = s.toCharArray();
		char c = charArray[1];
		
		if(c == '\\') {
			if(charArray.length != 4) {		// Two apostrophes, and two characters: one backslash(the escaping char), and one escaped char
				return false;
			}
			
			c = charArray[2];
			if(c == 't' || c == 'n' || c == '0' || c == '\'' || c == '"' || c == '\\')
				return true;
			else
				return false;
		} else if(character && s.equals("'''")) {
			return false;
		} else if(!character && s.equals("'\"'")) {
			return false;
		}
		
		return jeliCharURasponu(charArray[1]);
	}
	
	public static List<String> ppjCSplitString(String s) {
		List<String> dijeloviStringa = new ArrayList<>();
		//pravimo niz charova sa \ i bez 
		for(int i = 1; i < s.length()-1; i++) {
			if(s.charAt(i) == '\\') {
				String novi;
				if(i + 1 <= s.length() - 2) {
					novi = "\\" + s.charAt(i+1);	// If the next character is located WITHIN the string(not counting the String boundary double quotes)
				} else {
					novi = "" + s.charAt(i);
				}
				i++;
				dijeloviStringa.add(novi);
			} else {
				dijeloviStringa.add("" + s.charAt(i));
			}
		}
		
		return dijeloviStringa;
	}
	
	public static int effectiveStringSize(String s) {
		return ppjCSplitString(s).size();
	}
	public static boolean provjeriNizZnakova(String s) {
		List<String> dijeloviStringa = ppjCSplitString(s);
		
		//provjera svakog podniza
		for(String pom: dijeloviStringa) {
			if(!jeliDobarChar("'" + pom + "'", false))
				return false;
		}
		return true;	
	}
	
	public boolean jeliConst(Znak z) {
		return z.getJeliKonstanta();
	}
	
	public static Tipovi pretvoriUKonstantu(Tipovi tip) {
		Tipovi tip1 = null;
		
		if(tip == Tipovi.CHAR)
			tip1 = Tipovi.CONST_CHAR;
		else if(tip == Tipovi.INT)
			tip1 = Tipovi.CONST_INT;
		else if(tip == Tipovi.NIZ_CHAR)
			tip1 = Tipovi.NIZ_CONST_CHAR;
		else if(tip == Tipovi.NIZ_INT)
			tip1 = Tipovi.NIZ_CONST_INT;
			
		return tip1;
	}
	
	public static Tipovi pretvoriUNiz(Tipovi tip) {
		Tipovi tip1 = null;
		
		if(tip == Tipovi.CHAR)
			tip1 = Tipovi.NIZ_CHAR;
		else if(tip == Tipovi.INT)
			tip1 = Tipovi.NIZ_INT;
		else if(tip == Tipovi.CONST_CHAR)
			tip1 = Tipovi.NIZ_CONST_CHAR;
		else if(tip == Tipovi.CONST_INT)
			tip1 = Tipovi.NIZ_CONST_INT;
			
		return tip1;
	}
	
	public static boolean assignmentImplicitCast(String tip1, String tip2) {
		if(tip1.equals("CHAR") && tip2.equals("INT")
				|| tip1.equals("CHAR") && tip2.equals("CONST_INT")
				|| tip1.equals("CHAR") && tip2.equals("CONST_CHAR")
				|| tip1.equals("INT") && tip2.equals("CONST_INT")
//				|| tip1.equals("CONST_NIZ_CHAR") && tip2.equals("NIZ_CONST_CHAR")
//				|| tip1.equals("CONST_NIZ_CONST_CHAR") && tip2.equals("NIZ_CONST_CHAR")
//				|| tip1.equals("CONST_NIZ_CHAR") && tip2.equals("NIZ_CHAR")
//				|| tip1.equals("CONST_NIZ_CONST_CHAR") && tip2.equals("NIZ_CHAR")
//				|| tip1.equals("CONST_NIZ_CHAR") && tip2.equals("CONST_NIZ_CHAR")
//				|| tip1.equals("CONST_NIZ_CONST_CHAR") && tip2.equals("CONST_NIZ_CHAR")
//				|| tip1.equals("CONST_NIZ_INT") && tip2.equals("NIZ_CONST_INT")
//				|| tip1.equals("CONST_NIZ_CONST_INT") && tip2.equals("NIZ_CONST_INT")
//				|| tip1.equals("CONST_NIZ_INT") && tip2.equals("NIZ_INT")
//				|| tip1.equals("CONST_NIZ_CONST_INT") && tip2.equals("NIZ_INT")
//				|| tip1.equals("CONST_NIZ_INT") && tip2.equals("CONST_NIZ_INT")
//				|| tip1.equals("CONST_NIZ_CONST_INT") && tip2.equals("CONST_NIZ_INT")
				|| tip1.equals("CONST_CHAR") && tip2.equals("CHAR")
				|| tip1.equals("CONST_CHAR") && tip2.equals("INT")
				|| tip1.equals("CONST_CHAR") && tip2.equals("CONST_INT")
				|| tip1.equals("CONST_INT") && tip2.equals("INT")
				|| tip1.equals("NIZ_CONST_INT") && tip2.equals("NIZ_INT")
				|| tip1.equals("NIZ_CONST_CHAR") && tip2.equals("NIZ_INT")
				|| tip1.equals("NIZ_CONST_CHAR") && tip2.equals("NIZ_CONST_INT")
				|| tip1.equals("NIZ_CONST_CHAR") && tip2.equals("NIZ_CHAR")
				|| tip1.equals(tip2)
				) {
					return true;
		} 
		
		return false;
	}
	
	public static boolean pretvori(String tip1, String tip2) {
		if(tip1.equals("CHAR") && tip2.equals("INT")
		 ||tip1.equals("CHAR") && tip2.equals("CONST_INT")
		 ||tip1.equals("CONST_CHAR") && tip2.equals("CHAR")
		 ||tip1.equals("CONST_CHAR") && tip2.equals("INT")
		 ||tip1.equals("CONST_CHAR") && tip2.equals("CONST_INT")
		 ||tip1.equals("CHAR") && tip2.equals("CONST_CHAR")
		 ||tip1.equals("CONST_INT") && tip2.equals("INT")
		 ||tip1.equals("INT") && tip2.equals("CONST_INT")
		 ||tip1.equals("NIZ_INT") && tip2.equals("NIZ_CONST_INT")
		 ||tip1.equals("NIZ_INT") && tip2.equals("NIZ_CONST_CHAR")
		 ||tip1.equals("NIZ_CHAR") && tip2.equals("NIZ_CONST_CHAR")
		 ||tip1.equals("NIZ_CHAR") && tip2.equals("NIZ_CONST_INT")
		 ||tip1.equals("NIZ_CHAR") && tip2.equals("NIZ_INT")
		 ||tip1.equals(tip2))
			return true;
		
		return false;
	}
	
	public static boolean mozeCast(String tip1, String tip2) {
//		if(tip1.equals("INT") && tip2.equals("CONST_INT")
//		 ||tip1.equals("INT") && tip2.equals("CHAR")
//		 ||tip1.equals("INT") && tip2.equals("CONST_CHAR")
//		 ||tip1.equals("CONST_INT") && tip2.equals("CHAR")
//		 ||tip1.equals("CONST_INT") && tip2.equals("CONST_CHAR")
//		 ||tip1.equals("CONST_CHAR") && tip2.equals("CHAR")
//		 ||tip1.equals("CHAR") && tip2.equals("INT"))
//			return true;
		if(tip1.equals("INT") && tip2.equals("CHAR") 
		 ||tip1.equals("CONST_INT") && tip2.equals("CONST_CHAR")
		 ||tip1.equals("INT") && tip2.equals("CONST_CHAR")
		 ||tip1.equals("CONST_INT") && tip2.equals("CHAR")
		 )
			return true;
		else {
			return pretvori(tip1,tip2);
		}
	}
	
	public static boolean mozeCastGrupno(List<Tipovi> argumenti, List<Tipovi> parametri) {
		if(argumenti.size() != parametri.size())
			return false;
		
		for(int i=0;i<parametri.size();++i) {
			if(!pretvori(argumenti.get(i).toString(),parametri.get(i).toString())) 
				return false;
		}
		
		return true;
	}
	
	public static boolean jeliTipKonstanta(Tipovi tip) {
		return tip.toString().contains("CONST");
	}
	
	public static boolean jeliTipNiz(Tipovi tip) {
		return tip.toString().contains("NIZ");
	}
	
	public static Tipovi pretvoriUObicni(Tipovi tip) {
		if(tip.toString().contains("CHAR"))
			return Tipovi.CHAR;
		else
			return Tipovi.INT;
	}
	
}
