import java.util.ArrayList;

public class Djelokrug {
	
	private ArrayList<DeklaracijaFunkcije> listaDeklaracija;
	private ArrayList<Varijabla> listaVarijabla;
	
	public Djelokrug(ArrayList<DeklaracijaFunkcije> listaDeklaracija, ArrayList<Varijabla> listaVarijabla) {
		super();
		this.listaDeklaracija = listaDeklaracija;
		this.listaVarijabla.addAll(listaVarijabla);
	}
	
	public Djelokrug() {
		listaDeklaracija = new ArrayList<DeklaracijaFunkcije>();
		listaVarijabla = new ArrayList<Varijabla>();
	}
	
	public ArrayList<DeklaracijaFunkcije> getListaDeklaracija() {
		return listaDeklaracija;
	}
	
	public void setListaDeklaracija(ArrayList<DeklaracijaFunkcije> listaDeklaracija) {
		this.listaDeklaracija = listaDeklaracija;
	}
	
	public ArrayList<Varijabla> getListaVarijabla() {
		return listaVarijabla;
	}
	
	public void setListaVarijabla(ArrayList<Varijabla> listaVarijabla) {
		this.listaVarijabla = listaVarijabla;
	}

}
