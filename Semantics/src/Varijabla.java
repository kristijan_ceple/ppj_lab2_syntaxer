

public class Varijabla {
	
	private String ime;
	private Tipovi tip;
	private boolean defined;

	public Varijabla(String ime, Tipovi tip, boolean defined) {
		super();
		this.ime = ime;
		this.tip = tip;
		this.defined = defined;
	}
	
	public Varijabla(String ime, Tipovi tip) {
		this(ime, tip, false);
	}
	
	public boolean isDefined() {
		return defined;
	}

	public void setDefined(boolean defined) {
		this.defined = defined;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public Tipovi getTip() {
		return tip;
	}

	public void setTip(Tipovi tip) {
		this.tip = tip;
	}
	
	
}
