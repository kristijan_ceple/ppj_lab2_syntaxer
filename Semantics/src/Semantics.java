import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;

public class Semantics {
	
	public static Cvor<Znak> glava;
	public static Znak radniZnak;
	public static Set<DeklaracijaFunkcije> sveDeklariraneFunkcije = new HashSet<>();
	public static boolean imaIspravanMain = false;
	
	public static void main(String[] args) {
		
		ArrayList<Cvor<Znak>> sviCvorovi = new ArrayList<Cvor<Znak>>();
		
		Scanner sc = new Scanner(System.in);
		ArrayList<String> ulaz = new ArrayList<String>();
		while(sc.hasNextLine())
			ulaz.add(sc.nextLine());
		int indexClana = 0;
		int brojPraznina=1;
		Cvor<Znak> cvoric;
		Cvor<Znak> trenutniCvor;
		int brojacObradjenihCvorova=0;
		
		//Prolazak kroz stablo i bilježenje svih razina
		while(brojacObradjenihCvorova<ulaz.size()) {
			
			if(indexClana==0) {
				//rezanje < i >
				String imeGlave = ulaz.get(indexClana).substring(1, ulaz.get(indexClana).length()-1);
				Znak znak = new Znak(imeGlave,true);
				glava = new Cvor<Znak>(znak ,indexClana);
				sviCvorovi.add(glava);
			
				for(int i=indexClana+1;i<ulaz.size();++i) {
					if(brojPraznina(ulaz.get(i))==brojPraznina) {
						String imeZnaka = ulaz.get(i).substring(brojPraznina(ulaz.get(i)));
						if((imeZnaka.toCharArray())[0]=='<') {
							imeZnaka = imeZnaka.substring(1,imeZnaka.length()-1);
							Znak znakic = new Znak(imeZnaka,true);
							cvoric = new Cvor<Znak>(znakic, i);
							glava.dodajDijete(cvoric);
							sviCvorovi.add(cvoric);
							cvoric.setRodCvor(glava);
						} else {
							String[] dijeloviNezavrsnogZnaka = ulaz.get(i).substring(brojPraznina(ulaz.get(i))).split(" ");
							Znak znakic = new Znak(dijeloviNezavrsnogZnaka[0], Integer.parseInt(dijeloviNezavrsnogZnaka[1]), dijeloviNezavrsnogZnaka[2]);
							cvoric = new Cvor<Znak>(znakic, i);
							glava.dodajDijete(cvoric);
							sviCvorovi.add(cvoric);
							cvoric.setRodCvor(glava);
						}
					}
				}
				
				indexClana++;
				brojPraznina++;
				brojacObradjenihCvorova++;
			} else {
				
				trenutniCvor = pronadjiCvor(sviCvorovi, indexClana ,glava ,brojPraznina);
				
				for ( int i = indexClana+1;i<ulaz.size() && brojPraznina(ulaz.get(i))>=brojPraznina;++i) {
					if(brojPraznina(ulaz.get(i))==brojPraznina) {
						String imeZnaka = ulaz.get(i).substring(brojPraznina(ulaz.get(i)));
						if((imeZnaka.toCharArray())[0]=='<') {
							imeZnaka = imeZnaka.substring(1,imeZnaka.length()-1);
							Znak znakic = new Znak(imeZnaka,true);
//							System.out.println(imeZnaka);
							cvoric = new Cvor<Znak>(znakic, i);
							trenutniCvor.dodajDijete(cvoric);
							sviCvorovi.add(cvoric);
							cvoric.setRodCvor(trenutniCvor);
						} else {
							String[] dijeloviNezavrsnogZnaka = ulaz.get(i).substring(brojPraznina(ulaz.get(i))).split(" ");
							Znak znakic = new Znak(dijeloviNezavrsnogZnaka[0], Integer.parseInt(dijeloviNezavrsnogZnaka[1]), dijeloviNezavrsnogZnaka[2]);
							cvoric = new Cvor<Znak>(znakic, i);
							trenutniCvor.dodajDijete(cvoric);
							sviCvorovi.add(cvoric);
							cvoric.setRodCvor(trenutniCvor);
						}
					}
				}
				
				//pronađi indeks idućeg čvora
				boolean nijeNadjenIduci=true;
				for(int i=indexClana+1;i<ulaz.size() && nijeNadjenIduci;++i) {
					if(brojPraznina(ulaz.get(i))==brojPraznina-1) {
						indexClana=i;
						nijeNadjenIduci=false;
					}
				}
				if(nijeNadjenIduci) {
					brojPraznina++;
					for(int i=0;i<ulaz.size();++i)
						if(brojPraznina(ulaz.get(i))==brojPraznina-1) {
							indexClana = i;
							break;
						}
				}
				brojacObradjenihCvorova++;
				
			}	
		}
		sc.close();

		Cvor<Djelokrug> glavaOpsega= kreirajStabloDjelokruga(glava ,sviCvorovi);

		prijevodnaJedinica(glava);
		
		if(!imaIspravanMain) {
			System.out.println("main");
			System.exit(-1);
		}
		provjeraDefinicijaFunkcija();
	}
	
	public static void prijevodnaJedinica(Cvor<Znak> radniCvor) {	
		Cvor<Znak> dijeteCvor = radniCvor.getDjeca().get(0);
		radniZnak = dijeteCvor.getVrijednost();
		
		if(radniZnak.getImeZnaka().equals("vanjska_deklaracija"))
			vanjskaDeklaracija(dijeteCvor);
		else if (radniZnak.getImeZnaka().equals("prijevodna_jedinica")) {
			prijevodnaJedinica(dijeteCvor);	//TODO:  moguce potrebno dodati parametre
			dijeteCvor = radniCvor.getDjeca().get(1);
			vanjskaDeklaracija(dijeteCvor);
		}
	}
	
	public static void vanjskaDeklaracija(Cvor<Znak> radniCvor) {
		Cvor<Znak> dijeteCvor = radniCvor.getDjeca().get(0);
		radniZnak = dijeteCvor.getVrijednost();
		
		if(radniZnak.getImeZnaka().equals("definicija_funkcije"))
			definicijaFunkcije(dijeteCvor);
		
		else if(radniZnak.getImeZnaka().equals("deklaracija"))
			deklaracija(dijeteCvor);
	}
	
	public static void definicijaFunkcije(Cvor<Znak> radniCvor) {
		Cvor<Znak> dijeteCvor = radniCvor.getDjeca().get(0);               //1.
			radniZnak = dijeteCvor.getVrijednost(); // radniZnak = ime_tipa
			imeTipa(dijeteCvor);
			//TODO: implement getting type atrribute of Znak from TablicaUniformnihZnakova of correct Djelokrug
			
			String s = dijeteCvor.atributi.get("tip").toString();
			if(!(s == "INT" || s == "CHAR" || s == "VOID")) {  			//2
				pogreska(radniCvor);
			}
			
			dijeteCvor = radniCvor.getDjeca().get(1);       		//3
			radniZnak = dijeteCvor.getVrijednost(); // radniZnak = IDN
			
			if(!imaIspravanMain) {  // PROVJERA MAINA 4.4.7
				Cvor <Znak> pomCvorPov = radniCvor.getDjeca().get(0);
				Cvor <Znak> pomCvorParam = radniCvor.getDjeca().get(3);
				if(dijeteCvor.getVrijednost().getZavrsniUProgramu().equals("main") && 
			       pomCvorPov.atributi.get("tip") == Tipovi.INT &&
			       pomCvorParam.getVrijednost().getZavrsniUProgramu().equals("void")) {
					imaIspravanMain = true;
				}
			}
			for(DeklaracijaFunkcije pom : sveDeklariraneFunkcije) {
				//function can't be defined twice
				if(pom.getIme().equals(radniZnak.getZavrsniUProgramu()) && pom.definiranaFunkcija) {
					pogreska(radniCvor);
				}
			}
			if(radniCvor.getDjeca().get(3).getVrijednost().getImeZnaka().equals("KR_VOID")){ //4.1
				
				for(DeklaracijaFunkcije pom : sveDeklariraneFunkcije) {
					if(pom.getIme().equals(radniZnak.getZavrsniUProgramu()) && !pom.definiranaFunkcija) { //3.1 i 4.1
						if(!(pom.getPovratniTip().equals(radniCvor.getDjeca().get(0).atributi.get("tip")) && //4.1
						   pom.getTipoviParametara().size() == 1 && pom.getTipoviParametara().toArray()[0]==Tipovi.VOID)) {
								pogreska(radniCvor);
						} else {
							sveDeklariraneFunkcije.remove(pom);
							break;
						}
					}
				}
				LinkedList<Tipovi> pomList = new LinkedList<>();    //5.1
				pomList.add(Tipovi.VOID);
				DeklaracijaFunkcije pomDek = new DeklaracijaFunkcije(dijeteCvor.getVrijednost().getZavrsniUProgramu(),
						   											 (Tipovi) radniCvor.getDjeca().get(0).atributi.get("tip"),
						   											 pomList);
				pomDek.definiranaFunkcija = true;
				sveDeklariraneFunkcije.add(pomDek);
				
				dijeteCvor = radniCvor.getDjeca().get(5);   //6.1
				slozenaNaredba(dijeteCvor);
			}
			
			else if(radniCvor.getDjeca().get(3).getVrijednost().getImeZnaka().equals("lista_parametara")) {
				dijeteCvor = radniCvor.getDjeca().get(3);  //4.2
				listaParametara(dijeteCvor);
							
				for(DeklaracijaFunkcije pom : sveDeklariraneFunkcije) { //5.2
					if(pom.getIme().equals(radniZnak.getZavrsniUProgramu()) && !pom.definiranaFunkcija) {
						if(!(pom.getPovratniTip().equals(radniCvor.getDjeca().get(0).atributi.get("tip")) &&
						  (radniCvor.getDjeca().get(3).atributi.get("tipovi").equals(pom.tipoviParametara)))) {
								pogreska(radniCvor);
						}
					}
				}
				// pomListImena.size() == pomListTipovi.size()
				List<Tipovi> pomListTipova = (List<Tipovi>) radniCvor.getDjeca().get(3).atributi.get("tipovi"); //6.2
				List<String> pomListImena = (List<String>) radniCvor.getDjeca().get(3).atributi.get("imena");
				DeklaracijaFunkcije pomDek = new DeklaracijaFunkcije(radniCvor.getDjeca().get(1).getVrijednost().getZavrsniUProgramu(),
						   											 (Tipovi) radniCvor.getDjeca().get(0).atributi.get("tip"),
						   											 pomListTipova);
				pomDek.definiranaFunkcija = true;
				sveDeklariraneFunkcije.add(pomDek);
				
				dijeteCvor = radniCvor.getDjeca().get(5); //7.2
				Djelokrug tmp = dijeteCvor.referenca.getVrijednost();
				
				for( int i = 0; i < pomListTipova.size(); i++) {
					tmp.getListaVarijabla().add(new Varijabla(pomListImena.get(i), pomListTipova.get(i)));
				}
				slozenaNaredba(dijeteCvor);
			}
	}

	private static void slozenaNaredba(Cvor<Znak> radniCvor) {
		Cvor<Znak> dijeteCvor = radniCvor.getDjeca().get(1);  
		radniZnak = dijeteCvor.getVrijednost();
		
		if(radniZnak.getImeZnaka().equals("lista_naredbi")) { //1.1
			listaNaredbi(dijeteCvor);
		}
		
		else if(radniZnak.getImeZnaka().equals("lista_deklaracija")) {
			listaDeklaracija(dijeteCvor); //1.2
			
			dijeteCvor = radniCvor.getDjeca().get(2);
			listaNaredbi(dijeteCvor);
			
		}
	}

	private static void listaDeklaracija(Cvor<Znak> radniCvor) {
		Cvor<Znak> dijeteCvor = radniCvor.getDjeca().get(0);  
		radniZnak = dijeteCvor.getVrijednost();
		
		if(radniZnak.getImeZnaka().equals("deklaracija"))
			deklaracija(dijeteCvor);
		
		else if(radniZnak.getImeZnaka().equals("lista_deklaracija")) {
			listaDeklaracija(dijeteCvor);   
			dijeteCvor = radniCvor.getDjeca().get(1);
			deklaracija(dijeteCvor); 
		}
		
	}

	private static void listaNaredbi(Cvor<Znak> radniCvor) {
		Cvor<Znak> dijeteCvor = radniCvor.getDjeca().get(0);  
		radniZnak = dijeteCvor.getVrijednost();
		
		if(radniZnak.getImeZnaka().equals("naredba")) //1.1
			naredba(dijeteCvor);
		
		else if(radniZnak.getImeZnaka().equals("lista_naredbi")) {
			listaNaredbi(dijeteCvor);     //1.2
			dijeteCvor = radniCvor.getDjeca().get(1);
			naredba(dijeteCvor); 		//2.2
		}
		
	}

	private static void naredba(Cvor<Znak> radniCvor) {
		Cvor<Znak> dijeteCvor = radniCvor.getDjeca().get(0);  
		radniZnak = dijeteCvor.getVrijednost();
		
		if(radniZnak.getImeZnaka().equals("slozena_naredba"))
			slozenaNaredba(dijeteCvor);
		else if(radniZnak.getImeZnaka().equals("izraz_naredba"))
			izrazNaredba(dijeteCvor);
		else if(radniZnak.getImeZnaka().equals("naredba_grananja"))
			naredbaGrananja(dijeteCvor);
		else if(radniZnak.getImeZnaka().equals("naredba_petlje"))
			naredbaPetlje(dijeteCvor);
		else if(radniZnak.getImeZnaka().equals("naredba_skoka"))
			naredbaSkoka(dijeteCvor);
		
	}

	private static void naredbaSkoka(Cvor<Znak> radniCvor) {
		Cvor<Znak> dijeteCvor = radniCvor.getDjeca().get(0);  
		radniZnak = dijeteCvor.getVrijednost();
		
		if(radniZnak.getImeZnaka().equals("KR_CONTINUE") || //1.1
		   radniZnak.getImeZnaka().equals("KR_BREAK")){
			   Cvor<Znak> tmpCvor = dijeteCvor;
			   boolean imaPetlje = false;
			   while(tmpCvor.getRodCvor() != null) {
				   tmpCvor = tmpCvor.getRodCvor();
				   if(tmpCvor.getVrijednost().getImeZnaka().equals("naredba_petlje")) {
					   imaPetlje = true;
					   break;
				   }
			   }
			   if(!imaPetlje)
				   pogreska(radniCvor);
		   }
		
		dijeteCvor = radniCvor.getDjeca().get(1);
		radniZnak = dijeteCvor.getVrijednost();
		
		if(radniZnak.getImeZnaka().equals("TOCKAZAREZ")) { //1.2
		   Cvor<Znak> tmpCvor = dijeteCvor;
		   boolean imaFunkcijuSVoid = false;
		   while(tmpCvor.getRodCvor() != null) {
			   tmpCvor = tmpCvor.getRodCvor();
			   if(tmpCvor.getVrijednost().getImeZnaka().equals("definicija_funkcije")) {
				   if(tmpCvor.getDjeca().get(0).atributi.get("tip").equals(Tipovi.VOID)) {
					   imaFunkcijuSVoid = true;
					   break;
				   }
			   }
		   }
		   if(!imaFunkcijuSVoid)
			   pogreska(radniCvor);
		}
		else if(radniZnak.getImeZnaka().equals("izraz")) { //1.3
			 izraz(dijeteCvor);
			 
			 Cvor<Znak> tmpCvor = dijeteCvor;
			 Tipovi tipOdIzraz = (Tipovi) dijeteCvor.atributi.get("tip");
			 boolean imaFunkcijuSPov = false;
			 while(tmpCvor.getRodCvor() != null) {
				 tmpCvor = tmpCvor.getRodCvor();
				 if(tmpCvor.getVrijednost().getImeZnaka().equals("definicija_funkcije")) {
					 if(Pravila.pretvori(tipOdIzraz.toString(),tmpCvor.getDjeca().get(0).atributi.get("tip").toString())) {
						 imaFunkcijuSPov = true;
						 break;
					 }
				 }
			 }
			 if(!imaFunkcijuSPov)
				 pogreska(radniCvor);
		}
	}

	private static void izraz(Cvor<Znak> radniCvor) {
		Cvor<Znak> dijeteCvor = radniCvor.getDjeca().get(0); 
		radniZnak = dijeteCvor.getVrijednost();
		
		if(radniZnak.getImeZnaka().equals("izraz_pridruzivanja")) {  //1.1
			izrazPridruzivanja(dijeteCvor);
			Object toAdd = dijeteCvor.atributi.get("tip");
			radniCvor.atributi.put("tip", toAdd);
			
			toAdd = dijeteCvor.atributi.get("l-izraz");
			radniCvor.atributi.put("l-izraz", toAdd);
		}
		else if(radniZnak.getImeZnaka().equals("izraz")) {
			izraz(dijeteCvor);			//1.2
			
			dijeteCvor = radniCvor.getDjeca().get(0);  //2.2
			izrazPridruzivanja(dijeteCvor);
			
			Object toAdd = dijeteCvor.atributi.get("tip");
			radniCvor.atributi.put("tip", toAdd);
			
			radniCvor.atributi.put("l-izraz", false);
		}
	}

	private static void izrazPridruzivanja(Cvor<Znak> radniCvor) {
		Cvor<Znak> dijeteCvor = radniCvor.getDjeca().get(0);  
		radniZnak = dijeteCvor.getVrijednost();
		
		if(radniZnak.getImeZnaka().equals("log_ili_izraz")) {  //1.1
			logIliIzraz(dijeteCvor);
			
			Object toAdd = dijeteCvor.atributi.get("tip");
			radniCvor.atributi.put("tip", toAdd);
			
			toAdd = dijeteCvor.atributi.get("l-izraz");
			radniCvor.atributi.put("l-izraz", toAdd);
		}
		else if(radniZnak.getImeZnaka().equals("postfiks_izraz")) { //1.2
			radniCvor.atributi.put("l-izraz", false);
			
			postfiksIzraz(dijeteCvor);
			dijeteCvor.atributi.put("l-izraz", true);  //2.2
			
			dijeteCvor = radniCvor.getDjeca().get(2);
			Tipovi pomTip = (Tipovi) radniCvor.getDjeca().get(0).atributi.get("tip");
			if(!Pravila.pretvori(dijeteCvor.atributi.get("tip").toString(), pomTip.toString())) {
				pogreska(radniCvor);
			}
			
		}
	}

	private static void logIliIzraz(Cvor<Znak> radniCvor) {
		Cvor<Znak> dijeteCvor = radniCvor.getDjeca().get(0);  
		radniZnak = dijeteCvor.getVrijednost();
		if(radniZnak.getImeZnaka().equals("log_i_izraz")) { // 1.1
			logIIzraz(dijeteCvor);
			
			Object toAdd = dijeteCvor.atributi.get("tip");
			radniCvor.atributi.put("tip", toAdd);
			
			toAdd = dijeteCvor.atributi.get("l-izraz");
			radniCvor.atributi.put("l-izraz", toAdd);
		}
		else if(radniZnak.getImeZnaka().equals("log_ili_izraz")) { //1.2
			radniCvor.atributi.put("tip", Tipovi.INT);
			radniCvor.atributi.put("l-izraz", false);
			logIliIzraz(dijeteCvor);
			
			if(!Pravila.pretvori(dijeteCvor.atributi.get("tip").toString(), Tipovi.INT.toString())) { //2.2
				pogreska(radniCvor);
			}
			
			dijeteCvor = radniCvor.getDjeca().get(2); //3.2
			logIIzraz(dijeteCvor);
			
			if(!Pravila.pretvori(dijeteCvor.atributi.get("tip").toString(), Tipovi.INT.toString())) { //4.2
				pogreska(radniCvor);
			}
		}
	}

	private static void logIIzraz(Cvor<Znak> radniCvor) {
		Cvor<Znak> dijeteCvor = radniCvor.getDjeca().get(0);  
		radniZnak = dijeteCvor.getVrijednost();
		
		if(radniZnak.getImeZnaka().equals("bin_ili_izraz")) {
			binIliIzraz(dijeteCvor); //1.1
			Object toAdd = dijeteCvor.atributi.get("tip");
			radniCvor.atributi.put("tip", toAdd);
			
			toAdd = dijeteCvor.atributi.get("l-izraz");
			radniCvor.atributi.put("l-izraz", toAdd);
		}
		else if(radniZnak.getImeZnaka().equals("log_i_izraz")) {
			radniCvor.atributi.put("tip", Tipovi.INT);
			radniCvor.atributi.put("l-izraz", false);
			logIliIzraz(dijeteCvor);
			
			if(!Pravila.pretvori(dijeteCvor.atributi.get("tip").toString(), Tipovi.INT.toString())) { //2.2
				pogreska(radniCvor);
			}
			
			dijeteCvor = radniCvor.getDjeca().get(2); //3.2
			binIliIzraz(dijeteCvor);
			
			if(!Pravila.pretvori(dijeteCvor.atributi.get("tip").toString(), Tipovi.INT.toString())) { //4.2
				pogreska(radniCvor);
			}
			
		}
		
	}

	private static void binIliIzraz(Cvor<Znak> radniCvor) {
		Cvor<Znak> dijeteCvor = radniCvor.getDjeca().get(0);  
		radniZnak = dijeteCvor.getVrijednost();
		
		if(radniZnak.getImeZnaka().equals("bin_xili_izraz")) {
			binXiliIzraz(dijeteCvor); //1.1
			Object toAdd = dijeteCvor.atributi.get("tip");
			radniCvor.atributi.put("tip", toAdd);
			
			toAdd = dijeteCvor.atributi.get("l-izraz");
			radniCvor.atributi.put("l-izraz", toAdd);
		}
		else if(radniZnak.getImeZnaka().equals("bin_ili_izraz")) {
			radniCvor.atributi.put("tip", Tipovi.INT);
			radniCvor.atributi.put("l-izraz", false);
			binIliIzraz(dijeteCvor);
			
			if(!Pravila.pretvori(dijeteCvor.atributi.get("tip").toString(), Tipovi.INT.toString())) { //2.2
				pogreska(radniCvor);
			}
			
			dijeteCvor = radniCvor.getDjeca().get(2); //3.2
			binXiliIzraz(dijeteCvor);
			
			if(!Pravila.pretvori(dijeteCvor.atributi.get("tip").toString(), Tipovi.INT.toString())) { //4.2
				pogreska(radniCvor);
			}
		}
	}
	
	private static void binXiliIzraz(Cvor<Znak> radniCvor) {
		Cvor<Znak> dijeteCvor = radniCvor.getDjeca().get(0);  
		radniZnak = dijeteCvor.getVrijednost();
		
		if(radniZnak.getImeZnaka().equals("bin_i_izraz")) {
			binIIzraz(dijeteCvor); //1.1
			Object toAdd = dijeteCvor.atributi.get("tip");
			radniCvor.atributi.put("tip", toAdd);
			
			toAdd = dijeteCvor.atributi.get("l-izraz");
			radniCvor.atributi.put("l-izraz", toAdd);
		}
		else if(radniZnak.getImeZnaka().equals("bin_ili_izraz")) {
			radniCvor.atributi.put("tip", Tipovi.INT);
			radniCvor.atributi.put("l-izraz", false);
			binXiliIzraz(dijeteCvor);
			
			if(!Pravila.pretvori(dijeteCvor.atributi.get("tip").toString(), Tipovi.INT.toString())) { //2.2
				pogreska(radniCvor);
			}
			
			dijeteCvor = radniCvor.getDjeca().get(2); //3.2
			binIIzraz(dijeteCvor);
			
			if(!Pravila.pretvori(dijeteCvor.atributi.get("tip").toString(), Tipovi.INT.toString())) { //4.2
				pogreska(radniCvor);
			}
		}	
	}
	
	private static void binIIzraz(Cvor<Znak> radniCvor) {
		Cvor<Znak> dijeteCvor = radniCvor.getDjeca().get(0);  
		radniZnak = dijeteCvor.getVrijednost();
		
		if(radniZnak.getImeZnaka().equals("jednakosni_izraz")) {
			jednakosniIzraz(dijeteCvor); //1.1
			Object toAdd = dijeteCvor.atributi.get("tip");
			radniCvor.atributi.put("tip", toAdd);
			
			toAdd = dijeteCvor.atributi.get("l-izraz");
			radniCvor.atributi.put("l-izraz", toAdd);
		}
		else if(radniZnak.getImeZnaka().equals("bin_i_izraz")) {
			radniCvor.atributi.put("tip", Tipovi.INT);
			radniCvor.atributi.put("l-izraz", false);
			binIIzraz(dijeteCvor);
			
			if(!Pravila.pretvori(dijeteCvor.atributi.get("tip").toString(), Tipovi.INT.toString())) { //2.2
				pogreska(radniCvor);
			}
			
			dijeteCvor = radniCvor.getDjeca().get(2); //3.2
			jednakosniIzraz(dijeteCvor);
			
			if(!Pravila.pretvori(dijeteCvor.atributi.get("tip").toString(), Tipovi.INT.toString())) { //4.2
				pogreska(radniCvor);
			}
		}
		
	}

	private static void naredbaPetlje(Cvor<Znak> radniCvor) {
		
		ArrayList<Cvor<Znak>> djeca = radniCvor.getDjeca();
		
		if(djeca.size()==5) {
			
			izraz(djeca.get(2));
			if(!Pravila.pretvori(djeca.get(2).atributi.get("tip").toString(),Tipovi.INT.toString()))
				pogreska(radniCvor);
			naredba(djeca.get(4));
			
		} else if(djeca.size()==6) {
			
			izrazNaredba(djeca.get(2));
			izrazNaredba(djeca.get(3));
			if(!Pravila.pretvori(djeca.get(3).atributi.get("tip").toString(),Tipovi.INT.toString()))
				pogreska(radniCvor);
			naredba(djeca.get(5));
			
		} else if(djeca.size()==7) {
			
			izrazNaredba(djeca.get(2));
			izrazNaredba(djeca.get(3));
			if(!Pravila.pretvori(djeca.get(3).atributi.get("tip").toString(),Tipovi.INT.toString()))
				pogreska(radniCvor);
			izraz(djeca.get(4));
			naredba(djeca.get(6));
			
		}
		
	}

	private static void naredbaGrananja(Cvor<Znak> radniCvor) {
		
		ArrayList<Cvor<Znak>> djeca = radniCvor.getDjeca();
		
		if(djeca.size()==5) {
			
			izraz(djeca.get(2));
			if(!Pravila.pretvori(djeca.get(2).atributi.get("tip").toString(),Tipovi.INT.toString()))
				pogreska(radniCvor);
			naredba(djeca.get(4));
			
		} else if(djeca.size()==7) {
			
			izraz(djeca.get(2));
			if(!Pravila.pretvori(djeca.get(2).atributi.get("tip").toString(),Tipovi.INT.toString()))
				pogreska(radniCvor);
			naredba(djeca.get(4));
			naredba(djeca.get(6));
		}
		
	}

	private static void izrazNaredba(Cvor<Znak> radniCvor) {
		
		ArrayList<Cvor<Znak>> djeca = radniCvor.getDjeca();
		
		if(djeca.size()==1) {
			
			radniCvor.atributi.put("tip", Tipovi.INT);
			
		} else if(djeca.size()==2) {
			
			izraz(djeca.get(0));
			
			radniCvor.atributi.put("tip", djeca.get(0).atributi.get("tip"));	
			
		}
		
	}

	public static void deklaracija(Cvor<Znak> radniCvor) {
		Cvor<Znak> dijeteCvor = radniCvor.getDjeca().get(0);  
		radniZnak = dijeteCvor.getVrijednost();
		imeTipa(dijeteCvor);
		
		dijeteCvor = radniCvor.getDjeca().get(1);
		
		dijeteCvor.atributi.put("ntip", radniCvor.getDjeca().get(0).atributi.get("tip"));
		listaInitDeklaratora(dijeteCvor);
		
	}
	
	public static void imeTipa(Cvor<Znak> radniCvor) {
		
		ArrayList<Cvor<Znak>> djeca = radniCvor.getDjeca();
		
		if(djeca.size()==1) {
			
			specifikatorTipa(djeca.get(0));
			
			radniCvor.atributi.put("tip", djeca.get(0).atributi.get("tip"));
			
		} else {
			
			specifikatorTipa(djeca.get(1));
			if((Tipovi) djeca.get(1).atributi.get("tip")==Tipovi.VOID)
				pogreska(radniCvor);
			
			radniCvor.atributi.put("tip", Pravila.pretvoriUKonstantu((Tipovi) djeca.get(1).atributi.get("tip")));
			
			}
		
	}	

	
	public static void postfiksIzraz(Cvor<Znak> radniCvor) {
		
		ArrayList<Cvor<Znak>> djeca = radniCvor.getDjeca();
		
		if(djeca.size()==1) {
			
			//produkcija u primarni_izraz
			
			primarniIzraz(djeca.get(0));
			
			radniCvor.atributi.put("tip", djeca.get(0).atributi.get("tip"));
			radniCvor.atributi.put("l-izraz", djeca.get(0).atributi.get("l-izraz"));
			
		} else if(djeca.size()==2) {
			
			
			
			postfiksIzraz(djeca.get(0));
			// provjeri
			if(!((boolean)djeca.get(0).atributi.get("l-izraz")==true && Pravila.pretvori(djeca.get(0).atributi.get("tip").toString(),Tipovi.INT.toString())));
				pogreska(radniCvor);
			
			radniCvor.atributi.put("tip", Tipovi.INT);
			radniCvor.atributi.put("l-izraz", false);
			
		} else if(djeca.size()==3) {
			
		
			postfiksIzraz(djeca.get(0));
			ArrayList<Tipovi> listaParametara = new ArrayList<>();
			listaParametara.add(Tipovi.VOID);
			if(!(((DeklaracijaFunkcije)djeca.get(0).atributi.get("tip")).tipoviParametara.equals(listaParametara)))
				pogreska(radniCvor);
			
			radniCvor.atributi.put("tip", ((DeklaracijaFunkcije)djeca.get(0).atributi.get("tip")).getPovratniTip());
			radniCvor.atributi.put("l-izraz", false);
			
			
		} else if(djeca.size()==4) {
			
			if(djeca.get(2).getVrijednost().getImeZnaka().equals("lista_argumenata")) {
			
				
				postfiksIzraz(djeca.get(0));
				listaArgumenata(djeca.get(2));
				if(!Pravila.mozeCastGrupno((ArrayList<Tipovi>)djeca.get(2).atributi.get("tipovi"),(ArrayList<Tipovi>)((DeklaracijaFunkcije)djeca.get(0).atributi.get("tip")).getTipoviParametara() )) {
					pogreska(radniCvor);
				}
				
				radniCvor.atributi.put("tip", ((DeklaracijaFunkcije)djeca.get(0).atributi.get("tip")).getPovratniTip());
				radniCvor.atributi.put("l-izraz", false);
				
			} else {
				
				postfiksIzraz(djeca.get(0));
				if(!(djeca.get(0).atributi.get("tip").toString().contains("NIZ")))
					pogreska(radniCvor);
				izraz(djeca.get(2));
				if(!(Pravila.pretvori(djeca.get(2).atributi.get("tip").toString(),Tipovi.INT.toString())))
					pogreska(radniCvor);	

				radniCvor.atributi.put("tip",djeca.get(0).atributi.get("tip"));
				radniCvor.atributi.put("l-izraz", Pravila.jeliTipKonstanta((Tipovi)radniCvor.atributi.get("tip")));
				
			}
			
		}
	}
	
	private static void listaIzrazaPridruzivanja(Cvor<Znak> cvor) {
		
		ArrayList<Cvor<Znak>> djeca = cvor.getDjeca();
		
		if(djeca.size()==1) {
			
			ArrayList<Tipovi> tipovi = new ArrayList<Tipovi>();
			
			izrazPridruzivanja(djeca.get(0));
			
			tipovi.add((Tipovi) djeca.get(0).atributi.get("tip"));
			cvor.atributi.put("tipovi", tipovi);
			cvor.atributi.put("br-elem",1);
			
		} else if (djeca.size()==3) {
			
			listaIzrazaPridruzivanja(djeca.get(0));
			izrazPridruzivanja(djeca.get(2));
			
			ArrayList<Tipovi> tipovi = (ArrayList<Tipovi>)djeca.get(0).atributi.get("tipovi");
			tipovi.add((Tipovi) djeca.get(2).atributi.get("tip"));
			cvor.atributi.put("tipovi", tipovi);
			
			cvor.atributi.put("br-elem", ((int)cvor.atributi.get("br-elem")+1));
		}
		
	}
	
	private static void listaParametara(Cvor<Znak> cvor) {
		
		ArrayList<Cvor<Znak>> djeca = cvor.getDjeca();
		
		if(djeca.size()==1) {
			
			ArrayList<Tipovi> tipovi = new ArrayList<Tipovi>();
			ArrayList<String> imena = new ArrayList<String>();
			
			deklaracijaParametra(djeca.get(0));
			
			tipovi.add((Tipovi) djeca.get(0).atributi.get("tip"));
			cvor.atributi.put("tipovi", tipovi);
			
			imena.add((String) djeca.get(0).atributi.get("ime"));
			cvor.atributi.put("imena", imena);
			
		} else if(djeca.size()==3) {
			
			listaParametara(djeca.get(0));
			deklaracijaParametra(djeca.get(2));
			
			ArrayList<String> imena = (ArrayList<String>)djeca.get(0).atributi.get("imena");
			
			if(!imena.contains(djeca.get(2).atributi.get("ime")))
				pogreska(cvor);
			
			ArrayList<Tipovi> tipovi = (ArrayList<Tipovi>)djeca.get(0).atributi.get("tipovi");
			tipovi.add((Tipovi) djeca.get(2).atributi.get("tip"));
			cvor.atributi.put("tipovi", tipovi);
			
			imena.add((String) djeca.get(2).atributi.get("ime"));
			cvor.atributi.put("imena", imena);
			
		}
		
	}

	
	private static void listaInitDeklaratora(Cvor<Znak> cvor) {
			
		ArrayList<Cvor<Znak>> djeca = cvor.getDjeca();
		
		if(djeca.size()==1) {
			
			djeca.get(0).atributi.put("ntip", cvor.atributi.get("ntip"));
			initDeklarator(djeca.get(0));
			
		} else if(djeca.size()==3) {
			
			djeca.get(0).atributi.put("ntip", cvor.atributi.get("ntip"));
			listaInitDeklaratora(djeca.get(0));
			
			djeca.get(2).atributi.put("ntip", cvor.atributi.get("ntip"));
			initDeklarator(djeca.get(2));
		}
		
	}
	

	private static void deklaracijaParametra(Cvor<Znak> radniCvor) {
		ArrayList<Cvor<Znak>> djeca = radniCvor.getDjeca();
		
		if(djeca.size()==2) {
			
			imeTipa(djeca.get(0));
			if((Tipovi)djeca.get(0).atributi.get("tip") == Tipovi.VOID)
				pogreska(radniCvor);
			
			radniCvor.atributi.put("tip",djeca.get(0).atributi.get("tip"));
			radniCvor.atributi.put("ime",djeca.get(1).getVrijednost().getZavrsniUProgramu());
			
		} else if(djeca.size()==4) {
			
			imeTipa(djeca.get(0));
			if((Tipovi)djeca.get(0).atributi.get("tip") != Tipovi.VOID)
				pogreska(radniCvor);
			
			radniCvor.atributi.put("tip",Pravila.pretvoriUNiz((Tipovi)djeca.get(0).atributi.get("tip")));
			radniCvor.atributi.put("ime",djeca.get(1).getVrijednost().getZavrsniUProgramu());
			
		}
		
		Djelokrug djelokrug = vratiDjelokrug(radniCvor);
		List<Varijabla> lista = djelokrug.getListaVarijabla();
		Varijabla varijabla = new Varijabla((String)radniCvor.atributi.get("ime"),(Tipovi)radniCvor.atributi.get("tip"));
		varijabla.setDefined(true);
		lista.add(varijabla);
	}
	
	private static void initDeklarator(Cvor<Znak> radniCvor) {
		
		ArrayList<Cvor<Znak>> djeca = radniCvor.getDjeca();
		
		if(djeca.size()==1) {
			
			djeca.get(0).atributi.put("ntip", radniCvor.atributi.get("ntip"));
			izravni_deklarator(djeca.get(0));
			
			if(Pravila.jeliTipKonstanta((Tipovi)djeca.get(0).atributi.get("tip"))) {
				pogreska(radniCvor);
			}
			
		} else if(djeca.size()== 3) {
			
			djeca.get(0).atributi.put("ntip", radniCvor.atributi.get("ntip"));
			izravni_deklarator(djeca.get(0));
			inicijalizator(djeca.get(2));
			
			if(!Pravila.jeliTipNiz((Tipovi)djeca.get(0).atributi.get("tip"))) {
				
				if(!Pravila.pretvori(djeca.get(2).atributi.get("tip").toString(),Pravila.pretvoriUObicni((Tipovi)djeca.get(0).atributi.get("tip")).toString()))
					pogreska(radniCvor);
				
			} else {
				
				if(!((int)djeca.get(2).atributi.get("br-elem")<=(int)djeca.get(0).atributi.get("br-elem")))
					pogreska(radniCvor);
				List<Tipovi> tipovi = (List<Tipovi>)djeca.get(2).atributi.get("tipovi");
				for(Tipovi tip : tipovi)
					if(!Pravila.pretvori(tip.toString(),Pravila.pretvoriUObicni(((Tipovi)djeca.get(0).atributi.get("tip"))).toString()))
						pogreska(radniCvor);
			}
			
		}

		Djelokrug djelokrug = vratiDjelokrug(radniCvor);
		List<Varijabla> lista = djelokrug.getListaVarijabla();
		Varijabla varijabla = new Varijabla((String)radniCvor.atributi.get("ime"),(Tipovi)radniCvor.atributi.get("tip"));
		if(djeca.size()==1) {
			varijabla.setDefined(false);
		} else {
			varijabla.setDefined(true);
		}
		lista.add(varijabla);
	}

	private static void listaArgumenata(Cvor<Znak> cvor) {
		
		ArrayList<Cvor<Znak>> djeca = cvor.getDjeca();
		
		if(djeca.size()==1) {
			
			ArrayList<Tipovi> tipovi = new ArrayList<Tipovi>();
			
			izrazPridruzivanja(cvor.getRodCvor());
			
			tipovi.add((Tipovi) djeca.get(0).atributi.get("tip"));
			cvor.atributi.put("tipovi", tipovi);
			
		} else {
			
			listaArgumenata(cvor.getRodCvor());
			izrazPridruzivanja(cvor.getRodCvor());
			
			ArrayList<Tipovi> tipovi = (ArrayList<Tipovi>)djeca.get(0).atributi.get("tipovi");
			tipovi.add((Tipovi) djeca.get(2).atributi.get("tip"));
			cvor.atributi.put("tipovi", tipovi);
		}
	}

	private static void primarniIzraz(Cvor<Znak> radniCvor) {
		Cvor<Znak> dijeteCvor = radniCvor.getDjeca().get(0);  
		radniZnak = dijeteCvor.getVrijednost();
		
		if(radniZnak.getImeZnaka().equals("IDN")) {
			Varijabla tmp = (Varijabla)pronadiDjelokrug(radniZnak.getZavrsniUProgramu(), radniCvor, "var");
			if(tmp == null) {
				// Now try to find a function call?
				DeklaracijaFunkcije tmp_dek = (DeklaracijaFunkcije)pronadiDjelokrug(radniZnak.getZavrsniUProgramu(), radniCvor, "fun");
				if(tmp_dek == null) {
					pogreska(radniCvor);
				}
			}
			
			Object toAdd = tmp.getTip();
			radniCvor.atributi.put("tip", toAdd);
			
			toAdd = dijeteCvor.atributi.get("l-izraz");
			radniCvor.atributi.put("l-izraz", toAdd);
		
		} else if(radniZnak.getImeZnaka().equals("BROJ")){
			try {
				Integer.parseInt(radniZnak.getZavrsniUProgramu());
			} catch(Exception e) {
				pogreska(radniCvor);
			}
			
			radniCvor.atributi.put("tip", Tipovi.INT);
			radniCvor.atributi.put("l-izraz", false);

		} else if(radniZnak.getImeZnaka().equals("ZNAK")) {
			if(!Pravila.jeliDobarChar(dijeteCvor.getVrijednost().getZavrsniUProgramu())) {		// If character doesn't satisfy the requirements
				pogreska(radniCvor);
			}
			
			// After checking let's put attributed
			radniCvor.atributi.put("tip", Tipovi.CHAR);
			radniCvor.atributi.put("l-izraz", false);
			
		} else if(radniZnak.getImeZnaka().equals("NIZ_ZNAKOVA")) {
			if(!Pravila.provjeriNizZnakova(radniZnak.getZavrsniUProgramu()))
					pogreska(radniCvor);
			
			radniCvor.atributi.put("tip", Tipovi.NIZ_CONST_CHAR);
			radniCvor.atributi.put("l-izraz", false);
			 
		} else if(radniZnak.getImeZnaka().equals("L_ZAGRADA")) {
			dijeteCvor = radniCvor.getDjeca().get(1);
			izraz(dijeteCvor);
			
			Object toAdd = dijeteCvor.atributi.get("tip");
			radniCvor.atributi.put("tip", toAdd);
			
			toAdd = dijeteCvor.atributi.get("l-izraz");
			radniCvor.atributi.put("l-izraz", toAdd);
		}
	}

	public static void pogreska(Cvor<Znak> radniCvor){

		String ispis ="";

		ispis+="<"+radniCvor.getVrijednost().getImeZnaka()+">";

		ispis+=" ::=";
		for(Cvor<Znak> dijete : radniCvor.getDjeca()) {
			
			if(dijete.getVrijednost().isJeliNezavrsni()) {
				
				ispis+=(" <"+dijete.getVrijednost().getImeZnaka()+">");
			
			} else {
				
				Znak znak =dijete.getVrijednost();
				ispis+=(" "+znak.getImeZnaka()+"("+znak.getRedakZavrsnog()+","+znak.getZavrsniUProgramu()+")");
				
			}
				
			System.out.println(ispis);
		}
		
		System.exit(1);
		
	}
	
	public static Cvor<Znak> pronadjiCvor(ArrayList<Cvor<Znak>> sviCvorovi , int indeksUListi, Cvor<Znak> glava ,int brojacPraznina) {
		for(Cvor<Znak> cvoric : sviCvorovi) {
			if(cvoric.getIndeksUlisti()==indeksUListi)
				return cvoric;
		}
		throw new IllegalStateException("Nije moguce pronaci cvor "+ indeksUListi + " "+ sviCvorovi.toString());
	}
	
	public static int brojPraznina(String niz) {
		
		return niz.length()-niz.trim().length();
	}
	
	public static Cvor<Djelokrug> kreirajStabloDjelokruga(Cvor<Znak> glavaOriginalnogStabla, ArrayList<Cvor<Znak>> sviCvorovi) {
		
		int provjereniCvorovi = 0;
		Cvor<Djelokrug> glavaStabla = null;
		boolean nijePostavljenGlobalniDjelokrug = true;
		
		for(Cvor<Znak> trenutniCvor : sviCvorovi)
			
			if(trenutniCvor.getVrijednost().getImeZnaka().equals("prijevodna_jedinica") && nijePostavljenGlobalniDjelokrug) {
				
				nijePostavljenGlobalniDjelokrug = false;
				glavaStabla = new Cvor<Djelokrug>(new ArrayList<Cvor<Djelokrug>>(),new Djelokrug());
				trenutniCvor.setImaSvojDjelokrug(true);
				trenutniCvor.setReferenca(glavaStabla);
				glavaStabla.setSlozenaNaredba(trenutniCvor);
				
			} else if(trenutniCvor.getVrijednost().getImeZnaka().equals("slozena_naredba")) {
				
				Cvor<Znak> roditeljskiCvor = trenutniCvor.getRodCvor();
				
				while(!roditeljskiCvor.isImaSvojDjelokrug()) {
					
				roditeljskiCvor = roditeljskiCvor.getRodCvor();
				
			}
				
				trenutniCvor.setImaSvojDjelokrug(true);
				Cvor<Djelokrug> roditeljskiDjelCvor = roditeljskiCvor.getReferenca();
				Cvor<Djelokrug> cvorDjelokruga = new Cvor<Djelokrug>(new ArrayList<Cvor<Djelokrug>>(),new Djelokrug());
				cvorDjelokruga.setRodCvor(roditeljskiDjelCvor);
				roditeljskiDjelCvor.dodajDijete(cvorDjelokruga);
				trenutniCvor.setReferenca(cvorDjelokruga);
				cvorDjelokruga.setSlozenaNaredba(trenutniCvor);
			
		}
		
		return glavaStabla;
	}

	public static Djelokrug vratiDjelokrug(Cvor<Znak> radniCvor) {
		Djelokrug tmp;
		//TODO optimize this
		
		while(!radniCvor.isImaSvojDjelokrug()) {
			radniCvor = radniCvor.getRodCvor();
		}
		
		tmp = radniCvor.getReferenca().getVrijednost();
		return tmp;
	}
	
	public static Object pronadiDjelokrug(String ime, Cvor<Znak> radniCvor, String varORfun) {
		if(!(varORfun.equals("var") || varORfun.equals("fun"))) {
			throw new IllegalArgumentException("Pisi kako treba klinjo");
		}
		
		Djelokrug tmp;
		//TODO optimize this
		
		while(!radniCvor.isImaSvojDjelokrug()) {
			radniCvor = radniCvor.getRodCvor();
		}
		
		Cvor<Djelokrug> djelokrugCvor = radniCvor.getReferenca();
		
		do {
			tmp = djelokrugCvor.getVrijednost();
			if(varORfun.equals("var")) {
				for(Varijabla var: tmp.getListaVarijabla()) {
					if(var.getIme().equals(ime)){
						return var;
					}
				}
			} else if(varORfun.equals("fun")) {
				for(DeklaracijaFunkcije fun: tmp.getListaDeklaracija()) {
					if(fun.getIme().equals(ime)){
						return fun;
					}
				}
			}
			
			djelokrugCvor = djelokrugCvor.getRodCvor();
		} while(djelokrugCvor != null);

		return null;
	}
	
	public static void multiplikativniIzraz(Cvor<Znak> radniCvor) {
		Cvor<Znak> dijeteCvor = radniCvor.getDjeca().get(0);  
		radniZnak = dijeteCvor.getVrijednost();
		
		if(radniZnak.getImeZnaka().equals("cast_izraz")) {
			castIzraz(dijeteCvor);
			
			Object toAdd = dijeteCvor.atributi.get("tip");
			radniCvor.atributi.put("tip", toAdd);
			
			toAdd = dijeteCvor.atributi.get("l-izraz");
			radniCvor.atributi.put("l-izraz", toAdd);
			
		} else {
			radniCvor.atributi.put("tip", Tipovi.INT);
			radniCvor.atributi.put("l-value", false);
			
			multiplikativniIzraz(dijeteCvor);
			
			if(!Pravila.pretvori(dijeteCvor.atributi.get("tip").toString(), "INT"))
				pogreska(radniCvor);
			
			castIzraz(dijeteCvor);
			
			dijeteCvor = radniCvor.getDjeca().get(2);
			if(!Pravila.pretvori(dijeteCvor.atributi.get("tip").toString(), "INT"))
				pogreska(radniCvor);
		}
	}

	public static void castIzraz(Cvor<Znak> radniCvor) {
		Cvor<Znak> dijeteCvor = radniCvor.getDjeca().get(0);  
		radniZnak = dijeteCvor.getVrijednost();
		
		if(radniZnak.getImeZnaka().equals("unarni_izraz")) {
			unarniIzraz(dijeteCvor);
			
			Object toAdd = dijeteCvor.atributi.get("tip");
			radniCvor.atributi.put("tip", toAdd);
			
			toAdd = dijeteCvor.atributi.get("l-izraz");
			radniCvor.atributi.put("l-izraz", toAdd);
			
		} else if(radniZnak.getImeZnaka().equals("L_ZAGRADA")) {
			dijeteCvor = radniCvor.getDjeca().get(1);
			imeTipa(dijeteCvor);
			
			Object toAdd = dijeteCvor.atributi.get("tip");
			radniCvor.atributi.put("tip", toAdd);
			
			radniCvor.atributi.put("l-izraz", false);
			
			dijeteCvor = radniCvor.getDjeca().get(3);
			castIzraz(dijeteCvor);
			
			Tipovi pomTip = (Tipovi) radniCvor.getDjeca().get(1).atributi.get("tip");
			if(!Pravila.mozeCast(dijeteCvor.atributi.get("tip").toString(), pomTip.toString()))
				pogreska(radniCvor);
		}
	}
	
	private static void unarniIzraz(Cvor<Znak> radniCvor) {
		Cvor<Znak> dijeteCvor = radniCvor.getDjeca().get(0);  
		radniZnak = dijeteCvor.getVrijednost();
		
		if(radniZnak.getImeZnaka().equals("postfiks_izraz")) {
			postfiksIzraz(dijeteCvor);
			
			Object toAdd = dijeteCvor.atributi.get("tip");
			radniCvor.atributi.put("tip", toAdd);
			
			toAdd = dijeteCvor.atributi.get("l-izraz");
			radniCvor.atributi.put("l-izraz", toAdd);
		}
		else if(radniZnak.getImeZnaka().equals("unarni_operator")) {
			radniCvor.atributi.put("tip", Tipovi.INT);
			radniCvor.atributi.put("l-izraz", false);
			
			dijeteCvor = radniCvor.getDjeca().get(1);
			castIzraz(dijeteCvor);
			
			if(!Pravila.pretvori(dijeteCvor.atributi.get("tip").toString(),Tipovi.INT.toString()))
				pogreska(radniCvor);
		}
		else {
			radniCvor.atributi.put("tip", Tipovi.INT);
			radniCvor.atributi.put("l-izraz", false);
			
			unarniIzraz(dijeteCvor);
			if(!((boolean) dijeteCvor.atributi.get("l-izraz") && 
				Pravila.pretvori(dijeteCvor.atributi.get("tip").toString(),Tipovi.INT.toString()))) {
				pogreska(radniCvor);
			}
		}
	}

	public static void specifikatorTipa(Cvor<Znak> radniCvor) {
		Cvor<Znak> dijeteCvor = radniCvor.getDjeca().get(0);
		radniZnak = dijeteCvor.getVrijednost();
		
		if(radniZnak.getImeZnaka().equals("KR_VOID")) {
			radniCvor.atributi.put("tip", Tipovi.VOID);
		} else if(radniZnak.getImeZnaka().equals("KR_CHAR")) {
			radniCvor.atributi.put("tip", Tipovi.CHAR);
		} else if(radniZnak.getImeZnaka().equals("KR_INT")) {
			radniCvor.atributi.put("tip", Tipovi.INT);
		}
		
		
	}

	public static void izravni_deklarator(Cvor<Znak> radniCvor) {
		Cvor<Znak> dijeteCvor = radniCvor.getDjeca().get(0);  
		
		List<Cvor<Znak>> children = radniCvor.getDjeca();
		int childrenNum = children.size();
		
		if(childrenNum == 4) {
			// They differ from the second index
			if(children.get(1).getVrijednost().getImeZnaka().equals("L_UGL_ZAGRADA")) {
				// Prod  1
				// First add the attribute
				// Check that ntip isn't void
				Tipovi ntip = (Tipovi)dijeteCvor.atributi.get("ntip");
				if(ntip == Tipovi.VOID) {
					pogreska(radniCvor);
				}
				
				// Now check if the name isn't already declared in the local scope
				// Need to check if the local scope contains IDN
				String toCheckName = dijeteCvor.getVrijednost().getZavrsniUProgramu(); 
				Djelokrug tmp = localScope(toCheckName, radniCvor);
				// Now need to check whether the localScope contains this var
				List<String> localScopeNames = tmp.getListaDeklaracija().stream()
					.map((elem) -> elem.getIme())
					.collect(Collectors.toList());
				if(localScopeNames.contains(toCheckName)) {
					pogreska(radniCvor);
				}
				
				// Now make the declaration and add it to the local scope
				Varijabla newVar = new Varijabla(toCheckName, ntip);
				tmp.getListaVarijabla().add(newVar);
				
				// Put the corresponding attribs
				radniCvor.atributi.put("tip", ntip);
			} else if(children.get(1).getVrijednost().getImeZnaka().equals("L_ZAGRADA") && children.get(2).getVrijednost().getImeZnaka().equals("KR_VOID")) {
				// Prod 2
				// Check that ntip isn't void
				Tipovi ntip = (Tipovi)radniCvor.atributi.get("ntip");
				if(ntip == Tipovi.VOID) {
					pogreska(radniCvor);
				}
				
				// Check if IDN.ime isn't declared in the local scope
				String toCheckName = dijeteCvor.getVrijednost().getZavrsniUProgramu(); 
				Djelokrug tmp = localScope(toCheckName, radniCvor);
				// Now need to check whether the localScope contains this var
				List<String> localScopeNames = tmp.getListaDeklaracija().stream()
					.map((elem) -> elem.getIme())
					.collect(Collectors.toList());
				if(localScopeNames.contains(toCheckName)) {
					pogreska(radniCvor);
				}
				
				// Check number scope
				if( !Pravila.jeliBrojURasponu(Integer.parseInt(dijeteCvor.getVrijednost().getZavrsniUProgramu())) ) {
					pogreska(radniCvor);
				}
				
				// Time to add the attribs
				// Need to decode ntip and get its array version
				String expectedTip = ntip.toString() + "_NIZ";
				Tipovi arrayType = Tipovi.valueOf(expectedTip);
				// Now make the declaration and add it to the local scope
				Varijabla newVar = new Varijabla(toCheckName, arrayType);
				tmp.getListaVarijabla().add(newVar);
				
				// Put the br-elem attrib
				radniCvor.atributi.put("br-elem", radniCvor.getDjeca().get(2).getVrijednost().getZavrsniUProgramu());
			} else {
				// Prod 3
				// Now we're making functions
				// Check for declarations inside local(?) scope
				String toCheckName = dijeteCvor.getVrijednost().getZavrsniUProgramu(); 
				Djelokrug tmp = localScope(toCheckName, radniCvor);
				Tipovi ntip = (Tipovi)radniCvor.atributi.get("ntip");
				
				boolean alreadyPresent = false;
				DeklaracijaFunkcije currDecl = new DeklaracijaFunkcije(toCheckName, ntip, Tipovi.VOID);
				for(DeklaracijaFunkcije tmpDecl : tmp.getListaDeklaracija()) {
					if(currDecl.getIme().equals(dijeteCvor.getVrijednost().getZavrsniUProgramu())) {
						// Same name functions - check if everything else matches
						if(!currDecl.compareReturnAndParamsType(tmpDecl)) {
							pogreska(radniCvor);		// Same name function declaration, but different return/parameter types
						} else {
							// A good declaration!
							alreadyPresent = true;
						}
					}
				}
				
				if(!alreadyPresent) {
					// Add this declaration to the local scope
					tmp.getListaDeklaracija().add(currDecl);
				}
				
				// Finally put the attributes
				radniCvor.atributi.put("tip", currDecl);
			}
		} else if(childrenNum == 1) {
			// Prod 0
			// First check that ntip isn't VOID
			String toCheckName = dijeteCvor.getVrijednost().getZavrsniUProgramu(); 
			Djelokrug tmp = localScope(toCheckName, radniCvor);
			Tipovi ntip = (Tipovi)radniCvor.atributi.get("ntip");
			
			if(ntip == Tipovi.VOID) {
				pogreska(radniCvor);
			}
			
			// Now check that IDN.ime isn't declared in the local scope
			// Now need to check whether the localScope contains this var
			List<String> localScopeNames = tmp.getListaDeklaracija().stream()
				.map((elem) -> elem.getIme())
				.collect(Collectors.toList());
			if(localScopeNames.contains(toCheckName)) {
				pogreska(radniCvor);
			}
			
			// Now make the declaration and add it to the local scope
			Varijabla newVar = new Varijabla(toCheckName, ntip);
			tmp.getListaVarijabla().add(newVar);
			
			// At last, add the attribs
			radniCvor.atributi.put("tip", newVar);
		}
	}
	
	private static Djelokrug localScope(String name, Cvor<Znak> radniCvor) {
		
		while(!radniCvor.isImaSvojDjelokrug()) {
			// Now need to check whether the name is located in the variable names list
			radniCvor = radniCvor.getRodCvor();
		}
		
		// Now check if the var is located in the local scope
		return radniCvor.getReferenca().getVrijednost(); 
	}
	
	public static void jednakosniIzraz(Cvor<Znak> radniCvor) {
		Cvor<Znak> dijeteCvor = radniCvor.getDjeca().get(0);  
		radniZnak = dijeteCvor.getVrijednost();
		
		if(radniZnak.getImeZnaka().equals("odnosni_izraz")) {
			odnosniIzraz(dijeteCvor);
			
			Object toAdd = dijeteCvor.atributi.get("tip");
			radniCvor.atributi.put("tip", toAdd);
			
			toAdd = dijeteCvor.atributi.get("l-izraz");
			radniCvor.atributi.put("l-izraz", toAdd);
			
		} else if(radniZnak.getImeZnaka().equals("jednakosni_izraz")) {
			radniCvor.atributi.put("tip", Tipovi.INT);
			radniCvor.atributi.put("l-value", false);
			
			jednakosniIzraz(dijeteCvor);
			
			if(!Pravila.pretvori(dijeteCvor.atributi.get("tip").toString(), "INT"))
				pogreska(radniCvor);
			
			dijeteCvor = radniCvor.getDjeca().get(2);
			odnosniIzraz(dijeteCvor);
			
			if(!Pravila.pretvori(dijeteCvor.atributi.get("tip").toString(), "INT"))
				pogreska(radniCvor);	
		}
	}

	public static void odnosniIzraz(Cvor<Znak> radniCvor) {

		Cvor<Znak> dijeteCvor = radniCvor.getDjeca().get(0);  
		radniZnak = dijeteCvor.getVrijednost();
		
		if(radniZnak.getImeZnaka().equals("aditivni_izraz")) {
			aditivniIzraz(dijeteCvor);
			
			Object toAdd = dijeteCvor.atributi.get("tip");
			radniCvor.atributi.put("tip", toAdd);
			
			toAdd = dijeteCvor.atributi.get("l-izraz");
			radniCvor.atributi.put("l-izraz", toAdd);
			
		} else if(radniZnak.getImeZnaka().equals("odnosni_izraz")) {
			radniCvor.atributi.put("tip", Tipovi.INT);
			radniCvor.atributi.put("l-value", false);
			
			odnosniIzraz(dijeteCvor);
			
			if(!Pravila.pretvori(dijeteCvor.atributi.get("tip").toString(), "INT"))
				pogreska(radniCvor);
			
			dijeteCvor = radniCvor.getDjeca().get(2);
			aditivniIzraz(dijeteCvor);
			
			if(!Pravila.pretvori(dijeteCvor.atributi.get("tip").toString(), "INT"))
				pogreska(radniCvor);
			
		}
	}

	private static void aditivniIzraz(Cvor<Znak> radniCvor) {
		Cvor<Znak> dijeteCvor = radniCvor.getDjeca().get(0);
		radniZnak = dijeteCvor.getVrijednost();
		
		if(radniZnak.getImeZnaka().equals("multiplikativni_izraz")) {
			multiplikativniIzraz(dijeteCvor);
			
			Object toAdd = dijeteCvor.atributi.get("tip");
			radniCvor.atributi.put("tip", toAdd);
			
			toAdd = dijeteCvor.atributi.get("l-izraz");
			radniCvor.atributi.put("l-izraz", toAdd);
			
		} else if(radniZnak.getImeZnaka().equals("aditivni_izraz")) {
			radniCvor.atributi.put("tip", Tipovi.INT);
			radniCvor.atributi.put("l-value", false);
			
			aditivniIzraz(dijeteCvor);
			
			if(!Pravila.pretvori(dijeteCvor.atributi.get("tip").toString(), "INT"))
				pogreska(radniCvor);
			
			dijeteCvor = radniCvor.getDjeca().get(2);
			multiplikativniIzraz(dijeteCvor);
			
			if(!Pravila.pretvori(dijeteCvor.atributi.get("tip").toString(), "INT"))
				pogreska(radniCvor);
		}
	}
	
	private static void provjeraDefinicijaFunkcija() {
		Cvor<Djelokrug> globalniDjelokrug = glava.getReferenca();
		Set<DeklaracijaFunkcije> sveDeklaracije = new HashSet<>();
		
		obilazakStablaZaDeklaracije(sveDeklaracije, globalniDjelokrug, true);
		Set<String> imenaSvihDefiniranihFunkcija = new HashSet<>();
		for(DeklaracijaFunkcije jednaDefinicija: sveDeklariraneFunkcije) {
			if(jednaDefinicija.definiranaFunkcija)
				imenaSvihDefiniranihFunkcija.add(jednaDefinicija.getIme());
		}
		
		for(DeklaracijaFunkcije jednaDeklaracija: sveDeklaracije) {
			String name = jednaDeklaracija.getIme();
			if(!imenaSvihDefiniranihFunkcija.contains(name)) {
				System.out.println("funkcija");
				System.exit(-1);
			}
		}
		
	}
	
	private static void obilazakStablaZaDeklaracije(Set<DeklaracijaFunkcije> sveDeklaracije, Cvor<Djelokrug> radniCvor, boolean global) {
		if(radniCvor == null) return;
		
		List<DeklaracijaFunkcije> lokalneDeklaracije;
		if(global) {
			for(DeklaracijaFunkcije jednaDeklaracija: sveDeklariraneFunkcije) {
				if(jednaDeklaracija.definiranaFunkcija)
					sveDeklaracije.add(jednaDeklaracija);
			}
		} else {
			lokalneDeklaracije = radniCvor.getVrijednost().getListaDeklaracija();
			sveDeklaracije.addAll(lokalneDeklaracije);
		}
		
		List<Cvor<Djelokrug>> djeca = radniCvor.getDjeca();
		for(Cvor<Djelokrug> dijete: djeca) {
			obilazakStablaZaDeklaracije(sveDeklaracije, dijete, false);
		}
	}
		
	private static void deklaracijaParametara (Cvor<Znak> radniCvor) {
		ArrayList<Cvor<Znak>> djeca = radniCvor.getDjeca();
		
		if(djeca.size()==2) {
			
			imeTipa(djeca.get(0));
			if((Tipovi)djeca.get(0).atributi.get("tip") != Tipovi.VOID)
				pogreska(radniCvor);
			
			radniCvor.atributi.put("tip",djeca.get(0).atributi.get("tip"));
			radniCvor.atributi.put("ime",djeca.get(1).getVrijednost().getZavrsniUProgramu());
			
		} else if(djeca.size()==4) {
			
			imeTipa(djeca.get(0));
			if((Tipovi)djeca.get(0).atributi.get("tip") != Tipovi.VOID)
				pogreska(radniCvor);
			
			radniCvor.atributi.put("tip",Pravila.pretvoriUNiz((Tipovi)djeca.get(0).atributi.get("tip")));
			radniCvor.atributi.put("ime",djeca.get(1).getVrijednost().getZavrsniUProgramu());
			
		}
		
		Djelokrug djelokrug = vratiDjelokrug(radniCvor);
		List<Varijabla> lista = djelokrug.getListaVarijabla();
		Varijabla varijabla = new Varijabla((String)radniCvor.atributi.get("ime"),(Tipovi)radniCvor.atributi.get("tip"));
		varijabla.setDefined(true);
		lista.add(varijabla);
	}

	private static void inicijalizator(Cvor<Znak> cvor) {
		
		ArrayList<Cvor<Znak>> djeca = cvor.getDjeca();
		ArrayList<Tipovi> tipovi = new ArrayList<>();
		
		if(djeca.size()==1) {
			
			izrazPridruzivanja(djeca.get(0));
			
			if(djeca.get(0).atributi.get("tip")==Tipovi.NIZ_CONST_CHAR) {
				cvor.atributi.put("tip", djeca.get(0).getVrijednost().getZavrsniUProgramu().length()+1);
				for(int i = 0;i<djeca.get(0).getVrijednost().getZavrsniUProgramu().length();++i)
					tipovi.add(Tipovi.CHAR);
				cvor.atributi.put("tipovi",tipovi);
			} else {
				cvor.atributi.put("tip", djeca.get(0).atributi.get("tip"));
			}
			
		} else if(djeca.size()==3) {
			
			cvor.atributi.put("br-elem", djeca.get(1).atributi.get("br-elem"));
			cvor.atributi.put("tip", djeca.get(1).atributi.get("tip"));
			
		}
		
	}
	
}

