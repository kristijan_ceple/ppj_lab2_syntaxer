

import java.util.ArrayList;
import java.util.HashMap;

public class Cvor<T> {

	private ArrayList<Cvor<T>> djeca;
	private T vrijednost;
	private int indeksUlisti;
	private Cvor<T> rodCvor;
	private boolean imaSvojDjelokrug;
	Cvor<Djelokrug> referenca;
	private Cvor<Znak> slozenaNaredba;
	HashMap<String,Object> atributi = new HashMap<>();
	
	public Cvor(ArrayList<Cvor<T>> djeca, T vrijednost) {
		super();
		this.djeca = djeca;
		this.vrijednost = vrijednost;
	}
	
	public Cvor(ArrayList<Cvor<T>> djeca, T vrijednost, int indeksUlisti) {
		super();
		this.djeca = djeca;
		this.vrijednost = vrijednost;
		this.indeksUlisti = indeksUlisti;
	}
	
	public Cvor(T vrijednost, int indeks) {
		this.djeca = new ArrayList<>();
		this.vrijednost = vrijednost;
		this.indeksUlisti = indeks;
	}

	public boolean isImaSvojDjelokrug() {
		return imaSvojDjelokrug;
	}

	public void setImaSvojDjelokrug(boolean imaSvojDjelokrug) {
		this.imaSvojDjelokrug = imaSvojDjelokrug;
	}

	public Cvor<Djelokrug> getReferenca() {
		return referenca;
	}

	public void setReferenca(Cvor<Djelokrug> referenca) {
		this.referenca = referenca;
	}

	public void dodajDijete(Cvor<T> vrijednost) {
		djeca.add(vrijednost);
	}

	public int getIndeksUlisti() {
		return indeksUlisti;
	}

	public void setIndeksUlisti(int indeksUlisti) {
		this.indeksUlisti = indeksUlisti;
	}

	public String obidjiPodstablo() {

		String rez = "";
		rez += (" " + this.vrijednost.toString());
		if (!djeca.isEmpty()) {
			for (Cvor<T> dijete : djeca) {
				rez += dijete.obidjiPodstablo();
			}
		}

		return rez;
	}

	public ArrayList<Cvor<T>> getDjeca() {
		return djeca;
	}

	public T getVrijednost() {
		return vrijednost;
	}
	
	@Override
	public String toString() {
		return "ime:"+this.vrijednost.toString()+" indeks:"+this.indeksUlisti;
	}

	public Cvor<T> getRodCvor() {
		return rodCvor;
	}

	public void setRodCvor(Cvor<T> rodCvor) {
		this.rodCvor = rodCvor;
	}
	
	public Cvor<Znak> getSlozenaNaredba() {
		return slozenaNaredba;
	}

	public void setSlozenaNaredba(Cvor<Znak> slozenaNaredba) {
		this.slozenaNaredba = slozenaNaredba;
	}
}
