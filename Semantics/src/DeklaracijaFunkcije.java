import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DeklaracijaFunkcije {
		String ime;
		Tipovi povratniTip;
		Set<Tipovi> tipoviParametara = new HashSet<>();
		boolean definiranaFunkcija;
		
		
		public DeklaracijaFunkcije(String ime, Tipovi povratniTip, List<Tipovi> tipoviParametara) {
			this(ime, povratniTip);
			
			this.tipoviParametara.addAll(tipoviParametara);
		}
		
		public DeklaracijaFunkcije(String ime, Tipovi povratniTip, Tipovi tipParametra) {
			this(ime, povratniTip);
			
			this.tipoviParametara.addAll(tipoviParametara);
		}
		
		public DeklaracijaFunkcije(String ime, Tipovi povratniTip) {
			this.ime = ime;
			this.povratniTip = povratniTip;
			definiranaFunkcija = false;
		}

		public String getIme() {
			return ime;
		}

		public Tipovi getPovratniTip() {
			return povratniTip;
		}

		public Set<Tipovi> getTipoviParametara() {
			return tipoviParametara;
		}

		/**
		 * This method is used in productions when it is determined that two function declaration names match. Therefore
		 * also their return and parameter types must match, otherwise there's a semantic error!
		 * 
		 * @param other The deklaracija that is being compared to
		 * @return true or false
		 */
		public boolean compareReturnAndParamsType(DeklaracijaFunkcije other) {
			if(!other.povratniTip.equals(this.povratniTip)) {
				return false;
			}
			
			if(!other.tipoviParametara.equals(this.tipoviParametara)) {
				return false;
			}
			
			return true;
		}
		
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + (definiranaFunkcija ? 1231 : 1237);
			result = prime * result + ((ime == null) ? 0 : ime.hashCode());
			result = prime * result + ((povratniTip == null) ? 0 : povratniTip.hashCode());
			result = prime * result + ((tipoviParametara == null) ? 0 : tipoviParametara.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			DeklaracijaFunkcije other = (DeklaracijaFunkcije) obj;
			if (definiranaFunkcija != other.definiranaFunkcija)
				return false;
			if (ime == null) {
				if (other.ime != null)
					return false;
			} else if (!ime.equals(other.ime))
				return false;
			if (povratniTip == null) {
				if (other.povratniTip != null)
					return false;
			} else if (!povratniTip.equals(other.povratniTip))
				return false;
			if (tipoviParametara == null) {
				if (other.tipoviParametara != null)
					return false;
			} else if (!tipoviParametara.equals(other.tipoviParametara))
				return false;
			return true;
		}

	}