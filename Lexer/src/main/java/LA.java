
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * A lot of code from the mainFormer method. This class shall analyse the input program code being submitted to it.
 * 
 * @author kikyy99
 *
 */
public class LA {
	
	private static String currentState;
	private static List<Pravilo> poredanaPravila = null;
	private static int br_redaka = 1;
	private static List<Pravilo> currRules = new ArrayList<>();
	private static int pocetak = 0, zavrsetak = 0, posljednji = -1;
	private static StringBuilder izlaznaTablica = new StringBuilder();
	private static boolean vratiSe = false;
	private static char[] programCharArray;
	
	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws IOException, InterruptedException {
		String pocetnoStanje = "";
		Map<String,Enfa> regexi = new HashMap<>();		// regex - enfa pair
		
		try {
			FileInputStream fis1 = new FileInputStream("./analizator/listaZaAnalizator.ser");
			ObjectInputStream ois1 = new ObjectInputStream(fis1);
		
			poredanaPravila = (ArrayList<Pravilo>) ois1.readObject();

			ois1.close();
			fis1.close();
			
			fis1 = new FileInputStream("./analizator/pocetnoStanjeZaAnalizator.ser");
			ois1 = new ObjectInputStream(fis1);
			
			pocetnoStanje = (String) ois1.readObject();
			
			ois1.close();
			fis1.close();
			
			fis1 = new FileInputStream("./analizator/mapRegexEnfa.ser");
			ois1 = new ObjectInputStream(fis1);
			
			regexi = (Map<String, Enfa>) ois1.readObject();
			
			ois1.close();
			fis1.close();
			
//			fis1 = new FileInputStream("./mapStateRegex.ser");
//			ois1 = new ObjectInputStream(fis1);
//			
//			regexiPoStanjima = (Map<String, String>) ois1.readObject();
//			
//			ois1.close();
//			fis1.close();
		} catch (FileNotFoundException e) {
			System.out.println("File not found");
		} catch (IOException e) {
			System.out.println("Error initializing stream");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
//		System.out.println(pocetnoStanje);
//		System.out.println("test");
//		for(Pravilo pravilo : poredanaPravila) {
//			System.out.println(pravilo.getStanje());
//			System.out.println(pravilo.getRegularniIzraz());
//			System.out.println(pravilo.getListaNaredaba());
//		}
		
		String pom;
		StringBuilder program = new StringBuilder();
		try(BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
			while((pom = reader.readLine()) != null) {
				program.append(pom);
				program.append('\n');
			}
		}
		
		// Necessary data structures for program parsing
		boolean[] acceptable;
		
		// Let's parse the String Program
		String programString = program.toString();
		programCharArray = programString.toCharArray();
		currentState = pocetnoStanje;
		
		StringBuilder currString = new StringBuilder();
		//adding all the current states
		currRules = pravilaCurrStanja();
		
		// reading the input program
		for(int i = 0; i < programCharArray.length; i++) {
			zavrsetak = i;		// zavrsetak is current index
			
			char currChar = programCharArray[zavrsetak];
			currString.append(currChar);
			
			boolean sviEnfaPrazni = true;
			
			for(Pravilo rule : currRules) {
				String regex = rule.getRegularniIzraz();
				Enfa currEnfa = regexi.get(regex);
				
				acceptable = RegexEnfa.testRegexOnEnfa2(currEnfa, currString.toString());
				
				if(acceptable[0]) {
					posljednji = i;
				}
				
				if(!acceptable[1])
					sviEnfaPrazni = false;		
			}
			
			// Error recovery
			/*
			 * Need to somehow reset the posljednji var. There are 2 ways to solve
			 * this:	a) A boolean error recovery flag
			 * 			b) Just set posljednji equal to pocetak
			 * 
			 * Will use the b) method as it seems easier, and logical enough
			 * to work -- and as long as it works, it's not stupid
			 * 
			 * Aka just need to propagate the posljednji var - keep them equal
			 */
			if(!sviEnfaPrazni) {
				// Continue on - yet need to see how will the situation develop
				continue;
			}
			
			if(posljednji >= pocetak) {
				// Need to group [Pocetak, Posljednji]
				
				// Need to return to the last acceptable String
				currString.delete(posljednji - pocetak + 1, currString.length());
				
				for(Pravilo rule : currRules) {
					String regex = rule.getRegularniIzraz();
					Enfa currEnfa = regexi.get(regex);
					if(RegexEnfa.testRegexOnEnfa(currEnfa,currString.toString())) {
						doAkcija(rule, currString);
						currString.delete(0, currString.length());
						
						if(vratiSe) {
							vratiSe = false;
							i = pocetak - 1; // i shall be incremented by the loop
						} else {
							pocetak = posljednji + 1;
							i = posljednji;		// This is done to counter for incrementing
						}
						
						break;
					}
				}
			} else {
				// posljednji < pocetak
				// ALl the enfas are inn empty states, and we can't fall back to an accepted prefix
				// Error recovery
				currString.delete(0, currString.length());
				System.err.println(programCharArray[zavrsetak]);
				posljednji = pocetak;
				pocetak++;
				i = posljednji;			// i shall be incremented by the loop
			}
		}
		
		System.out.print(izlaznaTablica);
	}
	
	public static void doAkcija(Pravilo pravilo, StringBuilder currString) {
		/*
		 * Vrati_se needs to be done first! Need to scout ahead for it, and do it first.
		 * Ooor, in the generator put them in different order :D
		 */
		for(String akcija : pravilo.getListaNaredaba()) {
			switch(akcija) {
				case "-": 
					continue;
				case "NOVI_REDAK":
					br_redaka++;
					continue;
			}
			
			if(akcija.length() >= 15 && akcija.substring(0,13).equals("UDJI_U_STANJE")) {
				currentState = akcija.substring(14);
				currRules = pravilaCurrStanja();
			} else if(akcija.length() >= 10 && akcija.substring(0,8).equals("VRATI_SE")) {
				int indeks = Integer.valueOf(akcija.substring(9));
				currString.delete(indeks, currString.length());
				posljednji = pocetak + indeks - 1;
				pocetak = posljednji + 1;
				vratiSe = true;
			} else { 
				izlaznaTablica.append(akcija)
					.append(' ')
					.append(br_redaka)
					.append(' ')
					.append(currString)
					.append(System.lineSeparator());
			}
		}
	}
	
	public static List<Pravilo> pravilaCurrStanja() {
		List<Pravilo> currRules = new ArrayList<>();
		
		for(Pravilo currRule : poredanaPravila) {
			String ruleState = currRule.getStanje();
			
			if(ruleState.equals(currentState)) {
				currRules.add(currRule);
			}
		}
		
		return currRules;
	}
	
	/**
	 * Checks if there are any active States in the enfas.
	 * 
	 * @return true if there are any states active. False if all the enfas are in
	 * empty states
	 */
	public static boolean anyStates(List<Enfa> enfas) {
		for(Enfa currEnfa : enfas) {
			Set<Enfa.Automaton.State> activeStates = currEnfa.getActiveStates();
			
			Enfa.Automaton.State currEnfaEmptyState = currEnfa.getEmptyState();
			boolean isEmpty = activeStates.size() == 1 && activeStates.contains(currEnfaEmptyState);
			if(!isEmpty) {
				return true;
			}
		}
		
		return false;
	}
}