import java.util.ArrayList;
import java.util.List;

public class Regex {
	
	int oznakaTrenutnogStanja = 0;	
	
	public int novo_stanje(Enfa automat) {
		automat.addState("" + oznakaTrenutnogStanja++, false);
		return oznakaTrenutnogStanja-1;
	}
	
	public static boolean je_operator(String izraz, int i) {
		int br = 0;
		while(i-1 >= 0 && izraz.charAt(i-1) == '\\') {
			br++;
			i--;
		}
		return br%2 == 0;
	}

	ParStanja pretvori(String izraz, Enfa automat) {
		return pretvori(izraz, automat, 0);
	}
	
	ParStanja pretvori(String izraz, Enfa automat, int stateEnumerationBeginIndex) {
		List<String> izbori = new ArrayList<>();
		oznakaTrenutnogStanja = stateEnumerationBeginIndex;
		int br_zagrada = 0;
		int zadnji = -1;		// Ukljucivo zadnji grupiran znak
		int brOperatora = 0;
		
		for (int i = 0; i < izraz.length(); i++) {
			if(izraz.charAt(i) == '(' && je_operator(izraz, i)) {
				br_zagrada++;
			} else if(izraz.charAt(i) == ')' && je_operator(izraz, i)) {
				br_zagrada--;
			} else if(br_zagrada == 0 && izraz.charAt(i) == '|' && je_operator(izraz, i)) {
				izbori.add(izraz.substring(zadnji+1, i));
				zadnji = i;
				brOperatora++;
			}
		}
		
		izbori.add(izraz.substring(zadnji+1, izraz.length()));

		int lijevo_stanje = novo_stanje(automat);
		int desno_stanje = novo_stanje(automat);
		
		if(brOperatora > 0) {
			// This part handles the multiple(logical or) possibilities
			
			for(int i = 0; i < izbori.size(); i++) {
				ParStanja privremeno = pretvori(izbori.get(i), automat, oznakaTrenutnogStanja);
		        automat.addEpsilonTransition("" + lijevo_stanje, "" + privremeno.lijevo_stanje);
		        automat.addEpsilonTransition("" + privremeno.desno_stanje, "" + desno_stanje);
			}
		} else {
			// This is what happens when there are no more | in the regex
			boolean prefiksirano = false;
			int zadnje_stanje = lijevo_stanje;
			
			for(int i = 0; i < izraz.length(); i++) {
				int a = 0, b = 0;
				if(prefiksirano == true) {
					prefiksirano = false;
					char prijelazni_znak;
					
					if(izraz.charAt(i) == 't')
						prijelazni_znak = '\t';
					else if(izraz.charAt(i) == 'n')
						prijelazni_znak = '\n';
					else if(izraz.charAt(i) == '_')
						prijelazni_znak = ' ';
					else 
						prijelazni_znak = izraz.charAt(i);
					
					a = novo_stanje(automat);
					b = novo_stanje(automat);
					automat.addTransition(""+a, ""+prijelazni_znak, ""+b);
				} else {
					if(izraz.charAt(i) == '\\') {
						prefiksirano = true;
						continue;
					}
					
					if(izraz.charAt(i) != '(') {
						a = novo_stanje(automat);
						b = novo_stanje(automat);
						
						if(izraz.charAt(i) == '$') {			// This makes sense to me
							automat.addEpsilonTransition(""+a,""+b);
						} else {
							// First need to add this alphabet character
							String alphChar = String.valueOf(izraz.charAt(i));
							automat.addAlphabetChar(alphChar);
							automat.addTransition(""+a, alphChar, ""+b);
						}
					} else {
						int j = pronadiDesnuZagradu(izraz, i);
						ParStanja privremeno = pretvori(izraz.substring(i+1, j), automat, oznakaTrenutnogStanja);
						a = privremeno.lijevo_stanje;
						b = privremeno.desno_stanje;
						i = j;
					}
				}
				
				//provjera ponavljanja
				if(i+1 < izraz.length() && izraz.charAt(i+1) == '*') {
					int x = a;
					int y = b;
					a = novo_stanje(automat);
					b = novo_stanje(automat);
					automat.addEpsilonTransition(""+a,""+x);
					automat.addEpsilonTransition(""+y,""+b);
					automat.addEpsilonTransition(""+a,""+b);
					automat.addEpsilonTransition(""+y,""+x);
					i++;
				}
				
				//povezivanje s ostatkom automata
				automat.addEpsilonTransition(""+zadnje_stanje, ""+a);
				zadnje_stanje = b;
			}
			automat.addEpsilonTransition(""+zadnje_stanje, ""+desno_stanje);
		}
		
		return new ParStanja(lijevo_stanje, desno_stanje);
	}

	public static int pronadiDesnuZagradu(String izraz, int pocetniIndex) {
		int brZagrada = 0;
		int trenutni = pocetniIndex+1;
		
		while(!(brZagrada == 0 && izraz.charAt(trenutni) == ')')){
			if(izraz.charAt(trenutni) == '(')
				brZagrada++;
			if(izraz.charAt(trenutni) == ')')
				brZagrada--;
			
			trenutni++;
			if(trenutni >= izraz.length())
				break;
		}
		return trenutni;
	}
}
