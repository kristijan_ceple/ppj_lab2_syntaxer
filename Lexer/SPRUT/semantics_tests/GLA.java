import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class GLA {

	public static void main(String[] args) {
		Scanner stringC = new Scanner(System.in);
		String linija = stringC.nextLine();
		HashMap<String,String> mapaRegova = new HashMap<>();
		String[] splitanelinije;
		String imeRega;
		String keyStringic;
		char[] polje;
		
		while(!linija.contains("%X")) {
			
			splitanelinije=linija.split(" ");
			imeRega=splitanelinije[0].substring(1, splitanelinije[0].length()-1);
			polje=splitanelinije[1].toCharArray();
			StringBuilder stringB = new StringBuilder();
			
			for(int i=0;i<polje.length;++i) {
		
				if(polje[i]=='\\') {
					stringB.append(polje[i]);
					stringB.append(polje[++i]);
				}
				
				else if(polje[i]=='{') {
					i++;
					StringBuilder stringD = new StringBuilder();
					while(polje[i]!='}') {
						stringD.append(polje[i]);
						++i;
					}
					keyStringic=stringD.toString();
					stringB.append("("+mapaRegova.get(keyStringic)+")");
				} else {
					stringB.append(polje[i]);
				}
				
			}
			
			mapaRegova.put(imeRega, stringB.toString());
			linija=stringC.nextLine();
		}
		
		linija=linija.substring(3,linija.length());
		String[] stanja = linija.split(" ");
		
		linija=stringC.nextLine();
		linija=linija.substring(3,linija.length());
		String[] leksickeJedinke = linija.split(" ");
		String pocetnoStanje = stanja[0];
		
		List<Pravilo> listaPravila = new ArrayList<>();
		
		while(stringC.hasNextLine()) {
			List<String> listaNaredaba = new ArrayList<>();
			
			linija = stringC.nextLine(); 
			linija = linija.substring(1);
			
			String[] stanjeiNiz = linija.split(">",2);
			
			polje=stanjeiNiz[1].toCharArray();
			StringBuilder stringB = new StringBuilder();
			
			for(int i=0;i<polje.length;++i) {
		
				if(polje[i]=='\\') {
					stringB.append(polje[i]);
					stringB.append(polje[++i]);
				}
				
				else if(polje[i]=='{') {
					i++;
					StringBuilder stringD = new StringBuilder();
					while(polje[i]!='}') {
						stringD.append(polje[i]);
						++i;
					}
					keyStringic=stringD.toString();
					stringB.append("("+mapaRegova.get(keyStringic)+")");
				} else {
					stringB.append(polje[i]);
				}
				
			}
			
			stanjeiNiz[1]=stringB.toString();
			
			linija=stringC.nextLine();
			while(!linija.startsWith("}")) {
				if(!linija.equals("{")) {
					listaNaredaba.add(linija);
				}
				
				if(stringC.hasNextLine()) {
					linija=stringC.nextLine();
				}
			}
			
			// Let's reorganize lista naredaba
			List<String> listaNaredabaSorted = new ArrayList<>();
			boolean containsVratiSe = false;
			int indexVratiSe = 0;			// The index of the VRATI_SE -- if found
			for(int i = 0; i < listaNaredaba.size(); i++) {
				String naredba = listaNaredaba.get(i);
				if(naredba.contains("VRATI_SE")) {
					indexVratiSe = i;
					containsVratiSe = true;
					break;
				}
			}
			
			if(containsVratiSe) {
				listaNaredabaSorted.add(listaNaredaba.get(indexVratiSe));
				
				for(int i = 0; i < listaNaredaba.size(); i++) {
					if(i == indexVratiSe) {
						continue;
					}
					
					listaNaredabaSorted.add(listaNaredaba.get(i));
				}
				
				listaNaredaba = listaNaredabaSorted;
			}
			
			Pravilo p = new Pravilo(stanjeiNiz[0], stanjeiNiz[1], listaNaredaba);
			listaPravila.add(p);
			
		}
		stringC.close();
		
		Map<String,Enfa> regexi = new HashMap<>();		// regex - enfa pair
		for(Pravilo prijelaz : listaPravila) {
			String regex = prijelaz.getRegularniIzraz();
			Enfa enfa = null;
	
			if(!regexi.containsKey(regex)) {
				enfa = ConversionAlgo.constructEnfa(regex);
				regexi.put(regex, enfa);
			}
			
		}
		
		//spremanje objekata za analizator hihihi
		// Need to serialize: String beginningState, Rules List and Enfas
			
		try {
			FileOutputStream fos1 = new FileOutputStream("./analizator/listaZaAnalizator.ser");
			ObjectOutputStream oos1 = new ObjectOutputStream(fos1);
			
			oos1.writeObject(listaPravila);
			
			oos1.close();
			fos1.close();
		
			fos1 = new FileOutputStream("./analizator/pocetnoStanjeZaAnalizator.ser");
			oos1 = new ObjectOutputStream(fos1);
			
			oos1.writeObject(pocetnoStanje);
			
			oos1.close();
			fos1.close();
			
			fos1 = new FileOutputStream("./analizator/mapRegexEnfa.ser");
			oos1 = new ObjectOutputStream(fos1);
			
			oos1.writeObject(regexi);
			
			oos1.close();
			fos1.close();
			
			// Used for regexiPoStanjima -- currently unnecessary
	//		fos1 = new FileOutputStream("./mapStateRegex.ser");
	//		oos1 = new ObjectOutputStream(fos1);
	//		
	//		oos1.writeObject(regexiPoStanjima);
	//		
	//		oos1.close();
	//		fos1.close();
			
		} catch (FileNotFoundException e) {
			System.out.println("File not found");
		} catch (IOException e) {
			System.out.println("Error initializing stream");
			System.err.println(e.toString());
		}
	}
	
//	/**
//	 * Prints all the states and their transitions of the automatons.
//	 * Prints the starting state and the acceptable states.
//	 * 
//	 * The format is CSV(comma-seperated values) String
//	 * 
//	 * 1. Row - States
//	 * 2. Row - Transitions
//	 * 3. Row - Acceptable States
//	 * 4. Row - Starting State
//	 * 
//	 * @param enfa the enfa whose table shall be printed out
//	 * @return a String containing the table of the enfa
//	 */
//	public static String getEnfaTable(Enfa enfa) {
//		StringBuilder toPrint = new StringBuilder();
//		
//		// First the States
//		Set<State> states = enfa.getStates();
//		for(State curr : states) {
//			toPrint.append(curr.toString());
//		}
//		toPrint.append(System.lineSeparator());		// OS independant line separator
//		
//		// Now all of their Transitions
//		for(State currState : states) {
//			for(Transition currTrans : currState.getTransitions()) {
//				
//			}
//		}
//		
//		return toPrint.toString();
//	}
}
