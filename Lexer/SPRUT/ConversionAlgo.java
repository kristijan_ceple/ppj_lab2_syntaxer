import java.util.Objects;
import java.util.Set;

/**
 * A class that takes multiple regexes and constructs a single Enfa that accepts
 * them all.
 * 
 * @author kikyy99
 *
 */
public class ConversionAlgo {
	public static final String automataFilePath = "./src/main/resources/enfa.data";
	public static String placeHolder;
	public static Set<String> regexSet;
	
	/**
	 * Constructs an enfa from a regex
	 * 
	 * @param regex
	 * @return an Enfa instance
	 */
	public static Enfa constructEnfa(String regex) {
		return constructEnfa(regex, 0);
	}
	
	/**
	 * Constructs an Enfa from a regex - features the beginStateIndex bonus counter parameter.
	 * 
	 * @param regex
	 * @param beginStateIndex The index which State enumeration inside the Enfa will begin from
	 * @return an Enfa instance
	 */
	public static Enfa constructEnfa(String regex, int stateEnumerationBeginIndex) {
		Enfa enfa = new Enfa();
		
		/*
		 * Need to initialise the Enfa with the alphabet and the input. The
		 * Regex method pretvori shall do that
		 */
		
		Regex regexConvObject = new Regex();
		ParStanja IOStates = regexConvObject.pretvori(regex, enfa, stateEnumerationBeginIndex);
		
		// ParStanja.Left = starting State, and ParStanja.right = acceptable State
		enfa.setStartingState(String.valueOf(IOStates.getLijevo_stanje()));
		enfa.setAcceptedState(String.valueOf(IOStates.getDesno_stanje()));
		
		return enfa;
	}
	
	/**
	 * Combines multiple Enfas into one. This is done by constructing another Enfa with only one state,
	 * and then form that Enfa epsilon transitions are made to the other Enfas.
	 * 
	 * @param first the first Enfa
	 * @param second the second Enfa
	 * @param others eventually the other Enfas
	 * @return a combined Enfa
	 */
	public static Enfa unifyEnfas(Enfa first, Enfa second, Enfa... others) {
		Objects.requireNonNull(first);
		Objects.requireNonNull(second);
		
		Enfa start = new Enfa();
		start.addState("p1", false);
		start.setStartingState("p1");
		start.connectEnfas(first);
		start.connectEnfas(second);
		
		if(others != null) {
			for(Enfa curr : others) {
				start.connectEnfas(curr);
			}
		}
		
		return start;
	}
	
	public static Enfa unifyRegexes(String first, String second, String... others) {
		Objects.requireNonNull(first);
		Objects.requireNonNull(second);

		Enfa start = new Enfa();
		start.addState("p1", false);
		start.setStartingState("p1");
		
		// During construction need to watch out for State names to differ
		
		int counter = 0;
		Enfa firstEnfa = ConversionAlgo.constructEnfa(first, counter);
		counter = firstEnfa.getLastStateNumber();		// Must take into consideration State enumeration
		Enfa secondEnfa = ConversionAlgo.constructEnfa(second, ++counter);
		
		start.connectEnfas(firstEnfa);
		start.connectEnfas(secondEnfa);
		
		if(others != null) {
			counter = secondEnfa.getLastStateNumber();
			for(String curr : others) {
				Enfa currEnfa = ConversionAlgo.constructEnfa(curr, ++counter);
				start.connectEnfas(currEnfa);
				counter = currEnfa.getLastStateNumber();
			}
		}
		
		return start;
	}
	
	/**
	 * Reads the automata table definition from a file and returns it
	 * as one massive string.
	 * 
	 * @return massive automata table String
	 */
	public static String getAutomataTable() {
		
		
		return "placeholder";
	}
}
