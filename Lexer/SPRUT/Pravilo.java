import java.util.ArrayList;
import java.util.List;

public class Pravilo implements java.io.Serializable {
	
	/**
	 * Used for serialisation
	 */
	private static final long serialVersionUID = 6809283262305029269L;
	private String stanje;
	private String regularniIzraz;
	private List<String> listaNaredaba;
	
	public Pravilo(String stanje, String regularniIzraz, List<String> listaNaredaba) {
		this.stanje = stanje;
		this.regularniIzraz = regularniIzraz;
		this.listaNaredaba = listaNaredaba;
	}
	
	public String getStanje() {
		return stanje;
	}
	public String getRegularniIzraz() {
		return regularniIzraz;
	}
	public List<String> getListaNaredaba() {
		return listaNaredaba;
	}
	
	
}