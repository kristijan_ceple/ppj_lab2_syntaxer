

public class JedinkaUlaza {
	
	private String znak;
	private String redak;
	private String niz;
	
	public JedinkaUlaza(String znak, String redak, String niz) {
		super();
		this.znak = znak;
		this.redak = redak;
		this.niz = niz;
	}
	public String getZnak() {
		return znak;
	}
	public String getRedak() {
		return redak;
	}
	public String getNiz() {
		return niz;
	}
	public void setZnak(String znak) {
		this.znak = znak;
	}
	@Override
	public String toString() {
		return "znak=" + znak + ", redak=" + redak + ", niz=" + niz;
	}

}
