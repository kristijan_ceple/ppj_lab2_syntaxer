

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Prs implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2170131196137248450L;
	private Akcija vrsta;
	private int indeks;
	private Znak lijeviZnak;
	private List<Znak> produkcija;
	private int indeksProdukcije;
	private Set<Znak> curlyBracketChars = new HashSet<>();
	
	public Prs(Akcija vrsta) {
		
		this.vrsta = vrsta;
	
	}
	
	public Prs(Akcija vrsta ,int indeks) {
	
		this.vrsta = vrsta;
		this.indeks = indeks;
		
	}
	
	public Prs(Akcija vrsta ,Znak lijeviZnak ,List<Znak> produkcija, Set<Znak> curlyBracketChars) {
		this.vrsta = vrsta;
		this.lijeviZnak = lijeviZnak;
		this.produkcija = produkcija;
		this.curlyBracketChars.addAll(curlyBracketChars);
	}
	
	public Prs(Akcija vrsta ,Znak lijeviZnak ,List<Znak> produkcija, int indeks, Set<Znak> curlyBracketChars) {
		this(vrsta, lijeviZnak, produkcija, curlyBracketChars);
		this.indeksProdukcije = indeks;
		
	}
	
	public Akcija getVrsta() {
		return vrsta;
	}
	
	public int getIndeksProdukcije() {
		return indeksProdukcije;
	}

	public void setIndeksProdukcije(int indeksProdukcije) {
		this.indeksProdukcije = indeksProdukcije;
	}

	public Integer getIndeks() {
		if(vrsta == Akcija.STAVI || vrsta == Akcija.POMAKNI)
			return indeks;
		else return (Integer) null;
	}

	public Znak getLijeviZnak() {
		if(vrsta == Akcija.REDUCIRAJ || vrsta == Akcija.PRIHVATI)
			return lijeviZnak;
		else return null;
	}

	public List<Znak> getProdukcija() {
		if(vrsta == Akcija.REDUCIRAJ || vrsta == Akcija.PRIHVATI)
			return produkcija;
		else return null;
	}

	public String getProdukcijaString() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.lijeviZnak).append(" -> ");
		
		for(int i = 0; i < this.produkcija.size(); i++) {
			sb.append(" ").append(this.produkcija.get(i).toString());
		}
				
		sb.append(" *");
		
		sb.append(" {");
		for(Znak znak : curlyBracketChars) {
			sb.append(" ").append(znak.toString());
		}
		sb.append(" }");
		
		return sb.toString();
	}
		
	@Override
	public String toString() {
		return "Prs [vrsta=" + vrsta + ", indeks=" + indeks + ", lijeviZnak=" + lijeviZnak + ", produkcija="
				+ produkcija + "]";
	}	
	
	
}
