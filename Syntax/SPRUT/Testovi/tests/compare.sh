#!/bin/bash

dir_array=("00aab_1" "01aab_2" "02aab_3" "03gram100_1" "04gram100_2" "05gram100_3" "06oporavak1" "07oporavak2" "08pomred" "09redred" "10minusLang_1" "11minusLang_2" "12ppjC" "13ppjLang" "14simplePpjLang" "15kanon_gramatika" "16minusLang" "17simplePpjLang_manji" "18simplePpjLang_err" "19simplePpjLang_veci" "20simplePpjLang_najmanji")

for dir in ${dir_array[*]}
do
	echo "Currently testing $dir"

	res=$(diff $dir/output.out $dir/test.out)
	if [ -z "$res" ]
	then
		printf "$dir Test successful\n\n"
	else
		printf "$dir Test failed\n\n"
		printf "############################ FAIL ################################\n\n"
	fi
done
