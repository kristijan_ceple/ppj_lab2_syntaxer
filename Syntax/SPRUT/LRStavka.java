

import java.util.List;
import java.util.Set;

public class LRStavka {
	
	Znak lijevaStrana;
	List<Znak> desnaStrana;
	Set<Znak> znakoviUViticama;
	int indexTocke;
	boolean oznacen;
	int ulaznaDatotekaIndex;
	
	public LRStavka(String lijevaStrana, List<Znak> desnaStrana, Set<Znak> znakoviUViticama) {

		this.lijevaStrana = new Znak(lijevaStrana, true);
		this.desnaStrana = desnaStrana;
		this.indexTocke = 0;
		this.znakoviUViticama = znakoviUViticama;
		this.oznacen = false;
	}

	public LRStavka(String lijevaStrana, List<Znak> desnaStrana, Set<Znak> znakoviUViticama, int ulaznaDatotekaIndex) {
		this(lijevaStrana, desnaStrana, znakoviUViticama);
		this.setUlaznaDatotekaIndex(ulaznaDatotekaIndex);
	}
	
	public int getIndexTocke() {
		return indexTocke;
	}

	public void setIndexTocke(int indexTocke) {
		this.indexTocke = indexTocke;
	}

	public Znak getLijevaStrana() {
		return lijevaStrana;
	}

	public void setLijevaStrana(Znak lijevaStrana) {
		this.lijevaStrana = lijevaStrana;
	}

	public List<Znak> getDesnaStrana() {
		return desnaStrana;
	}

	public void setDesnaStrana(List<Znak> desnaStrana) {
		this.desnaStrana = desnaStrana;
	}

	public Set<Znak> getZnakoviUViticama() {
		return znakoviUViticama;
	}

	public void setZnakoviUViticama(Set<Znak> znakoviUViticama) {
		this.znakoviUViticama = znakoviUViticama;
	}
	
	public int getUlaznaDatotekaIndex() {
		return ulaznaDatotekaIndex;
	}

	public void setUlaznaDatotekaIndex(int ulaznaDatotekaIndex) {
		this.ulaznaDatotekaIndex = ulaznaDatotekaIndex;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.lijevaStrana)
		.append(" -> ");
		
		// Lets append until we get to the star - so [0 to index>
		for(int i = 0; i < this.indexTocke; i++) {
			sb.append(" ").append(this.desnaStrana.get(i).toString());
		}
		
		sb.append(" *");
		
		for(int i = indexTocke; i < this.desnaStrana.size(); i++) {
			sb.append(" ").append(this.desnaStrana.get(i).toString());
		}
		
		sb.append(" {");
		for(Znak znak : znakoviUViticama) {
			sb.append(" ").append(znak.toString());
		}
		sb.append(" }");
		
		return sb.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((desnaStrana == null) ? 0 : desnaStrana.hashCode());
		result = prime * result + indexTocke;
		result = prime * result + ((lijevaStrana == null) ? 0 : lijevaStrana.hashCode());
		result = prime * result + ((znakoviUViticama == null) ? 0 : znakoviUViticama.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LRStavka other = (LRStavka) obj;
		if (desnaStrana == null) {
			if (other.desnaStrana != null)
				return false;
		} else if (!desnaStrana.equals(other.desnaStrana))
			return false;
		if (indexTocke != other.indexTocke)
			return false;
		if (lijevaStrana == null) {
			if (other.lijevaStrana != null)
				return false;
		} else if (!lijevaStrana.equals(other.lijevaStrana))
			return false;
		if (znakoviUViticama == null) {
			if (other.znakoviUViticama != null)
				return false;
		} else if (!znakoviUViticama.equals(other.znakoviUViticama))
			return false;
		return true;
	}
	
	
	

}
