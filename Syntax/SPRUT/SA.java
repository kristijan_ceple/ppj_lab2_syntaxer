import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class SA {
	static TransferTablica tablica = null;
	static ArrayList<JedinkaUlaza> listaUlaza = new ArrayList<JedinkaUlaza>();
	static ArrayList<Cvor<Znak>> stog = new ArrayList<Cvor<Znak>>();
	static ArrayList<Cvor<JedinkaUlaza>> tree = new ArrayList<Cvor<JedinkaUlaza>>();
	public static int treeIndex = -1;
		

	public static void main(String[] args) {
		
		Scanner inputScan = new Scanner(System.in);
		boolean nijePrihvaceno=true;
		Znak znak;
		Znak znakIndeks;
		Prs akcija;
		String[] znakRedakNiz;
		boolean jeliDosloDoPogreske = false;
		int indexListe = 0;
		int duljinaProdukcije;
		int treeDuljinaProdukcije;
		int indeks = 0;
		Cvor<JedinkaUlaza> glava=null;
		
		List<Cvor<JedinkaUlaza>> listaDjece;
		
		try {
			FileInputStream fi = new FileInputStream(new File("objekt.ser"));
			ObjectInputStream oi = new ObjectInputStream(fi);

			tablica = (TransferTablica)oi.readObject();

			oi.close();
			fi.close();
		} catch (ClassNotFoundException e) {
			System.out.println("Class not found");
		} catch (FileNotFoundException e) {
			System.out.println("File not found");
		} catch (IOException e) {
			System.out.println("Error initializing stream");
		}
		
		znak = new Znak("0",false);
		znak.setJeliBroj(true);
		stog.add(new Cvor<Znak>(null, znak));
		
		while(inputScan.hasNextLine()) {
			
			znakRedakNiz = inputScan.nextLine().split(" ",3);
//			for(int i = 2; i < znakRedakNiz[2].length
			listaUlaza.add(new JedinkaUlaza(znakRedakNiz[0], znakRedakNiz[1], znakRedakNiz[2]));
			
		}
		JedinkaUlaza ulaz = null;
		for (int i = 0; nijePrihvaceno; i++) {
			if(jeliDosloDoPogreske) {
				jeliDosloDoPogreske = false;
				i--;
			}
			if(indeks > 0) {
				indeks--;
				continue;
			}
			if(i >= listaUlaza.size())
				ulaz.setZnak("KN");
			else
				ulaz = listaUlaza.get(i);
			indexListe = stog.size()- 1;
			znakIndeks = stog.get(indexListe).getVrijednost();
			znak = new Znak(ulaz.getZnak(),false);
			
			akcija = tablica.getAkcija(Integer.parseInt(znakIndeks.getImeZnaka()), new Znak(ulaz.getZnak(),false));
			
			if(akcija == null) {
				jeliDosloDoPogreske = true;
				indeks = oporavak(ulaz, i);
				continue;
			} else if(akcija.getVrsta() == Akcija.POMAKNI) {
				
				stog.add(new Cvor<Znak>(null ,znak));
				// Need to add this to the tree as well - this is done only for terminals. Therefore pass ulaz in!
				tree.add(new Cvor<>(new JedinkaUlaza(ulaz.getZnak(), ulaz.getRedak(), ulaz.getNiz())));
				znak = new Znak (akcija.getIndeks().toString() ,false);
				znak.setJeliBroj(true);
				
				stog.add(new Cvor<Znak>(null ,znak));
				
				indexListe+=2;
				treeIndex++;
				
			} else if(akcija.getVrsta() == Akcija.REDUCIRAJ || akcija.getVrsta() == Akcija.PRIHVATI) {
				//Racunanje koliko moram brisati sa stoga
				duljinaProdukcije = akcija.getProdukcija().size();
				treeDuljinaProdukcije = duljinaProdukcije;
				duljinaProdukcije*=2;
				
				listaDjece = new ArrayList<Cvor<JedinkaUlaza>>();
				List<Cvor<JedinkaUlaza>> tmp = new ArrayList<Cvor<JedinkaUlaza>>();
//				// Need to group all the znaks under the left-side non-terminal
//				for(Znak znakic : akcija.getProdukcija()) {
//					listaDjece.add(new Cvor<>(znakic));
//				}
				
				for(int i1 = 0; i1<duljinaProdukcije ;++i1) {
					stog.remove(indexListe-i1);
				}
				indexListe-=duljinaProdukcije;
				
				for(int i1 = 0; i1 < treeDuljinaProdukcije; i1++) {
					tmp.add(tree.get(treeIndex-i1));
					
					tree.remove(treeIndex-i1);
				}
				
				if(tmp.size()>0) {
					// Need to reverse listaDjece
					for(int i1 = tmp.size()-1; i1 >= 0; i1--) {
						listaDjece.add(tmp.get(i1));
					}
				}
				
				treeIndex-=treeDuljinaProdukcije;
				
				// If epsilon reduction, then just add dollar
				if(akcija.getProdukcija().size()==0) {
					listaDjece.add(new Cvor<>(new JedinkaUlaza("$", null, null)));
				}
				
				// Now create parent root/non-terminal, and add its children
				Cvor<JedinkaUlaza> ovajCvor = new Cvor<JedinkaUlaza>(listaDjece, new JedinkaUlaza(akcija.getLijeviZnak().toString(), null, null));
				
				if(akcija.getVrsta() == Akcija.PRIHVATI) {
					glava = ovajCvor;
					System.out.print(glava.obidjiPodstablo(0));
					nijePrihvaceno = false;
				} else if(akcija.getVrsta() == Akcija.REDUCIRAJ) {
					// Need to add ovajCvor to stog and tree
					stog.add(new Cvor<Znak>(new Znak(akcija.getLijeviZnak().toString(), true)));
					tree.add(ovajCvor);
					treeIndex++;
					
					akcija = tablica.getAkcija(Integer.parseInt(stog.get(indexListe).getVrijednost()
							.getImeZnaka()), akcija.getLijeviZnak());
					
					znak = new Znak (akcija.getIndeks().toString() ,false);
					znak.setJeliBroj(true);
					
					stog.add(new Cvor<Znak>(null, znak));
					indexListe+=2;
				}
				
				// If we've just reduced, we need to read again
				i = i-1;
//				if(glava == null) {
//					glava = ovajCvor;
//				} else if(listaDjece.contains(glava)) 
//					glava = ovajCvor;
				
			}
		}
		inputScan.close();
	}
	
	public static int oporavak(JedinkaUlaza ulaz, int indeks) {
		int i, j = 0, kolikoIzbrisati = 0;
		boolean gotovOporavak = false;
		
		System.err.println("Broj retka gdje se pogreska dogodila: "+ ulaz.getRedak());
//		Set<Znak> dobriZnakovi = new HashSet<>();
		Integer stateNum = Integer.parseInt(stog.get(stog.size() - 1).getVrijednost().toString());
		Map<Znak ,Prs> redak = tablica.getGlavnaMapa().get(stateNum);
//		dobriZnakovi = tablica.getSinkroZnakovi();
		Set<Znak> dobriZnakovi = redak.keySet();
		System.err.println("Ocekivani uniformni znakovi:");
		for(Znak znak : dobriZnakovi) {
			// Now check if table entry not null
			if(tablica.getAkcija(stateNum, znak) != null) {
				System.err.println("\t" + znak + " ");
			}
		}
//		System.out.println();
		System.err.println("Procitani uniformni znakovi: " + ulaz.getZnak());
		System.err.println("Izvorni kod: " + ulaz.getNiz());
		
		for( i = indeks; i < listaUlaza.size(); i++) {
			if(gotovOporavak)
				break;
			kolikoIzbrisati++;
			
			if(tablica.getSinkroZnakovi().toString().contains(listaUlaza.get(i).getZnak().toString())) {
				kolikoIzbrisati--;
				for( j = stog.size() - 1; j >= 0; j--) {
					if(gotovOporavak)
						break;
					if(!stog.get(j).getVrijednost().isJeliBroj()) {
						continue;
					}
					for(Znak znak : tablica.getSinkroZnakovi()) {
						if(tablica.imaLiDefiniranuAkciju(Integer.parseInt(stog.get(j).getVrijednost().getImeZnaka()), znak)) {
							gotovOporavak = true;
							break;
						}
					}
				}
			}
			
		}
		
		ArrayList<Cvor<Znak>> pomStog = new ArrayList<Cvor<Znak>>();
		
		for( i = 0; i <= j + 1; i++) {
			pomStog.add(stog.get(i));
		}
		
		for(i = 0; i < (stog.size() - j - 1)/2; i++) {
			tree.remove(treeIndex);
			treeIndex--;
		}
		
		stog.clear();
		stog.addAll(pomStog);
		return kolikoIzbrisati;
	}
	
}