#!/bin/bash

echo "Point 1"
echo $PWD

dir_array=("00aab_1" "01aab_2" "02aab_3" "03gram100_1" "04gram100_2" "05gram100_3" "06oporavak1" "07oporavak2" "08pomred" "09redred" "10minusLang_1" "11minusLang_2" "12ppjC" "13ppjLang" "14simplePpjLang")

echo "Point 2"

Testing_folder=$PWD
for dir in ${dir_array[*]}
do
    current="$Testing_folder/tests/$dir"

    cat $current/test.san | java -cp $Testing_folder/target/classes GSA
    cd analizator

    cat $current/test.in | java -cp $Testing_folder/target/classes/analizator SA > $current/output.out
    cd $Testing_folder

done
