package hr.fer.ppj.syntax_analyzer.enfa;

import java.util.ArrayList;
import java.util.List;

import hr.fer.ppj.enfa.Enfa;
import hr.fer.ppj.enfa.Enfa.AlphabetChar;

public class RegexEnfa {
	
	public static boolean testRegexOnEnfa(Enfa enfa, String toTest) {
		List<AlphabetChar> inputList = new ArrayList<>();
		for(char curr : toTest.toCharArray()) {
			AlphabetChar toAdd = new AlphabetChar(String.valueOf(curr));
			inputList.add(toAdd);
		}
		enfa.setInput(inputList);
		
		// Need to clear the active states, and then once again set the starting state
		enfa.refresh();
		
		enfa.performWholeSequenceTransition();
		
		return enfa.isAccepted();
	}
	
	/**
	 * Tests whether the given input satisfies the regex via an enfa.
	 * 
	 * @param enfa the enfa that emulates a regex
	 * @param toTest the String to be tested
	 * @return boolean array ->
	 * 	arr[0] --> whether the input was accepted(enfa in acceptable state)
	 * 	arr[1] --> whether the enfa() is in the Empty State
	 */
	public static boolean[] testRegexOnEnfa2(Enfa enfa, String toTest) {
		List<AlphabetChar> inputList = new ArrayList<>();
		for(char curr : toTest.toCharArray()) {
			AlphabetChar toAdd = new AlphabetChar(String.valueOf(curr));
			inputList.add(toAdd);
		}
		enfa.setInput(inputList);
		
		// Need to clear the active states, and then once again set the starting state
		enfa.refresh();
		
		enfa.performWholeSequenceTransition();
		
		return new boolean[] {enfa.isAccepted(),enfa.getActiveStates().contains(enfa.getEmptyState())};
	}

}
