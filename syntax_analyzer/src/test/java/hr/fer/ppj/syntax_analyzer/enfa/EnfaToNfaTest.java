package hr.fer.ppj.syntax_analyzer.enfa;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import hr.fer.ppj.enfa.Enfa;
import hr.fer.ppj.enfa.EnfaToDfa;

class EnfaToNfaTest {

	@Test
	void test() {
		String regex = "abc";
		Enfa enfa = ConversionAlgo.constructEnfa(regex);
		
		enfa = EnfaToDfa.changeToNfa(enfa);
		
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, regex));
	}

}
