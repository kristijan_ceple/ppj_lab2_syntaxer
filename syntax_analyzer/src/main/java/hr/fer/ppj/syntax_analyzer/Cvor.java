package hr.fer.ppj.syntax_analyzer;

import java.util.ArrayList;
import java.util.List;

import hr.fer.ppj.syntax_analyzer.generator.Znak;

public class Cvor <T>{
	
	private List<Cvor<JedinkaUlaza>> djeca = new ArrayList<>();;
	private T vrijednost;
	
	public Cvor(List<Cvor<JedinkaUlaza>> listaDjece, T vrijednost) {
		super();
		
		if(listaDjece != null) {
			this.djeca.addAll(listaDjece);
		}
		this.vrijednost = vrijednost;
	}
	
	public Cvor(T znakic) {
		this.vrijednost = znakic;
	}

	public void dodajDijete(Cvor<JedinkaUlaza> vrijednost) {
		djeca.add(vrijednost);
	}
	
	public String obidjiPodstablo(int level) {
		// If S' skip
		JedinkaUlaza jedUlaz = (JedinkaUlaza)this.vrijednost;
		String name = jedUlaz.getZnak();
		if(name.equals("S'")) {
			return djeca.get(0).obidjiPodstablo(0);
		}
		
		StringBuilder sb = new StringBuilder();
		
		// First get to our level
		for(int i = 0; i < level; i++) {
			sb.append(" ");
		}
		
		JedinkaUlaza tmp = (JedinkaUlaza) vrijednost;
		
		sb.append(tmp.getZnak());
		if(tmp.getNiz() != null && tmp.getRedak() != null) {
			sb.append(" ")
				.append(tmp.getRedak())
				.append(" ")
				.append(tmp.getNiz());
		}
		sb.append("\n");
		
		if(!djeca.isEmpty()) {
			for(Cvor<JedinkaUlaza> dijete : djeca) {
				sb.append(dijete.obidjiPodstablo(level+1));
			}
		}
		
		return sb.toString();
	}
	
	public List<Cvor<JedinkaUlaza>> getDjeca() {
		return djeca;
	}

	public T getVrijednost() {
		return vrijednost;
	}

	@Override
	public String toString() {
		return "djeca=" + djeca + ", vrijednost=" + vrijednost;
	}
	
	

}
