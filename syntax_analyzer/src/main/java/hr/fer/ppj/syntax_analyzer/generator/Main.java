package hr.fer.ppj.syntax_analyzer.generator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;

import hr.fer.ppj.enfa.Enfa;
import hr.fer.ppj.enfa.Enfa.Automaton.State;


public class Main {
	
	private static String pocetniNezavrsniZnak;
	private static Map<Znak, List<Znak>> zapocinjeSkupovi = new LinkedHashMap<Znak, List<Znak>>();
	private static Map<Znak, Set<List<Znak>>> pocetneProdukcije = new LinkedHashMap<Znak, Set<List<Znak>>>();
	public static String EPSILON_CONST = "$";
	public static Set<Znak> prazniZnakovi = new LinkedHashSet<>();
	
	public static void main(String[] args) throws ClassNotFoundException {
		
		Znak znak;
		Scanner inputScan = new Scanner(System.in);
		String[] sinkroZnakovi;
		String linija;
		Znak zadnjiNezavrsni;
		char[] pomocni;
		String[] dijeloviProdukcije;
		List<Znak> nezavrsni;
		List<Znak> zavrsni;
		
		linija = inputScan.nextLine();
		nezavrsni = Arrays.asList(linija.substring(3).split(" ")).stream()
				.map(a -> new Znak(a, true)) 
				.collect(Collectors.toList());
		pocetniNezavrsniZnak = linija.split(" ")[1];
		linija = inputScan.nextLine();
		zavrsni = Arrays.asList(linija.substring(3).split(" ")).stream()
				.map(a -> new Znak(a, false)) 
				.collect(Collectors.toList());
//		zavrsni.add(new Znak("KN",false));
		
		linija = inputScan.nextLine();
		linija = linija.substring(5);
		
		sinkroZnakovi = linija.split(" ");
		
		linija = inputScan.nextLine();
		
		while(inputScan.hasNextLine()) {
			pomocni = linija.toCharArray();
			znak = new Znak (linija, true);
			Set<List<Znak>> sveProdukcijeJednogZnaka = new LinkedHashSet<List<Znak>>();
			
			while(inputScan.hasNextLine()) {
				linija = inputScan.nextLine();
				if(linija.charAt(0) == ' ') {
					List<String> redakProdukcija = Arrays.asList(linija.substring(1).split(" "));
					List<Znak> redakZnakovnihProdukcija = redakProdukcija.stream()
						.map(a -> new Znak(a, a.startsWith("<"))) 
						.collect(Collectors.toList());
					sveProdukcijeJednogZnaka.add(redakZnakovnihProdukcija);
				}			  
				else break;
				
				// Need to either put it into the map, or update the current key-value pair
//				pocetneProdukcije.put(znak,  sveProdukcijeJednogZnaka);
				pocetneProdukcije.merge(znak, sveProdukcijeJednogZnaka, (a, b) -> {
					a.addAll(b);
					return a;
				});
			}
		}	
		
		zapocinjeSkupovi = getZapocniSkupove(pocetneProdukcije, nezavrsni, zavrsni);
		
		for(Entry<Znak, Set<List<Znak>>> pom : pocetneProdukcije.entrySet()) {
			for(List<Znak> marin : pom.getValue()) {
				if(marin.get(0).getImeZnaka().equals("$"))
					marin.clear();
			}
		}
		
		Set<Znak> sviZnakovi = new LinkedHashSet<>();
		sviZnakovi.addAll(nezavrsni);
		sviZnakovi.addAll(zavrsni);
		
		zavrsni.add(new Znak("KN",false));
		
		Znak Scrta = new Znak("S'", true);
		Znak S = new Znak("S", true);
		nezavrsni.add(Scrta);
		Set<List<Znak>> tmp = new HashSet<>();
		List<Znak> tmp2 = new ArrayList<>();
		tmp2.add(S);
		tmp.add(tmp2);
		pocetneProdukcije.put(Scrta, tmp); 

		
		for(Znak nezavrsniZnak : nezavrsni) {
			nezavrsniZnak.setZapocinjeSkup(zapocinjeSkupovi.get(nezavrsniZnak));
		}
		
		// stvaranje tablice
	
		Enfa dka = Main.napraviDka(pocetneProdukcije, sviZnakovi);
		ArrayList <Znak> listaUlaza= new ArrayList<Znak>();
		
		listaUlaza.addAll(zavrsni);
		listaUlaza.addAll(nezavrsni);
		Set<Znak> sinkro = new HashSet<Znak>();
		for(int i=0;i<sinkroZnakovi.length;++i) {
			sinkro.add(new Znak(sinkroZnakovi[0] ,false));
		}
		
		TransferTablica tablica = new TransferTablica(sinkro, dka.getStatesLength()-1, listaUlaza);
		ArrayList<State> svaStanja = new ArrayList<State>();
		svaStanja.addAll(dka.getStates());
		
		//Unošenje Pomakni i Stavi u tablicu
		
		for(State stanje: svaStanja) {
			
			for(Znak prijelazZa : listaUlaza) {
				State ciljnoStanje = dka.imaLiPrijelazZa(stanje.getName(), prijelazZa.getImeZnaka());
				if(ciljnoStanje!=null) {
					if(prijelazZa.isNezavrsni())
						tablica.ubaciAkciju(Integer.parseInt(stanje.getName()), prijelazZa, new Prs(Akcija.STAVI,Integer.parseInt(ciljnoStanje.getName())));
					else
						tablica.ubaciAkciju(Integer.parseInt(stanje.getName()), prijelazZa, new Prs(Akcija.POMAKNI,Integer.parseInt(ciljnoStanje.getName())));
				}
			}
			
		}
		
		//Unošenje Reduciraj i Prihvati u tablicu
		
			for(State stanje: svaStanja) {
			
			LinkedHashSet<LRStavka> stavke = stanje.getLRStavke();
			
			for(LRStavka stavkica : stavke) {
				
				if(stavkica.indexTocke==stavkica.desnaStrana.size() && stavkica.lijevaStrana.getImeZnaka().equals("S'")) {
					tablica.ubaciAkciju(Integer.parseInt(stanje.getName()), new Znak("KN",false), new Prs(Akcija.PRIHVATI, stavkica.lijevaStrana ,stavkica.desnaStrana));
				} else if(stavkica.indexTocke==stavkica.desnaStrana.size()) {
					Set<Znak> vitice = stavkica.znakoviUViticama;
					for(Znak znakic : vitice) {
						if(tablica.getAkcija(Integer.parseInt(stanje.getName()), znakic)==null || (tablica.getAkcija(Integer.getInteger(stanje.getName()), znakic)!=null && tablica.getAkcija(Integer.getInteger(stanje.getName()), znakic).getVrsta() == Akcija.REDUCIRAJ && tablica.getAkcija(Integer.getInteger(stanje.getName()), znakic).getIndeksProdukcije()>stavkica.ulaznaDatotekaIndex)) {
							tablica.ubaciAkciju(Integer.parseInt(stanje.getName()), znakic, new Prs(Akcija.REDUCIRAJ ,stavkica.lijevaStrana ,stavkica.desnaStrana ,stavkica.ulaznaDatotekaIndex));
						}
					}
				}
			}
			
		}
			
		try {
			FileOutputStream fo = new FileOutputStream(new File("analizator/objekt.ser"));
			ObjectOutputStream oo = new ObjectOutputStream(fo);

			oo.writeObject(tablica);

			oo.close();
			fo.close();
		} catch (FileNotFoundException e) {
			System.out.println("File not found");
		} catch (IOException e) {
			System.out.println("Error initializing stream");
		}
			
	}

	public static Map<Znak, List<Znak>> getZapocniSkupove(Map<Znak, Set<List<Znak>>> produkcije, List<Znak> nezavrsni, List<Znak> zavrsni) {
		boolean dogodilaSePromjena = true;    		//dodavamo znakove u listu praznihZnakova sve dok nema promjena u for petlji
		boolean prebaciBreak;
		boolean nijePrazan = true;
		
		while(dogodilaSePromjena) {
			dogodilaSePromjena = false;
			
			for(Znak lijevaStrana : produkcije.keySet()) { // iteriramo po produkcijama
				
				if(prazniZnakovi.contains(lijevaStrana))   // ako je nezavrsni znak vec u listi preskacemo ga
					continue;
				
				for(List<Znak> desnaStrana : produkcije.get(lijevaStrana)) {  //iteriramo po desnim stranima i trazimo epsilon-prijelaz ili niz koji se samo sastoji od znakova koji su vec stavljeni u listu praznih nizova 
					prebaciBreak = false;
					
					for(Znak znak : desnaStrana) { //iteriramo po pojedinim produkcijama
						
//						if(prazniZnakovi.contains(lijevaStrana)) {
//							prebaciBreak = true;
//							break;
//						}
						
						if(znak.getImeZnaka().equals("$")) {
							prazniZnakovi.add(lijevaStrana);
							dogodilaSePromjena = true;
							prebaciBreak = true;
							break;
						}
						

						if(!znak.isNezavrsni() || !prazniZnakovi.contains(znak)) {
							nijePrazan = true;
							break;
						}
					}
					
					if(prebaciBreak) {
						break;
					}
					if(!nijePrazan) {
						prazniZnakovi.add(lijevaStrana); // svi znakovi su bili prazni i dodajemo ga
						dogodilaSePromjena = true;
						break;
					}
				}
			}
		}
		
		int N_redaka = nezavrsni.size();
		int N_stupaca = nezavrsni.size() + zavrsni.size();
		boolean[][] parovi = new boolean[N_redaka][N_stupaca];
		int br_redak, br_stupac;
		boolean prviZnak;
		Znak zadnjeProcitani = new Znak("ZadnjeProcitani", true);
		
		for(Znak lijevaStrana : produkcije.keySet()) {
			
			for(List<Znak> desnaStrana : produkcije.get(lijevaStrana)) {
				prviZnak = true;
				
				for(Znak znak : desnaStrana) {
					
					if(!prazniZnakovi.contains(zadnjeProcitani) && !prviZnak)
						break;
					
					if(znak.getImeZnaka().equals("$"))
						break;
					
					prviZnak = false;
				
					if(znak.isNezavrsni())
						br_stupac = nezavrsni.indexOf(znak);
					else
						br_stupac = nezavrsni.size() + zavrsni.indexOf(znak);
					
					br_redak = nezavrsni.indexOf(lijevaStrana);

					parovi[br_redak][br_stupac] = true;
					zadnjeProcitani = znak;		
				}
			}
		}
		
		int redak1, stupac1, redak2, stupac2;
		boolean[][] pomParovi = new boolean[N_redaka][N_stupaca];
		
		dogodilaSePromjena = true;
		while(dogodilaSePromjena) {
			dogodilaSePromjena = false;
			
			for(int i = 0; i < N_redaka; i++) {
				
				for(int j = 0; j < N_redaka; j++) {
					
					if(parovi[i][j]) {
						redak1 = i;
						stupac1 = j;
						
						for(int k = 0; k < N_stupaca; k++) {
							
							if(parovi[stupac1][k]) {
								redak2 = stupac1;
								stupac2 = k;
								if(!pomParovi[redak1][stupac2]) {
									pomParovi[redak1][stupac2] = true;
									dogodilaSePromjena = true;
								}
							}
							
						}
					}
				}
			}
			
			for(int i = 0; i < N_redaka; i++) {
				for(int j = 0; j < N_stupaca; j++) {
					if(pomParovi[i][j])
						parovi[i][j] = true;
				}
			}
		}
		
		for(int i = 0; i < N_redaka; i++) {
			parovi[i][i] = true;
		}
		Map<Znak, List<Znak>> zapocinjeZnakovi = new LinkedHashMap<Znak, List<Znak>>();
		List<Znak> sviUlazniZnakovi = new LinkedList<>();
		sviUlazniZnakovi.addAll(nezavrsni);
		sviUlazniZnakovi.addAll(zavrsni);
		
		for(int i = 0; i < N_redaka; i++) {
			List<Znak> skupZnakova = new LinkedList<>();
			
			for(int j = 0; j < N_stupaca; j++) {
				if(parovi[i][j] && !sviUlazniZnakovi.get(j).isNezavrsni()) {
					skupZnakova.add(sviUlazniZnakovi.get(j));
				}
			}
			zapocinjeZnakovi.put(sviUlazniZnakovi.get(i), skupZnakova);
			
		}
//		Map<List<Znak>, List<Znak>> zapocinjePro = new LinkedHashMap<List<Znak>, List<Znak>>();
//		
//		
//		for(Znak lijevaStrana : produkcije.keySet()) {
//			
//			for(List<Znak> desnaStrana : produkcije.get(lijevaStrana)) {	
//				List<Znak> skupZnakova2 = new LinkedList<>();
//				
//				if(desnaStrana.get(0).getImeZnaka().equals("$"))
//					continue;
//				
//				for(Znak znakDesneStrane : desnaStrana) {				
//					if(!znakDesneStrane.isNezavrsni()) {
//						skupZnakova2.add(znakDesneStrane);
//						break;
//					}
//					
//					skupZnakova2.addAll(zapocinjeZnakovi.get(znakDesneStrane));
//					
//					if(!prazniZnakovi.contains(znakDesneStrane))
//						break;
//				}
//				zapocinjePro.put(desnaStrana,skupZnakova2);
//			}
//		}
		return zapocinjeZnakovi;					
	}
	
	public static Enfa napraviDka(Map<Znak, Set<List<Znak>>> pocetneProdukcije, Set<Znak> sviZnakovi) {
		int brojacZaImeStanja = 1;
		
		Enfa DKA = new Enfa();
		 DKA.addState("0", true);
		 DKA.setStartingState("0");
		Set<LRStavka> listaStavkiStanjaPoc = DKA.getStateLRStavke("0");
		 Set<LRStavka> listaStavkiStanja = new HashSet<>();
		List<Znak> listaZnakova = new ArrayList<>();
		listaZnakova.add(new Znak(pocetniNezavrsniZnak, true));
		Set<Znak> vitice = new HashSet<>(); 
		vitice.add(new Znak("KN" , true));
		listaStavkiStanjaPoc.add(new LRStavka("S'", listaZnakova, vitice, -1));
		
		boolean dodanaStavka = true;
		boolean dodanaStanja = true;
		boolean vecPostojiStanjeSaSvimStavkama = false;
		
		class ParStanja {
			String izvor;
			String znakPrijelaza;
			public ParStanja(String izvor, String znakPrijelaza) {
				this.izvor = izvor;
				this.znakPrijelaza = znakPrijelaza;
			
			};
			
			public ParStanja getParStanja(String izvor, String znakPrijelaza){
				return this;
			}
		}
		
		Map<ParStanja, Set<LRStavka>> novaStanja = new HashMap<>();
		Set<String> znakoviPrijelaza = new LinkedHashSet<>();
		Set<LRStavka> pocStavkaNovogStanja = new LinkedHashSet<>();
		
		while(dodanaStanja) { // 1 PETLJA
			dodanaStanja = false;
			State lastState = null;
			dodanaStavka = true;
			
			// Adding LRStavka to DKA
			while(dodanaStavka){ // 1.1 PETLJA
				dodanaStavka = false;
				for(State pomStanje : DKA.getStates()) {
					Set<LRStavka> listaStavkiStanjaPom = new HashSet<>();
					if(pomStanje.getName().equals("$"))
						continue;
					
					if(pomStanje.rijeseneProdukcije)
						continue;
					for(LRStavka lrstavka: DKA.getStateLRStavke(pomStanje.getName())) {  // 1.1.1 PETLJA
						if(lrstavka.desnaStrana.size() <= lrstavka.indexTocke){ 
							continue;
						}
						if(lrstavka.oznacen || !lrstavka.desnaStrana.get(lrstavka.indexTocke).isNezavrsni())
							continue;
						Set<List<Znak>> podProdukcije = pocetneProdukcije.get(lrstavka.desnaStrana.get(lrstavka.indexTocke));
						for(List<Znak> pom: podProdukcije) { // 1.1.1.1 PETLJA
							LRStavka newStavka = new LRStavka(lrstavka.desnaStrana.get(lrstavka.indexTocke).toString(), pom, null);
							newStavka.setUlaznaDatotekaIndex(discoverUlaznaDatotekaIndex(newStavka));
							newStavka.znakoviUViticama = Main.saznajVitice(lrstavka, newStavka);
							listaStavkiStanjaPom.add(newStavka);
							dodanaStavka = true;
						}
						lrstavka.oznacen = true;
					}
					listaStavkiStanja.addAll(listaStavkiStanjaPom);
					pomStanje.getLRStavke().addAll(listaStavkiStanja);
					listaStavkiStanja.clear();
				}
			}
			
			for(State stanje : DKA.getStates()) { // 1.2 PETLJA
				if(stanje.rijeseneProdukcije || stanje.getName().equals("$")) {
					continue;
				}
				novaStanja = new HashMap<>();
				znakoviPrijelaza = new LinkedHashSet<>();
				Map<Znak, Set<LRStavka>> lrStavkeJednogZnakaPrijelaza = new HashMap<>();
				
				for(Znak znakPrijelaza : sviZnakovi) {
						Set<LRStavka> stavke = new LinkedHashSet<>();
						lrStavkeJednogZnakaPrijelaza.put(znakPrijelaza, stavke);
						
					for(LRStavka lrstavka : DKA.getStateLRStavke(stanje.getName())) { // 1.2.1 PETLJA 
						pocStavkaNovogStanja = new LinkedHashSet<>();
						
						if(lrstavka.desnaStrana.size() == lrstavka.indexTocke)
							continue;
						LRStavka testStavka = new LRStavka(lrstavka.getLijevaStrana().toString(), lrstavka.getDesnaStrana(), lrstavka.znakoviUViticama);
						testStavka.setIndexTocke(lrstavka.getIndexTocke() + 1);
						testStavka.setUlaznaDatotekaIndex(discoverUlaznaDatotekaIndex(testStavka));
						
						if(!testStavka.desnaStrana.get(testStavka.getIndexTocke() - 1).equals(znakPrijelaza))
							continue;
						lrStavkeJednogZnakaPrijelaza.get(znakPrijelaza).add(testStavka);
					}
				}
				
				
				for(Entry<Znak,Set<LRStavka>> stavkeJednogZnaka : lrStavkeJednogZnakaPrijelaza.entrySet()) {
					if(stavkeJednogZnaka.getValue().isEmpty())
						continue;
					vecPostojiStanjeSaSvimStavkama= false;
					
					for(State pomStanje : DKA.getStates()) { // 1.2.1.1
						boolean stanjeSadrziSveStavke = true;
						for(LRStavka pomStavka : stavkeJednogZnaka.getValue()) {
							if(!pomStanje.getLRStavke().contains(pomStavka)) {
								stanjeSadrziSveStavke = false;
								break;
							}
								
						}
						if(stanjeSadrziSveStavke) {
//						if(pomStanje.getLRStavke().contains(stavkeJednogZnaka.getValue())) {
							vecPostojiStanjeSaSvimStavkama = true;
							DKA.addTransition(stanje.getName(), stavkeJednogZnaka.getKey().toString() , pomStanje.getName());
						}
					}
					
					if(!vecPostojiStanjeSaSvimStavkama) {
						ParStanja par = new ParStanja(stanje.getName(), stavkeJednogZnaka.getKey().toString());
						novaStanja.put(par, stavkeJednogZnaka.getValue());
//							if(znakoviPrijelaza.contains(stavkeJednogZnaka.getKey())) {
//								for(ParStanja pomPar : novaStanja.keySet()) {
//									if(pomPar.izvor.equals(stanje.getName()) &&
//									   pomPar.znakPrijelaza.equals(testStavka.desnaStrana.get(testStavka.getIndexTocke() - 1).toString())) {
//										novaStanja.get(pomPar).add(testStavka);
//										break;
//									}
//								}
//							}
//							else {
//								ParStanja par = new ParStanja(stanje.getName(), testStavka.desnaStrana.get(testStavka.getIndexTocke() - 1).toString());
//								pocStavkaNovogStanja.add(testStavka);
//								novaStanja.put(par,pocStavkaNovogStanja);
//								znakoviPrijelaza.add(testStavka.desnaStrana.get(testStavka.getIndexTocke() - 1).toString());
//							}
					}
				}
				
				if(!novaStanja.isEmpty()) {
					dodanaStanja = true;
					stanje.rijeseneProdukcije = true;
					break;
				}
				stanje.rijeseneProdukcije = true;
			}
			
			for (Entry<ParStanja,Set<LRStavka>> stanjeIme : novaStanja.entrySet()) {
				ParStanja parStanja = stanjeIme.getKey();
				Set<LRStavka> newStavke = stanjeIme.getValue();

					// Add the state first!
					String odrediste = Integer.toString(brojacZaImeStanja++);
					DKA.addState(odrediste, true);
					
					// Now lets make transitions
					for(LRStavka newStavka : newStavke) {
					DKA.addTransition(parStanja.izvor
							, newStavka.getDesnaStrana().get((newStavka.indexTocke - 1)).toString()
							, odrediste
							);
						break;
					}
					// Let's add the stavkas to the current state
					Set<LRStavka> currStateStavkas = DKA.getStateLRStavke(odrediste);
					currStateStavkas.addAll(newStavke);
			}
		}
		
		System.out.println(DKA.ispisDKA());
		return DKA;
	}

	/**
	 * Returns index if found, else -1 if something went wrong.
	 * 
	 * @param toDiscover the stavka the index of which needs to be found
	 * @return the index in the ulazna datoteka
	 */
	private static int discoverUlaznaDatotekaIndex(LRStavka toDiscover) {
		Znak leftSide = toDiscover.lijevaStrana;
		List<Znak> rightSide = toDiscover.desnaStrana;
		
		for(Entry<Znak, Set<List<Znak>>> entrich : pocetneProdukcije.entrySet()) {
			Znak compareLeftSide = entrich.getKey();
			int i = 0;		// Keep track of index
			
			if(leftSide.equals(compareLeftSide)) {
				// Now need to compare right sides
				Set<List<Znak>> compareRightSides = entrich.getValue();
				for(List<Znak> compareRightSide : compareRightSides) {
					if(rightSide.equals(compareRightSides)) {
						return i;
					}
					
					i++;
				}
			}
		}

		return -1;		// If something went wrong
	}
	
	/**
	 * Finds out the content of the bracket part of the LR1 Stavka. Checks if the next+1. character is terminal or non-terminal.
	 * 1) It is non terminal --> return the non-terminal
	 * 2) It is terminal. Now need to check 2 cases:
	 * 	a) Get its starting character
	 * 	b) Epsilon --- inherit the chars from the LR stavka above
	 * 
	 * @return the set containing all the vitica chars
	 */
	private static Set<Znak> saznajVitice(LRStavka stavkaAbove, LRStavka currStavka) {
		Set<Znak> followUpSet = new HashSet<>();
		
		// find out its starting character!
		// first check if out of bounds!
		if(stavkaAbove.desnaStrana.size() <= stavkaAbove.indexTocke + 1) {
			// Beta/followUp is out of bounds - therefore it is epsilon
			followUpSet.addAll(stavkaAbove.znakoviUViticama);		// EPSILON - inherit
			return followUpSet;
		}
		
		Znak followUp = stavkaAbove.desnaStrana.get(stavkaAbove.indexTocke+1);
		// Check if terminal or non-terminal
		if(!followUp.isNezavrsni()) {
			// Else it is terminal -- return it
			followUpSet.add(followUp);
			return followUpSet;
		}
		
		//Is not terminal. Will have 2 cases, not necessarily disjunctive -> NEED TO FIND THEIR INTERSECTION
		// a) goes into epsilon
		// b) doesn't go into epsilon
		for(int i = stavkaAbove.indexTocke + 1; i < stavkaAbove.desnaStrana.size(); i++) {
			if(!stavkaAbove.desnaStrana.get(i).isNezavrsni()) {
				followUpSet.add(stavkaAbove.desnaStrana.get(i));
				break;
			}
			followUpSet.addAll(zapocinjeSkupovi.get(stavkaAbove.desnaStrana.get(i)));
			if(!prazniZnakovi.contains(stavkaAbove.desnaStrana.get(i)))
				break;
			if(prazniZnakovi.contains(stavkaAbove.desnaStrana.get(i)) && i  == stavkaAbove.desnaStrana.size() - 1) {
				followUpSet.addAll(stavkaAbove.znakoviUViticama);
			}
			
			
		}
		return followUpSet;
	}
}
