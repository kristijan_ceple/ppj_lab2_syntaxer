package hr.fer.ppj.syntax_analyzer.generator;

import java.io.Serializable;

public enum Akcija implements Serializable{
	POMAKNI ,
	REDUCIRAJ ,
	PRIHVATI ,
	STAVI
}
