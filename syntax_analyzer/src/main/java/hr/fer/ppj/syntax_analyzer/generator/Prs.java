package hr.fer.ppj.syntax_analyzer.generator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Prs implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2170131196137248450L;
	private Akcija vrsta;
	private int indeks;
	private Znak lijeviZnak;
	private List<Znak> produkcija;
	private int indeksProdukcije;
	
	
	
	public Prs(Akcija vrsta) {
		
		this.vrsta = vrsta;
	
	}
	
	public Prs(Akcija vrsta ,int indeks) {
	
		this.vrsta = vrsta;
		this.indeks = indeks;
		
	}
	
	public Prs(Akcija vrsta ,Znak lijeviZnak ,List<Znak> produkcija) {
		
		this.vrsta = vrsta;
		this.lijeviZnak = lijeviZnak;
		this.produkcija = produkcija;
		
	}
	
public Prs(Akcija vrsta ,Znak lijeviZnak ,List<Znak> produkcija ,int indeks) {
		
		this.vrsta = vrsta;
		this.lijeviZnak = lijeviZnak;
		this.produkcija = produkcija;
		this.indeksProdukcije = indeks;
		
	}
	
	public Akcija getVrsta() {
		return vrsta;
	}
	
	public int getIndeksProdukcije() {
		return indeksProdukcije;
	}

	public void setIndeksProdukcije(int indeksProdukcije) {
		this.indeksProdukcije = indeksProdukcije;
	}

	public Integer getIndeks() {
		if(vrsta == Akcija.STAVI || vrsta == Akcija.POMAKNI)
			return indeks;
		else return (Integer) null;
	}

	public Znak getLijeviZnak() {
		if(vrsta == Akcija.REDUCIRAJ || vrsta == Akcija.PRIHVATI)
			return lijeviZnak;
		else return null;
	}

	public List<Znak> getProdukcija() {
		if(vrsta == Akcija.REDUCIRAJ || vrsta == Akcija.PRIHVATI)
			return produkcija;
		else return null;
	}

	@Override
	public String toString() {
		return "Prs [vrsta=" + vrsta + ", indeks=" + indeks + ", lijeviZnak=" + lijeviZnak + ", produkcija="
				+ produkcija + "]";
	}	
	
	
}
