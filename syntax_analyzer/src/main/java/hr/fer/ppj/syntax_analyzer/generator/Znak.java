package hr.fer.ppj.syntax_analyzer.generator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Znak implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6035216731663810241L;
	private String imeZnaka;
	private boolean nezavrsni;
	private boolean jeliBroj;
	private List<Znak> zapocinjeSkup;
	private boolean jeliTocka;
	
	public Znak(String imeZnaka, boolean nezavrsni) {
		super();
		this.imeZnaka = imeZnaka;
		this.nezavrsni = nezavrsni;
		this.jeliBroj=false;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((imeZnaka == null) ? 0 : imeZnaka.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Znak other = (Znak) obj;
		if (imeZnaka == null) {
			if (other.imeZnaka != null)
				return false;
		} else if (!imeZnaka.equals(other.imeZnaka))
			return false;
		return true;
	}

	public String getImeZnaka() {
		return imeZnaka;
	}

	public boolean isNezavrsni() {
		return nezavrsni;
	}

	public boolean isJeliBroj() {
		return jeliBroj;
	}

	public void setJeliBroj(boolean jeliBroj) {
		this.jeliBroj = jeliBroj;
	}
	
	@Override
	public String toString() {
		return imeZnaka;
	}

	public List<Znak> getZapocinjeSkup() {
		return zapocinjeSkup;
	}

	public void setZapocinjeSkup(List<Znak> zapocinjeSkup) {
		if(nezavrsni)
			this.zapocinjeSkup = zapocinjeSkup;
		else {
			this.zapocinjeSkup = new ArrayList<Znak>();
			this.zapocinjeSkup.add(this);
		}
	}

	public boolean isJeliTocka() {
		return jeliTocka;
	}

	public void setJeliTocka(boolean jeliTocka) {
		this.jeliTocka = jeliTocka;
	}
	
}
