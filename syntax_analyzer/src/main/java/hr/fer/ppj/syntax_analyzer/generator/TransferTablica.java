package hr.fer.ppj.syntax_analyzer.generator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class TransferTablica implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3994745522051220256L;
	private Set<Znak> sinkroZnakovi = new HashSet<Znak>();
	private HashMap<Integer, HashMap<Znak ,Prs>> glavnaMapa;
	
	public TransferTablica(Set<Znak> sinkroZnakovi ,int brojStanja ,ArrayList<Znak> znakovi) {
		
		this.setSinkroZnakovi(sinkroZnakovi);
		glavnaMapa = new HashMap<Integer, HashMap<Znak,Prs>>();
		
		for(int i=0; i<brojStanja; ++i) {
			glavnaMapa.put(i, new HashMap<Znak, Prs>());
			HashMap<Znak, Prs> pomMapa = glavnaMapa.get(i);
			for(Znak znak : znakovi)
				pomMapa.put(znak, null);
		}
	
	}
	
	public void ubaciAkciju(Integer stanje, Znak znak, Prs akcija) {
		
		HashMap<Znak, Prs> pomMapa = glavnaMapa.get(stanje);
		pomMapa.put(znak, akcija);
		
	}
	
	public boolean imaLiDefiniranuAkciju(Integer stanje, Znak znak) {
		HashMap<Znak, Prs> pomMapa = glavnaMapa.get(stanje);
		return pomMapa.get(znak)!=null;
	}
	
	public Prs getAkcija(Integer indeks, Znak znak) {
		
		HashMap<Znak, Prs> pomMapa = glavnaMapa.get(indeks);
		return pomMapa!=null?pomMapa.get(znak):null;
	}

	public Set<Znak> getSinkroZnakovi() {
		return sinkroZnakovi;
	}

	public void setSinkroZnakovi(Set<Znak> sinkroZnakovi) {
		this.sinkroZnakovi.addAll(sinkroZnakovi);
	}

	public HashMap<Integer, HashMap<Znak ,Prs>> getGlavnaMapa() {
		return glavnaMapa;
		
	}
}
