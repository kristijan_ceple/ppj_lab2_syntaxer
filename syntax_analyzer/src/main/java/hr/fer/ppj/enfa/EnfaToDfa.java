package hr.fer.ppj.enfa;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

import hr.fer.ppj.enfa.Enfa.AlphabetChar;
import hr.fer.ppj.enfa.Enfa.Automaton.State;
import hr.fer.ppj.enfa.Enfa.Automaton.State.Transition;

public class EnfaToDfa {
	
	public static Enfa changeToNfa(Enfa enfa) {
		
		// First get the States set, and then clear the Enfa's Set of States
		Set<State> oldStates = new TreeSet<>(enfa.getStates());
		Set<State> newStates = new TreeSet<>();
		String startingState = enfa.getStartingState().getName();
		//enfa.setStartingState("S'");
		enfa.reset();
		enfa.clearStates();
		
		// &(q, input_char) -> eps( &( eps(q, input_char ) )
		for(State toCalculateDelta : oldStates) {
			// Call the method that computes - add the new Transitions
			deltaKappa(toCalculateDelta, enfa);
		}
		
		enfa.setStartingState(startingState);
		return enfa;
	}
	
	
	public static void deltaKappa(State enfaState, Enfa nfa) {
		/*
		 * Find its epsilon-env, then for each state in the epsilon-env find the qualifying
		 * Transitions. Add all the dest States into a new temporary Set. Then for each of those
		 * States perform the epsilon environment.
		 * 
		 * Formula:
		 * &(q, input_char) -> eps( &( eps(q), input_char ) )
		 * 
		 * Therefore, need to calculate new States and Transitions 
		 * |States| x |Input Alphabet|
		 */

		nfa.addState(enfaState.getName(), true);
		
		Set<State> epsilonEnv = enfaState.epsilonEnvironment();	
		Set<State> newEpsilonEnv = new HashSet<>();
		
		// Now for each input char present in the Set inputCharsToCalcFor, calculate the formula
		for(AlphabetChar currAlphChar : nfa.getAlphabet()) {
			Set<State> destStatesTemp = new HashSet<>();
			for(State currentEpsState : epsilonEnv) {
				// Now, get their Transitions and check if they match
				for(Transition currTrans : currentEpsState.getTransitions()) {
					// Check if input characters match
					if(currTrans.getInput().equals(currAlphChar)) {
						// Add the dest states to the set of dest states
						destStatesTemp.addAll(currTrans.getTargetStates());
					}
				}
			}
			
			// For this input character, we have got all the destination states.
			// Now need to compute the eps env of this set
			
			for(State innerCurrState : destStatesTemp) {
				newEpsilonEnv.addAll(innerCurrState.epsilonEnvironment());
			}
			
			// Got the Formula result right now
			
			for(State s: newEpsilonEnv) {
				nfa.addTransition(enfaState.getName(), currAlphChar.toString(), s.getName());
		
			}
		}
		
	}
	
	public static Enfa changeToDfa(Enfa enfa) {
		Enfa nfa = new Enfa();
		
		
		return nfa;
	}

}
