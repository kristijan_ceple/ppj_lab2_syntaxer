package hr.fer.ppj.enfa;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import hr.fer.ppj.enfa.Enfa.Automaton.State;
import hr.fer.ppj.enfa.Enfa.Automaton.State.Transition;
import hr.fer.ppj.syntax_analyzer.generator.LRStavka;
import hr.fer.ppj.syntax_analyzer.generator.Znak;

/**
 * A public API towards the inner Automaton mechanisms.
 * 
 * Before using the Enfa, the method prepare() MUST be called!
 * 
 * @author kikyy99
 */
public class Enfa implements java.io.Serializable {
	/**
	 * Used for serialisation
	 */
	private static final long serialVersionUID = -3167564234909188651L;
	/**
	 * Epsilon transition - used to reach acceptable states at the end of input
	 */
	public static final String EPSILON = String.valueOf(Character.getNumericValue(0));
	public static final AlphabetChar EPSILON_TRANSITION = new AlphabetChar(EPSILON);
	public static final String EMPTY_STATE_STRING = "$";
	
	/**
	 * The inner, wrapped Automaton.
	 */
	private Automaton automaton;
	/**
	 * The index of the input seq. character that YET NEEDS TO BE PROCESSED
	 */
	private int nextIndex;

	/**
	 * Connects this Automaton to another Automaton. This is done by providing an Epsilon-transition
	 * from this Automaton's starting state to the starting state of the arg Automaton.
	 * 
	 * The calling Automaton shall practically incorporate a copy of the other Automaton's data
	 * into itself(the calling Automaton) and thus it shall be changed. The other Automaton will
	 * remain intact and unchanged.
	 * 
	 * Note that by doing this the other Automaton's input is not carried over to this Automaton.
	 */
	
	/**
	 * Initializes an empty Automaton - its yet needs to be set up and filled.
	 */
	public Enfa() {
		this.automaton = new Automaton();
	}
	
	public String printStateTransitionsH(String stateName) {
		State state = automaton.getState(stateName);
		StringBuilder sb = new StringBuilder();
		
		for(Transition trans : state.getTransitions()) {
			sb.append(trans.toString())
				.append("\n");
		}

		return sb.toString();
	}
	
	public String ispisDKA() {
		StringBuilder sb = new StringBuilder();
		
		for(State currState: this.getStates()) {
			sb.append(currState.getName()).append("\n");
			for(LRStavka stavka: currState.getLRStavke()) {
				sb.append(stavka.toString()).append("\n");
			}
			sb.append("\n");
			
			
			sb.append("Prijelazi prema:")
				.append("\n");
			// Now need to see each of this state's transitions
			for(Transition trans : currState.getTransitions()) {
				State dest = (State)trans.getTargetStates().toArray()[0];
				sb.append("\t").append(trans.getInput().toString())
					.append(": (").append(dest.getName()).append(")")
					.append("\n");
				
				// Get dest State stavkas that correspond to the input
				Set<LRStavka> lrstavkas = dest.getLRStavke();
				for(LRStavka lrstavka : lrstavkas) {
					// Check for correspondance
					Boolean tmp = false;
					
					// Check for correspondance
					List<Znak> rightSide = lrstavka.getDesnaStrana();
					if(rightSide.size() == 0 || lrstavka.getIndexTocke() == 0) {
						continue;		// Eps trans or new Stavka --> go to next Stavka
					}
					
					tmp = rightSide.get(lrstavka.getIndexTocke()-1).toString().equals(trans.input.toString());
					if(tmp) {
						// Append this to sb
						sb.append("\t").append(lrstavka.toString())
							.append("\n");
					}
				}
				
				sb.append("\n");		// Another newline seperator
			}
			
			sb.append("\n");
		}
		
		return sb.toString();
	}	
	
	public State imaLiPrijelazZa(String stanje, String znak) {
		
		boolean provjera = false;
		State stanje2 = this.automaton.getState(stanje);
		
		for (Transition tranzicija : stanje2.getTransitions()) {
			provjera = tranzicija.input.value.equals(znak);
			if(provjera)
				return (State)tranzicija.targetStates.toArray()[0];
		}
		return null;
	}
	
	public void connectEnfas(Enfa other) {
		//let's make an epsilon transition and connect the 2 automatons using it
		Objects.requireNonNull(other);
		
		/* Need to move all the data from the other Automaton to this Automaton before
		 * connecting their starting States.
		 */
		this.automaton.addAlphabetSet(other.getAlphabet());
		
		Set<Automaton.State> otherStates = other.getStates();
		// Need to handle states by reinitialising them
		/*
		 * First need to add all the states, and then their Transitions,
		 * as Transitions depend on States being already present
		 */
		for(Automaton.State currState : otherStates) {
			String name = currState.getName();
			boolean isAccepted = currState.isAcceptedState();
			this.addState(name, isAccepted);
		}
		
		// Need to set up Transitions as well
		for(Automaton.State currState : otherStates) {
			Set<Automaton.State.Transition> otherStateTransitions = currState.getTransitions();
			for(Automaton.State.Transition trans : otherStateTransitions) {
				String stateName = trans.getState().getName();
				String alphabetChar = trans.getInput().toString();
				Set<Automaton.State> destinationStates = trans.getTargetStates();
				
				// Need to get a String[] from destinationStates
				String[] destinationStatesArray = destinationStates.stream()
				.map(Automaton.State::toString)
				.toArray(String[]::new);
				
				this.addTransition(stateName, alphabetChar, destinationStatesArray);
			}
		}
		
		Automaton.State startingStateFirst = this.getStartingState();
		Automaton.State startingStateSecond = other.getStartingState();
		startingStateFirst.addTransition(EPSILON_TRANSITION.toString(), new String[] {startingStateSecond.toString()});
	}
	
	/**
	 * Add single State
	 * 
	 * @param name The name of the state
	 * @param acceptedState Whether this state is acceptable or not
	 */
	public void addState(String name, boolean acceptedState) {
		this.automaton.addState(name, acceptedState);
	}
	
	/**
	 * Adds multiple states into this Automaton
	 * 
	 * @param states The Map consisting of stateName-stateAcceptability key-value pairs. Must not be null.
	 */
	public void addStates(Map<String, Boolean> states) {
		Objects.requireNonNull(states);
		
		for(Map.Entry<String, Boolean> state : states.entrySet()) {
			String name = state.getKey();
			Boolean acceptedState = state.getValue();
			this.automaton.addState(name, acceptedState);
		}
	}
	
	/**
	 * Sets the starting state of the Automaton.
	 * 
	 * @param name The name of the state that shall be set as the starting state. Must not be null.
	 */
	public void setStartingState(String name) {
		this.automaton.setStartingState(name);
	}
	
	/**
	 * Prepares the Automaton for execution. This method
	 * MUST be called.
	 * 
	 * After this method the Automaton is ready for operation.
	 */
	public void prepare() {
		this.automaton.activeStates = this.automaton.activeStatesWithEpsilonEnv();
	}
	
	/**
	 * Resets the Enfa, but doesn't erase the input. Also prepares the
	 * Enfa for operation.
	 */
	public void refresh() {
		this.automaton.activeStates.clear();
		this.automaton.activeStates.add(getStartingState());
		this.prepare();
	}
	
	/**
	 * Returns the starting state. Useful to the connectEnfas function.
	 * 
	 * @return The starting State of the Automaton.
	 */
	public Automaton.State getStartingState() {
		return this.automaton.getStartingState();
	}
	
	/**
	 * Adds input alphabet characters to the Automaton
	 * 
	 * @param value
	 */
	public void addAlphabetChar(String first, String...others) {
		this.automaton.addAlphabetChars(first, others);
	}
	
	/**
	 * Sets the input of the Automaton.
	 * 
	 * @param input The input of this Automaton. Must not be null.
	 */
	public void setInput(List<AlphabetChar> input) {
		this.automaton.setInput(input);
	}
	
	public void appendInput(List<AlphabetChar> input) {
		this.automaton.appendInput(input);
	}
	
	/**
	 * @return The alphabet set of the Automaton.
	 */
	public Set<AlphabetChar> getAlphabet() {
		return this.automaton.getAlphabet();
	}
	
	/**
	 * @return the states set of the Automaton
	 */
	public Set<Automaton.State> getStates() {
		return this.automaton.states;
	}
	
	/**
	 * Appends a single character to the input.
	 * 
	 * @param inputChar The character to be appended to the input. Must not be null.
	 */
	public void appendInput(AlphabetChar inputChar) {
		this.automaton.appendInput(inputChar);
	}
	
	/**
	 * Adds a SINGLE transition.
	 * 
	 * @param stateName The source State.
	 * @param alphabetChar The transition character.
	 * @param destinationStates The destination States array.
	 */
	public void addTransition(String stateName, String alphabetChar, String[] destinationStates) {
		this.automaton.bindTransition(stateName, alphabetChar, destinationStates);
	}
	
	/**
	 * Adds a SINGLE Transition.
	 * 
	 * @param stateName The source State.
	 * @param alphabetChar	The transition character.
	 * @param destinationState	The destination State
	 */
	public void addTransition(String stateName, String alphabetChar, String destinationState) {
		this.automaton.bindTransition(stateName, alphabetChar, destinationState);
	}
	
	/**
	 * Adds a SINGLE EPSILON Transition.
	 * 
	 * @param stateName The source State.
	 * @param destinationState The destination State
	 */
	public void addEpsilonTransition(String stateName, String destinationState) {
		this.automaton.bindTransition(stateName, Enfa.EPSILON, destinationState);
	}
	
	/**
	 * Adds MULTIPLE transitions.
	 * 
	 * @param stateName The source State.
	 * @param transitions A map consisting of alphabetChar - destinationStates key-value pairs.
	 * AlphabetCharacters are transition characters, and desinationStates are destination State arrays.
	 */
	public void addTransitions(String stateName, Map<String, String[]> transitions) {
		this.automaton.bindTransitions(stateName, transitions);
	}
	
	/**
	 * Sets this as input for the automaton.
	 * 
	 * @param inputSeqs An array of strings consisting of input elements
	 */
	public void setInput(String[] inputSeqs) {
		Objects.requireNonNull(inputSeqs);
		
		List<AlphabetChar> alphCharList = new ArrayList<>();
		
		for(String current : inputSeqs) {
			AlphabetChar alphChar = new AlphabetChar(current);
			alphCharList.add(alphChar);
		}
		
		this.automaton.setInput(alphCharList);
	}
	
	/**
	 * Appends one element at the end of the input sequence
	 * 
	 * @param inputChar the input character to be appended
	 */
	public void appendInputCharacer(String inputChar) {
		Objects.requireNonNull(inputChar);
		
		AlphabetChar toAppend = new AlphabetChar(inputChar);
		this.automaton.appendInput(toAppend);
	}
	
	/**
	 * Performs transition for a single(the next) input seq character
	 * 
	 * @return false if the whole input sequence has been processed, true otherwise
	 */
	public boolean performSingleTransition() {
		return this.automaton.readInputChar(nextIndex++);
	}
	
	/**
	 * Performs transition for the specified alphabet character
	 * 
	 * @param alphChar The alphabet character for which the transmission shall be performed
	 */
	public void performSingleTransition(String alphChar) {
		this.automaton.readInputChar(new AlphabetChar(alphChar));
	}
	
	/**
	 * Performs transitions for the whole input sequence
	 * 
	 * @return true whether the Enfa finished in an acceptable State, false otherwise
	 */
	public boolean performWholeSequenceTransition() {
		for(int i = 0; i < this.automaton.input.size(); i++) {
			this.automaton.readInputChar(i);
		}
		
		// Check whether any State is acceptable
		for(Automaton.State currentState : this.automaton.activeStates) {
			if(currentState.acceptedState) {
				return true;	// An acceptable state was found among active states
			}
		}
		return false;	// No acceptable state found among active states
	}
	
	/**
	 * @return whether the Enfa is currently located in an acceptable State or not
	 */
	public boolean isAccepted() {
		// Need to iterate through all the active States
		for(Automaton.State currState : this.automaton.getActiveStates()) {
			if(currState.acceptedState) {
				return true;			// An acceptable State has been found
			}
		}
		
		return false;		// An acceptable State hasn't been found
	}
	
	/**
	 * @return the currently active states of the Automaton
	 */
	public Set<Automaton.State> getActiveStates() {
		return this.automaton.activeStates;
	}
	
	/**
	 * @return the currently active states of the Automaton in an array format
	 */
	public String[] getActiveStatesArray() {
		return this.getActiveStates().stream()
		.map(Automaton.State::toString)
		.toArray(String[]::new);
	}
	
	/**
	 * @return the currently active states of the Automaton in an array format
	 */
	public List<String> getActiveStatesList() {
		return this.getActiveStates().stream()
		.map(Automaton.State::toString)
		.collect(Collectors.toList());
	}
	
	/**
	 * Used for testing purposes - not recommended otherwise
	 * 
	 * @return the empty state of the Automaton
	 */
	public Automaton.State getEmptyState() {
		return this.automaton.EMPTY_STATE;
	}
	
	/**
	 * Returns the number of states this Automaton has
	 * 
	 * @return the number of states
	 */
	public int getStatesLength() {
		return this.automaton.states.size();
	}
	
	/**
	 * Checks if the Enfa contains the queried State.
	 * 
	 * @param state the queried state whose presence is asked
	 * @return true if the state is present in the automaton, false otherwise
	 */
	public boolean containsState(String state) {
		Automaton.State toFind = this.automaton.getState(state);
		
		return !(toFind == null);
	}
	
	/**
	 * Resets the automaton - clears input and puts the automaton once again into its
	 * starting State(and of course takes into consideration the Epsilon-transitions)
	 */
	public void reset() {
		this.automaton.reset();
	}
	
	/**
	 * Clears the Enfa of all the States
	 */
	public void clearStates() {
		this.getStates().clear();
	}
	
	public void setAcceptedState(String toSet) {
		Automaton.State foundState = this.automaton.getState(toSet);
		foundState.setAcceptedState(true);
	}
	
	/**
	 * Used for the purpose of the lab
	 * 
	 * @return the number of the last State used in this enfa.
	 */
	public int getLastStateNumber() {
		TreeSet<Automaton.State> statesSet = (TreeSet<Automaton.State>)this.getStates();
		Automaton.State last = statesSet.last();
		return Integer.valueOf(last.getName());
	}
	
	public Set<LRStavka> getStateLRStavke(String stateName){
		return this.automaton.getState(stateName).getLRStavke();
	}
	
	/**
	 * Class that models a single input alphabet character.
	 * 
	 * @author kikyy99
	 *
	 */
	public static class AlphabetChar implements Comparable<AlphabetChar>, Serializable {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 4808891703001904030L;
		private String value;

		public AlphabetChar(String value) {
			Objects.requireNonNull(value);
			
			this.value = value;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((value == null) ? 0 : value.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			AlphabetChar other = (AlphabetChar) obj;
			if (value == null) {
				if (other.value != null)
					return false;
			} else if (!value.equals(other.value))
				return false;
			return true;
		}

		@Override
		public String toString() {
			return value;
		}

		@Override
		public int compareTo(AlphabetChar o) {
			//shall compare them by String natural ordering
			return this.value.compareTo(o.value);
		}
		
	}
	
	/**
	 * Features the automaton's states. Those states feature
	 * their own transitions stored internally. Is an internal class
	 * encapsulated/wrapped by the Enfa class.
	 * 
	 * @author kikyy99
	 *
	 */
	public class Automaton implements java.io.Serializable {
		
		/**
		 * Used for serialisation
		 */
		private static final long serialVersionUID = -2473754654445319507L;
		/**
		 * Default empty state
		 */
		private final State EMPTY_STATE = new State(Enfa.EMPTY_STATE_STRING, false);
		private Set<State> states =  new TreeSet<>();
		private Set<State> visitedStates = new LinkedHashSet<>();
		private State startingState;
		private Set<State> activeStates = new TreeSet<>();
		private Set<AlphabetChar> alphabet = new LinkedHashSet<>();
		private List<AlphabetChar> input = new ArrayList<>();
		
		/**
		 * A constructor which is provided an alphabet
		 * 
		 * @param alphabet The input string characters
		 */
		private Automaton(Set<AlphabetChar> alphabet) {
			this();
			Objects.requireNonNull(alphabet);
			this.alphabet.addAll(alphabet);
		}
		
		/**
		 * An empty constructor - this Automaton yet needs to be set up!
		 */
		private Automaton() {
			this.states.add(EMPTY_STATE);
			this.alphabet.add(EPSILON_TRANSITION);
		}
		
		/**
		 * Adds the alphabet character to the Automaton. 
		 * 
		 * @param first The mandatory arg - the character to be added. Must not be null.
		 * @param others Optional arg - other characters to be added as well
		 */
		private void addAlphabetChars(String first, String...others) {
			Objects.requireNonNull(first);
			AlphabetChar toAdd = new AlphabetChar(first);
			alphabet.add(toAdd);
			
			for(String single : others) {
				toAdd = new AlphabetChar(single);
				alphabet.add(toAdd);
			}
		}
		
		/**
		 * Directly adds to the entire Automaton's alphabet.
		 * 
		 * @param alphabet The set of AlphabetChars that shall be used
		 */
		private void addAlphabetSet(Set<AlphabetChar> alphabet) {
			Objects.requireNonNull(alphabet);
			this.alphabet.addAll(alphabet);
		}
		
		/**
		 * Returns the corresponding State instance of the String argument. Inner mechanism of the Automaton.
		 * 
		 * @param find The string whose State is to be found
		 * @return Corresponding State if found, null otherwise.
		 */
		private State getState(String find) {
			Objects.requireNonNull(find);
			if(find.equals(EMPTY_STATE_STRING)) {
				return EMPTY_STATE;
			}
			
			for(State currState : states) {
				if(currState.name.equals(find)) {
					return currState;
				}
			}
			
			return null;
		}
		
		/**
		 * The state that shall be set as the starting state. Note that first
		 * this state MUST BE ADDED TO THE AUTOMATON, and only after it is present
		 * in the Automaton can it be set as the starting State.
		 * 
		 * @param name The name of the state to be set as the starting State
		 */
		public void setStartingState(String name) {
			Objects.requireNonNull(name);
			
			State startingState = this.getState(name);
			this.startingState = startingState;
			activeStates.add(startingState);
			
			// Disregard the comment below, making some changes
//			 But that's not all - need to take into consideration the Epsilon-environment as well
//			activeStates = this.activeStatesWithEpsilonEnv();
		}
		
		/**
		 * @return the startingState
		 */
		public State getStartingState() {
			return startingState;
		}

		/**
		 * Appends the input of the Automaton.
		 * 
		 * @param input The list of AlphabetChars to be set as input.
		 */
		private void appendInput(List<AlphabetChar> input) {
			Objects.requireNonNull(input);
			this.input.addAll(input);
		}
		
		private void setInput(List<AlphabetChar> input) {
			Objects.requireNonNull(input);
			this.input.clear();
			this.input.addAll(input);
		}
		
		/**
		 * Appends a single character to the input sequence.
		 * 
		 * @param inputChar The input character to be appended
		 */
		private void appendInput(AlphabetChar inputChar) {
			Objects.requireNonNull(inputChar);
			this.input.add(inputChar);
		}
		
		/**
		 * Adds a single state
		 * 
		 * @param name
		 * @param acceptedState
		 */
		private void addState(String name, boolean acceptedState) {
			State toAdd = new State(name, acceptedState);
			states.add(toAdd);
		}
		
		/**
		 * Adds multiple states.
		 * 
		 * @param statesSet The states set which multiple states shall be drawn from
		 */
		private void addStatesSet(Set<State> statesSet) {
			Objects.requireNonNull(statesSet);
			this.states.addAll(statesSet);
		}
		
		/**
		 * Internal function that binds MULTIPLE transitions to the Automaton
		 * 
		 * @param stateName the name of the source state
		 * @param transitions A map consisting of String-String[] key-value pairs. The first part
		 * - the String is the name of the alphabet character, and the second part - the String[]
		 * is the array of destination States.
		 */
		private void bindTransitions(String stateName, Map<String, String[]> transitions) {
			Objects.requireNonNull(stateName);
			Objects.requireNonNull(transitions);
			
			State state = this.getState(stateName);
			state.addTransitions(transitions);
		}
		
		/**
		 * Internal function that binds a SINGLE transition to the Automaton
		 * 
		 * @param stateName the name of the source state
		 * @param alphChar the alphabet character for which the transition shall be performed
		 * @param destinationStates self-explanatory
		 */
		private void bindTransition(String stateName, String alphChar, String[] destinationStates) {
			Objects.requireNonNull(stateName);
			Objects.requireNonNull(alphChar);
			Objects.requireNonNull(destinationStates);
			
			State state = this.getState(stateName);
			state.addTransition(alphChar, destinationStates);
		}
		
		/**
		 * Internal function that binds a SINGLE transition to the Automaton
		 * 
		 * @param stateName the name of the source state
		 * @param alphChar the alphabet character for which the transition shall be performed
		 * @param destinationState self-explanatory
		 */
		private void bindTransition(String stateName, String alphChar, String destinationState) {
			Objects.requireNonNull(stateName);
			Objects.requireNonNull(alphChar);
			Objects.requireNonNull(destinationState);
			
			State state = this.getState(stateName);
			state.addTransition(alphChar, destinationState);
		}
		
//		private void addStates(Map<String, List<Boolean, Map<String, State[]>>> statesMap) {
//			for(Entry<String, Boolean> entry : statesMap.entrySet()) {
//				String name = entry.getKey();
//				Boolean acceptedState= entry.getValue();
//				
//				this.addState(name, acceptedState);
//			}
//		}
		
		/**
		 * @return false if no more input chars left to be processed,
		 * true otherwise
		 */
		private void readInputSequenceWResPrint() {
			StringBuilder sb = new StringBuilder();
			sb.append(activeStatesWithEpsilonEnv());
			for(int i = 0; i < input.size(); i++) {
				sb.append("|");
				readInputChar(i);
				if(activeStates.size()>=2) {
					//in case an empty state still made it into the set,
					//but there are more than 2 states in the set itself
					activeStates.remove(EMPTY_STATE);
					EMPTY_STATE.setActive(false);
				}
				sb.append(activeStatesWithEpsilonEnv());
			}
			
			String toPrint = sb.toString();
			toPrint = toPrint.replace("[", "");
			toPrint = toPrint.replace("]", "");
			toPrint = toPrint.replaceAll("\\s*", "");
			System.out.println(toPrint);
		}
		
		/**
		 * Calculates the set consisting of currently active states and their
		 * recursively-combined Epsilon environment States.
		 * 
		 * Note that this method
		 * doesn't modify the activeStates private field in any way, merely returns a new
		 * Set.
		 * 
		 * Should you wish to further modify the activeStates, such an action needs to be done manually
		 * by yourself.
		 * 
		 * @return the new set consisting of currently active states and their
		 * recursively-combined Epsilon environment States
		 */
		private Set<State> activeStatesWithEpsilonEnv() {
			Set<State> activePlusEpsilon = new TreeSet<>();
			
			for(State currentState : activeStates) {
				activePlusEpsilon.addAll(currentState.epsilonEnvironment());
			}
			
			if(activePlusEpsilon.size()>=2) {
				// if there is an empty state here, IT IS NOT ALONE, SO REMOVE IT
				activePlusEpsilon.remove(EMPTY_STATE);
			}
			
			return activePlusEpsilon;
		}
		
		/**
		 * Performs a transition of the Automaton for a single input character.
		 * 
		 * @param nextIndex the index of the next sequence character to be processed
		 * @return false if all the input has been processed, true otherwise(aka if a transition has been made)
		 */
		private boolean readInputChar(int nextIndex) {
			if(startingState == null) {
				throw new NullPointerException("Starting state not set!");
			}
			
			if(nextIndex >= input.size()) {
				return false;
			}
			
			AlphabetChar nextChar = this.input.get(nextIndex);
			readInputChar(nextChar);
			return true;
		}
		
		/**
		 * Performs a transition of the Automaton for a single input character.
		 * 
		 * @param alphChar the alphabet character for which a transmission shall be performed
		 */
		private void readInputChar(AlphabetChar alphChar) {
			if(startingState == null) {
				throw new NullPointerException("Starting state not set!");
			}
			
			Set<State> epsilonEnv = new HashSet<>();
			
			//get the epsilon-env of all the former states
			for(State currState : activeStates) {
				epsilonEnv.addAll(currState.epsilonEnvironment());
			}
			
			//perform transitions for ALL the states located in the epsilon env
			/*
			 * After all the transitions have been performed, remember
			 * to clean up the empty State in case there are other States active
			 */
			this.activeStates.clear();
			for(State currState : epsilonEnv) {
				currState.performTransition(alphChar);
			}
			
			// Let's check for the empty state
			if(this.activeStates.contains(this.EMPTY_STATE) && this.activeStates.size()>=2) {
				this.activeStates.remove(this.EMPTY_STATE);
				this.EMPTY_STATE.setActive(false);
			}
			
			//now find the epsilon environment of all the newfound active states, mark them all as active
			Set<State> activeWithEpsilons = new HashSet<>(activeStates);
			for(State currState : activeWithEpsilons) {
				activeStates.addAll(currState.epsilonEnvironment());
			}
		}
		
		/**
		 * Internal method - do not use. Instead of this method, use the
		 * Enfa's reset() method.
		 */
		public void reset() {
			this.activeStates.clear();
			this.activeStates.add(this.startingState);
			Enfa.this.prepare();
			this.input.clear();
		}
		
//		public String visitedStates() {
//			StringBuilder sb = new StringBuilder();
//			
//			sb.append(visitedStates);
//			sb.append("|");
//			
//			visitedStates.clear();
//			return sb.toString();
//		}
		
		/**
		 * Internal class modelling a single State.
		 * @author kikyy99
		 *
		 */
		public class State implements Comparable<State>, Serializable {
			
			/**
			 * 
			 */
			private static final long serialVersionUID = 4444177129087646397L;
			private String name;
			private boolean acceptedState;
			private LinkedHashSet<LRStavka> lrStavke = new LinkedHashSet<>();
			private Set<Transition> transitions = new HashSet<>();
			private boolean active;
			private AlphabetChar lastCharacter;
			public boolean rijeseneProdukcije;
			
			
			private State(String name, boolean acceptedState, Transition...transitions) {
				Objects.requireNonNull(name);		//State MUST have a name, and it must be UNIQUE
				Objects.requireNonNull(acceptedState);
				
				this.name = name;
				this.acceptedState = acceptedState;

				//we don't have any transitions from this state to others
				if(transitions == null) {
					return;
				}
				
				//else add all the transitions to our transitions set
				for(Transition currentTransition : transitions) {
					this.transitions.add(currentTransition);
				}
			}
			
			public State(String name, boolean acceptedState, LinkedHashSet<LRStavka> lrStavke, Transition...transitions) {
				Objects.requireNonNull(name);		//State MUST have a name, and it must be UNIQUE
				Objects.requireNonNull(acceptedState);
				
				this.lrStavke = lrStavke;
				
				this.name = name;
				this.acceptedState = acceptedState;

				//we don't have any transitions from this state to others
				if(transitions == null) {
					return;
				}
				
				//else add all the transitions to our transitions set
				for(Transition currentTransition : transitions) {
					this.transitions.add(currentTransition);
				}
			}
			
			State(String name, boolean acceptedState) {
				this(name, acceptedState, (Transition[])null);
			}
			
			public String getName() {
				return name;
			}

			public void setName(String name) {
				this.name = name;
			}

			public boolean isAcceptedState() {
				return acceptedState;
			}

			public Set<Transition> getTransitions() {
				return transitions;
			}

			public void setTransitions(Set<Transition> transitions) {
				this.transitions = transitions;
			}

			public boolean isActive() {
				return active;
			}

			public void setActive(boolean active) {
				this.active = active;
			}

			public void setAcceptedState(boolean acceptedState) {
				this.acceptedState = acceptedState;
			}

			public AlphabetChar getLastCharacter() {
				return lastCharacter;
			}

			public LinkedHashSet<LRStavka> getLRStavke() {
				return lrStavke;
			}
			
			public void addTransition(String input, String[] destinationStates) {
				Objects.requireNonNull(destinationStates);
				
				// Still not sure about the below updating(is it necessary? - do think it helps)
				/* First need to check if a transition for this input already exists;
				 * If yes, just update the destination states */
				boolean foundAndUpdated = updateTransition(input, destinationStates);
				if(foundAndUpdated) {
					return;
				}
				
				/* Else no entry was found for this input character
				 * and thus w need to add a new entry */
				Transition toAdd = new Transition(input, destinationStates); 
				transitions.add(toAdd);
			}
			
			public void addTransition(String input, String destinationState) {
				Objects.requireNonNull(destinationState);
				
				// Still not sure about the below updating(is it necessary? - do think it helps)			
				boolean foundAndUpdated = updateTransition(input, destinationState);
				if(foundAndUpdated) {
					return;
				}
				
				Transition toAdd = new Transition(input, destinationState); 
				transitions.add(toAdd);
			}
			
			public void addTransitions(Map<String, String[]> transitions) {
				Objects.requireNonNull(transitions);
				
				for(Entry<String, String[]> entry : transitions.entrySet()) {
					String input = entry.getKey();
					String[] states = entry.getValue();
					
					Objects.requireNonNull(input);
					Objects.requireNonNull(states);
					if(states.length == 0) {
						throw new IllegalArgumentException("Array of size 0 passed as argument to the "
								+ "addTransitions method!");
					}
					
					this.addTransition(input, states);
				}
			}
			
			/**
			 * Internal function for updating the State transitions. Checks the State for
			 * Transitions and their inputs and just adds all the Destination States arguments
			 * to this Transition's Destination States Set.
			 * 
			 * Finds the needed Transition.
			 * 
			 * @return true if trans found and updated, false otherwise
			 */
			public boolean updateTransition(String input
					, String...stringDestinationStates) {
				
				for(Transition currTrans : this.transitions) {
					// Let's check the inputs
					if(currTrans.input.toString().equals(input)) {
						for(String currDestStateString : stringDestinationStates) {
							State currDestState = Automaton.this.getState(currDestStateString);
							currTrans.targetStates.add(currDestState);
						}
						return true;
					}
				}
				
				// No such transition was found, need to make a new entry
				return false;
			}
			
			/**
			 * Finds the recursively-combined Set featuring the epsilon environment States
			 * of the current State
			 * 
			 * @return recursively-combined Set featuring the epsilon environment States
			 * of the current State
			 */
			Set<State> epsilonEnvironment(){
				Set<State> epsilonEnvironment = new HashSet<>();
				epsilonEnvironment.add(this);
				//let's check if this contains any epsilon transitions, and if it does
				//add those states to the epsilon env. Repeat recursively
				
				//recursive checking - do it until the two sets are equal
				Set<State> newEpsilonEnv = new HashSet<>(epsilonEnvironment);
				Set<String> checkedStates = new HashSet<>();
				boolean equals;
				do {
					equals = true;
					for(State state : epsilonEnvironment) {
						// Skip this state if it's already present in the destination States Set
						if(checkedStates.contains(state.toString())) {
							continue;
						}
						
						for(Transition transition : state.transitions) {
							if(transition.input.equals(EPSILON_TRANSITION)) {
								equals = false;
								newEpsilonEnv.addAll(transition.targetStates);
								visitedStates.addAll(transition.targetStates);
							}
						}
						
						/*
						 * After passing the above loop we've added this state,
						 * and all its epsilon-env states to the destination States Set.
						 * 
						 * Thus, we can mark it as checked.
						 */
						checkedStates.add(state.toString());
					}
					
					if(newEpsilonEnv.equals(epsilonEnvironment)) {
						break;
					}
					epsilonEnvironment.addAll(newEpsilonEnv);
				} while(!equals);

				return epsilonEnvironment;
			}
			
			/**
			 * Performs a transition
			 * 
			 * @param currentChar the input alphabet char
			 * @return true if transition has been found, false otherwise
			 */
			private boolean performTransition(AlphabetChar currentChar) {
				boolean matches = false;
				Transition matchedTrans = null;
				for(State.Transition currentTransition : transitions) {
					if(currentTransition.input.equals(currentChar)) {
						matches = true;
						matchedTrans = currentTransition;
						break;
					}
				}

				this.active = false;
				this.lastCharacter = currentChar;
				//okay, time to do the algo part
				if(!matches && !activeStates.isEmpty()) {
					//one of states went into nothing - nothing too bad
					return false;
				}
				
				if(!matches) {
					//not matched and no active states, go to empty state.
					Automaton.this.activeStates.add(Automaton.this.EMPTY_STATE);
					Automaton.this.EMPTY_STATE.setActive(true);
					Automaton.this.visitedStates.add(EMPTY_STATE);
					return false;
				}
				
				//else we have a normal state
				Set<State> nextStates = matchedTrans.targetStates;
				for(State currentState : nextStates) {
					currentState.active = true;
					Automaton.this.visitedStates.add(currentState);
					Automaton.this.activeStates.add(currentState);
				}
				
				//that's it. 
				return true;
			}
			
			@Override
			public int hashCode() {
				final int prime = 31;
				int result = 1;
				result = prime * result + ((name == null) ? 0 : name.hashCode());
				return result;
			}

			@Override
			public boolean equals(Object obj) {
				if (this == obj)
					return true;
				if (obj == null)
					return false;
				if (getClass() != obj.getClass())
					return false;
				State other = (State) obj;
				if (name == null) {
					if (other.name != null)
						return false;
				} else if (!name.equals(other.name))
					return false;
				return true;
			}
			
			@Override
			public int compareTo(State o) {
				return this.name.compareTo(o.name);
			}
			
			@Override
			public String toString() {
				return name;
			}
			
			/**
			 * Models a SINGLE transition - therefore it MUST have an input, 
			 * and it MUST have at least one target state. It SHALL not be used
			 * to model transitions to the {@link EMPTY_STATE} or model transitions
			 * that have no input(input is null). 
			 * 
			 * They are compared both by input and the set of destination States
			 * 
			 * @author kikyy99
			 *
			 */
			class Transition implements Serializable {
				
				/**
				 * 
				 */
				private static final long serialVersionUID = 8308245791304507847L;
				/**
				 * The input character
				 */
				private AlphabetChar input;
				/**
				 * ALL THE TARGET STATES - they MUST be UNIQUE
				 */
				private Set<State> targetStates = new HashSet<>();
				
				/**
				 * Constructor.
				 * @param input
				 * @param targetStates
				 */
				private Transition(String input, String... targetStates) {
					Objects.requireNonNull(input);
					Objects.requireNonNull(targetStates);

					this.input = new AlphabetChar(input);
					//add all the elements from our array to this set
					for(String currentState : targetStates) {
						//search the automaton for the wanted state;
						State toAdd = Automaton.this.getState(currentState);
						this.targetStates.add(toAdd);
					}
				}
				
				public AlphabetChar getInput() {
					return input;
				}

				public void setInput(AlphabetChar input) {
					this.input = input;
				}

				public Set<State> getTargetStates() {
					return targetStates;
				}

				public void setTargetStates(Set<State> targetStates) {
					this.targetStates = targetStates;
				}

				private State getState() {
					return State.this;
				}

				@Override
				public int hashCode() {
					final int prime = 31;
					int result = 1;
					result = prime * result + ((input == null) ? 0 : input.hashCode());
					result = prime * result + ((targetStates == null) ? 0 : targetStates.hashCode());
					return result;
				}

				@Override
				public boolean equals(Object obj) {
					if (this == obj)
						return true;
					if (obj == null)
						return false;
					if (getClass() != obj.getClass())
						return false;
					Transition other = (Transition) obj;
					if (input == null) {
						if (other.input != null)
							return false;
					} else if (!input.equals(other.input))
						return false;
					if (targetStates == null) {
						if (other.targetStates != null)
							return false;
					} else if (!targetStates.equals(other.targetStates))
						return false;
					return true;
				}
				
				@Override
				public String toString() {
					String toReturn = String.format("&(%s, %s) = %s", State.this, input, targetStates);
					toReturn.replace('[', '{');
					toReturn.replace(']', '}');
					
					return toReturn;
				}
			}
		}

		/**
		 * @return the alphabet
		 */
		public Set<AlphabetChar> getAlphabet() {
			return alphabet;
		}

		/**
		 * @return the activeStates
		 */
		public Set<State> getActiveStates() {
			return activeStates;
		}
	}

}
